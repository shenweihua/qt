﻿#include"XQFuncEvent.h"
#include<QApplication>
XQFuncEvent::XQFuncEvent(std::function<void()> func)
	:QEvent(QEvent::Type(ET_FuncEvent)),
	m_func(func)
{
}
XQFuncEvent::XQFuncEvent(QObject* receiver, std::function<void()>&& func)
	:QEvent(QEvent::Type(ET_FuncEvent)),
	m_func(func)
{
	QApplication::postEvent(receiver, this);
}
void XQFuncEvent::postEvent(QObject* receiver,int priority)
{
	QApplication::postEvent(receiver, this, priority);
}
