﻿#ifndef XQFUNCEVENTFILTER_HPP
#define XQFUNCEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include"XQFuncEvent.h"
//发送函数--事件过滤器
class XQFuncEventFilter :public XQEventFilterObject
{
public:
    XQFuncEventFilter(QObject* parent = nullptr)
        :XQEventFilterObject([](QEvent* ev)
    {
        static_cast<XQFuncEvent*>(ev)->m_func(); 
    }
    , EventType::ET_FuncEvent, parent){}
};
#endif // !FUNCEVENTFILTER_H
