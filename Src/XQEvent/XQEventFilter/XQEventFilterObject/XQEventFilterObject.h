﻿#ifndef XQEVENTFILTEROBJECT_H
#define XQEVENTFILTEROBJECT_H
#include<functional>
#include<QObject>
#include<QEvent>
//#include<QEvent>
//事件过滤器基类
class XQEventFilterObject :public QObject
{
public:
	XQEventFilterObject(const int EventType, QObject* parent = nullptr);
	XQEventFilterObject(std::function<void(QEvent* ev)> func,const int EventType, QObject* parent = nullptr);
	XQEventFilterObject(std::function<void()> func, const int EventType, QObject* parent = nullptr);
	//设置过滤器函数
	void setFilterFunc(std::function<void(QEvent* ev)> func);
	void setFilterFunc(std::function<void()> func);
	bool eventFilter(QObject* object, QEvent* ev)override;
protected:
	std::function<void(QEvent* ev)> m_eventFilterFunc=nullptr;//执行事件过滤器操作的函数
	int m_EventType = 0;//事件类型
};

#endif 