﻿#include"XQStretchEventFilter.h"
#include<QMouseEvent>
#include<QDebug>
XQStretchEventFilter::XQStretchEventFilter(QObject* parent)
    : QObject(parent)
{
    borderWidth(4);
}
XQStretchEventFilter::XQStretchEventFilter(QWidget* parent)
    :QObject(parent)
{
    borderWidth(4);
    setWidget(parent);
}
void XQStretchEventFilter::setWidget(QWidget* widget)
{
    m_widget = widget;
    widget->setMouseTracking(true); // 开启鼠标跟踪
}
void XQStretchEventFilter::borderWidth(int width)
{
    m_resizeBorderWidth = width;
}
//左键是否按下
static bool leftButtonDown(QMouseEvent* ev)
{
    if (ev->buttons() & Qt::MouseButton::LeftButton)//左键按下的时候
        return true;
    return false;
}
bool XQStretchEventFilter::eventFilter(QObject* obj, QEvent* event)
{
    if (obj != m_widget)
        return false;
    if (event->type() == QEvent::MouseButtonRelease)//鼠标释放
    {
        m_edge = Edge::None;
        m_widget->setCursor(Qt::ArrowCursor);
    }
    else if (event->type() == QEvent::MouseButtonPress)//鼠标按下
    {
        QMouseEvent* mouseevent = static_cast<QMouseEvent*>(event);
        if (leftButtonDown(mouseevent))//左键按下的时候
        {
            const QPoint& globalPos = mouseevent->globalPos();
            const QPoint& pos = mouseevent->pos();

            int border = m_resizeBorderWidth; // 调整边框宽度
            if ((pos.x() <= border && pos.y() <= border) ||
                (pos.x() >= m_widget->width() - border && pos.y() <= border) ||
                (pos.x() <= border && pos.y() >= m_widget->height() - border) ||
                (pos.x() >= m_widget->width() - border && pos.y() >= m_widget->height() - border) ||
                (pos.x() > border && pos.x() < m_widget->width() - border && pos.y() <= border) ||
                (pos.x() > border && pos.x() < m_widget->width() - border && pos.y() >= m_widget->height() - border) ||
                (pos.x() <= border && pos.y() > border && pos.y() < m_widget->height() - border) ||
                (pos.x() >= m_widget->width() - border && pos.y() > border && pos.y() < m_widget->height() - border))
            {
               /* qInfo() << "左键按下";*/
                m_rect = QRect(m_widget->geometry().topLeft(), m_widget->geometry().size());
            }
        }
    }
    else if (event->type() == QEvent::Leave)//鼠标离开
    {
        m_rect = QRect();
    }
    else if (event->type() == QEvent::Enter)//鼠标进入
    {
    }
    else if (event->type() == QEvent::MouseMove) 
    {
        QMouseEvent* mouseevent = static_cast<QMouseEvent*>(event);
        const QPoint& globalPos = mouseevent->globalPos();//获取鼠标屏幕坐标
        const QPoint& pos = mouseevent->pos();//获取鼠标客户区坐标
        auto widgetPos = m_widget->geometry().topLeft();//获取父窗口屏幕左上角的点
        auto widgetSize = m_widget->geometry().size();//获取父窗口在屏幕的大小
        int border = m_resizeBorderWidth; // 调整边框宽度

        if(!m_rect.isNull())
        {
            switch (m_edge)
            {
            case XQStretchEventFilter::Edge::None:
                break;
            case XQStretchEventFilter::Edge::Top:
            {
                if (globalPos.y() < m_rect.y() + m_rect.height())
                {
                    m_widget->move(m_rect.x(), globalPos.y());
                    m_widget->resize(m_rect.width(), m_rect.height() + m_rect.y() - globalPos.y());
                }
                return false;
            }
            case XQStretchEventFilter::Edge::Bottom:
            {
                m_widget->resize(m_rect.width(), globalPos.y() - m_rect.y());
                return false;
            }
            case XQStretchEventFilter::Edge::Left:
            {
                m_widget->move(globalPos.x(), m_rect.y());
                m_widget->resize(m_rect.width() + m_rect.x() - globalPos.x(), m_rect.height());
                return false;
            }
            case XQStretchEventFilter::Edge::Right:
            {
                m_widget->resize(globalPos.x() - m_rect.x(), m_rect.height());
                return false;
            }
            case XQStretchEventFilter::Edge::TopLeft:
            {
                m_widget->move(globalPos.x(), globalPos.y());
                m_widget->resize(m_rect.width() + m_rect.x() - globalPos.x(), m_rect.height() + m_rect.y() - globalPos.y());
                return false;
            }
            case XQStretchEventFilter::Edge::TopRight:
            {
                m_widget->move(m_rect.x(), globalPos.y());
                m_widget->resize(globalPos.x() - m_rect.x(), m_rect.height() + m_rect.y() - globalPos.y());
                return false;
            }
            case XQStretchEventFilter::Edge::BottomLeft:
            {
                m_widget->move(globalPos.x(), m_rect.y());
                m_widget->resize(m_rect.width() + m_rect.x() - globalPos.x(), globalPos.y() - m_rect.y());
                return false;
            }
            case XQStretchEventFilter::Edge::BottomRight:
            {
                m_widget->resize(globalPos.x() - m_rect.x(), globalPos.y() - m_rect.y());
                return false;
            }
            default:
                break;
            }
        }
        
        if (pos.x() <= border && pos.y() <= border) 
        {  // 左上角区域
            m_widget->setCursor(Qt::SizeFDiagCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::TopLeft;
        } 
        else if (pos.x() >= m_widget->width() - border && pos.y() <= border)
        { // 右上角区域
            m_widget->setCursor(Qt::SizeBDiagCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::TopRight;
        } 
        else if (pos.x() <= border && pos.y() >= m_widget->height() - border) 
        { // 左下角区域
            m_widget->setCursor(Qt::SizeBDiagCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::BottomLeft;
        } 
        else if (pos.x() >= m_widget->width() - border && pos.y() >= m_widget->height() - border) 
        { // 右下角区域
            m_widget->setCursor(Qt::SizeFDiagCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::BottomRight;
        } 
        else if (pos.x() > border && pos.x() < m_widget->width() - border && pos.y() <= border) 
        { // 上边区域
            m_widget->setCursor(Qt::SizeVerCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::Top;
        } 
        else if (pos.x() > border && pos.x() < m_widget->width() - border && pos.y() >= m_widget->height() - border) 
        { // 下边区域
            m_widget->setCursor(Qt::SizeVerCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::Bottom;
        } 
        else if (pos.x() <= border && pos.y() > border && pos.y() < m_widget->height() - border) 
        { // 左边区域
            m_widget->setCursor(Qt::SizeHorCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::Left;
        } 
        else if (pos.x() >= m_widget->width() - border && pos.y() > border && pos.y() < m_widget->height() - border) 
        { // 右边区域
            m_widget->setCursor(Qt::SizeHorCursor);
            if (leftButtonDown(mouseevent))//左键按下的时候
                m_edge = Edge::Right;
        } 
        else {
            m_widget->setCursor(Qt::ArrowCursor);
        }
    }

    return false;
}