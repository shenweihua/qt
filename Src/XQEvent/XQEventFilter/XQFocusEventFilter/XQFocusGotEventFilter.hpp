﻿#ifndef XQFOCUSGOTEVENTFILTER_HPP
#define XQFOCUSGOTEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QEnterEvent>
//部件或窗口焦点获得事件过滤器
class XQFocusGotEventFilter :public XQEventFilterObject
{
public:
    XQFocusGotEventFilter(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func,QEvent::FocusIn, parent){}
    XQFocusGotEventFilter(std::function<bool(QFocusEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::FocusIn, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QFocusEvent*>(ev));        //该事件已经被处理
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H
