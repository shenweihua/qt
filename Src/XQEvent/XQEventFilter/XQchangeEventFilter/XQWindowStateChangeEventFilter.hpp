﻿#ifndef XQWINDOWSTATECHANGEEVENTFILTER_HPP
#define XQWINDOWSTATECHANGEEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QEnterEvent>
// 窗口的状态（最小化、最大化或全屏）发生改变过滤器
class XQWindowStateChangeEventFilter :public XQEventFilterObject
{
public:
    XQWindowStateChangeEventFilter(std::function<void()>  func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::WindowStateChange, parent) {}
    XQWindowStateChangeEventFilter(std::function<void(QWindowStateChangeEvent* ev)>func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::WindowStateChange, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QWindowStateChangeEvent*>(ev));
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H