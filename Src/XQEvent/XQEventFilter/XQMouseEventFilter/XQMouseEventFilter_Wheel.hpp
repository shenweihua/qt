﻿#ifndef XQMOUSEEVENTFILTER_WHEEL_HPP
#define XQMOUSEEVENTFILTER_WHEEL_HPP
#include"XQEventFilterObject.h"
#include<QMouseEvent>
//鼠标滚轮事件过滤器
class XQMouseEventFilter_Wheel :public XQEventFilterObject
{
public:
    XQMouseEventFilter_Wheel(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func,QEvent::Wheel, parent) {}
    XQMouseEventFilter_Wheel(std::function<void(QWheelEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::Wheel, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QWheelEvent*>(ev));
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H