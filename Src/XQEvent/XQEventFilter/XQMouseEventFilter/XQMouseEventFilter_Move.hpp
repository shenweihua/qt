﻿#ifndef XQMOUSEEVENTFILTER_MOVE_HPP
#define XQMOUSEEVENTFILTER_MOVE_HPP
#include"XQEventFilterObject.h"
#include<QMouseEvent>
//鼠标移动事件过滤器
class XQMouseEventFilter_Move :public XQEventFilterObject
{
public:
    XQMouseEventFilter_Move(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::MouseMove, parent) {}
    XQMouseEventFilter_Move(std::function<void(QMouseEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::MouseMove, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QMouseEvent*>(ev));      
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H