﻿#ifndef XQMOUSEEVENTFILTER_PRESS_HPP
#define XQMOUSEEVENTFILTER_PRESS_HPP
#include"XQEventFilterObject.h"
#include<QMouseEvent>
//鼠标按下事件过滤器
class XQMouseEventFilter_Press :public XQEventFilterObject
{
public:
    XQMouseEventFilter_Press(std::function<void()> func, QObject* parent = nullptr)
        :XQEventFilterObject(func,QEvent::MouseButtonPress, parent) {}
    XQMouseEventFilter_Press(std::function<void()> func, Qt::MouseButton button ,QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::MouseButtonPress, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            auto event = static_cast<QMouseEvent*>(ev);
            if(event->buttons()== button)
            {
                func();
                event->accept();
            }
        };
        setFilterFunc(eventFilterFunc);
    }
    XQMouseEventFilter_Press(std::function<void(QMouseEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject(QEvent::MouseButtonPress, parent)
    {
        auto eventFilterFunc = [=](QEvent* ev)
        {
            func(static_cast<QMouseEvent*>(ev));      
        };
        setFilterFunc(eventFilterFunc);
    }
};
#endif // !MouseEventFilter_enter_H