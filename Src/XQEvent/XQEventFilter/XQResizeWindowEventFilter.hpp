﻿#ifndef XQRESIZEWINDOWEVENTFILTER_HPP
#define XQRESIZEWINDOWEVENTFILTER_HPP
#include"XQEventFilterObject.h"
#include<QResizeEvent>
//窗口大小改变事件过滤器
class XQResizeWindowEventFilter :public XQEventFilterObject
{
public:
    XQResizeWindowEventFilter(std::function<void()>  func, QObject* parent = nullptr)
        :XQEventFilterObject(func, QEvent::Resize, parent) {}
    XQResizeWindowEventFilter(std::function<bool(QResizeEvent*)> func, QObject* parent = nullptr)
        :XQEventFilterObject([=](QEvent* ev)
            {
                auto resize = static_cast<QResizeEvent*>(ev);
                 func(resize);        //该事件已经被处理
            }, QEvent::Resize, parent) {}
};
#endif 