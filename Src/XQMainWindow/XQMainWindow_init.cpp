﻿#include"XQMainWindow.h"
#include"XQAlgorithm.h"
#include"XQSettingsInfo.h"
#include"XQStackedWidget.h"
#include"XQMySql.hpp"
#include"XQMouseEventFilter_Press.hpp"
#include"XQUserInfo.h"
#include"XQAppInfo.h"
#include"XQMainCatalogueWidget.h"
#include"XQRobotDing.h"
#include"XQRobotSetupWidget.h"
#include"XQTimerGroup.h"
#include"XQArguments.h"
#include"XQLog.hpp"
#include<QMessagebox>
#include<QPushButton>
#include<QCoreApplication>
#include<QThreadPool>
#include<QLabel>
#include<QStatusBar>
#include<QMainWindow>
#include<QSettings>
#include<QDebug>
void XQMainWindow::init()
{
	if (appInfo() == nullptr)
		return;
	resize(1400, 800);
	m_mainWindows = new QMainWindow();
	//绑定显示堆栈窗口嵌入窗口
	connect(appInfo(), &XQAppInfo::showStackedWidget, this, QOverload<int>::of(&XQMainWindow::showStackedWidget), Qt::QueuedConnection);
	auto pool = QThreadPool::globalInstance();//初始化线程池
	pool->setStackSize(10000000);
	auto userInfo = new XQUserInfo(this);
	appInfo()->setThreadPool(pool);
	appInfo()->setUserInfo(userInfo);
	appInfo()->setMainWindow(this);
	/*setCssFile(this, ":/XQNoteMainWindow/style.css");*/
	
	init_systemSetupInfo();//配置文件初始化
	init_mysql();//初始化MySQL数据库
	init_ftp();//初始化ftp
	init_Model();
	init_systemTray();
	init_statusbar();
	init_stackedWidget();
	init_catalogue();
	init_affiche();
	init_robot();
	init_timerGroup();
	init_ui();//初始化ui
	bindFunc();
	XQArguments::setAutoStartWidget(this);
	//this->hide();
}

void XQMainWindow::bindFunc()
{
	//绑定窗口关闭信号，删除存储的指针
	if(m_StackedWidget)
	connect(m_StackedWidget, &XQStackedWidget::deleteWidget, [=](QWidget* widget) {
		if (m_widgetTypes.end() == m_widgetTypes.find(widget))
			return;//不存在有问题返回
		m_widgets.remove(m_widgetTypes[widget]);
		m_widgetTypes.remove(widget);
		/*qInfo() << "删除";*/
		});
	//绑定用户登录状态改变
	if(appInfo()&&appInfo()->userInfo())
	connect(appInfo()->userInfo(), &XQUserInfo::stateChange, this, &XQMainWindow::userStateChange, Qt::QueuedConnection);
	//绑定导航单机打开小部件
	if(m_catalogue)
	{
		connect(m_catalogue, &XQMainCatalogueWidget::openWidget, this, &XQMainWindow::openWidget, Qt::QueuedConnection);
		//绑定导航单机运行命令
		connect(m_catalogue, &XQMainCatalogueWidget::runCommand, this, &XQMainWindow::runCommand, Qt::QueuedConnection);
	}
}

void XQMainWindow::init_Model()
{
	
}

void XQMainWindow::init_mysql()
{
	//{
	//	auto mysql = new XQMySql(this);
	//	appInfo()->setMySql(mysql);
	//	//连接mysql的错误输出
	//	connect(mysql, &XQMySql::sqlError, [](const QString& error) {qWarning() << error; });
	//	//连接mysql的连接完成信号
	//	connect(mysql, &XQMySql::connectionSucceed, [this] {emit showMessage("已连接上数据库"); });
	//	connect(mysql, &XQMySql::connectionSucceed, this, &XQMainWindow::mysqlConnected, Qt::QueuedConnection);
	//	emit showMessage("正在连接数据库");
	//	mysql->connection("Fund", "www.727370630.top", 3307, "Financial", "uK1-XN)elC", "fund");
	//	//mysql错误发生
	//	connect(mysql, &XQMySql::sqlError, this, [=](const QString& error) {emit showMessage(error); QMessageBox::information(this, "mysql", error); });
	//}
}

void XQMainWindow::init_ftp()
{
	/*auto ftp = new XQFtp(this);
	appInfo()->setFtp(ftp);
	ftp->setHost("www.727370630.top", 21);
	ftp->login("FtpServer", "M1aSK4[z");
	ftp->setRemoteFileDir("/FtpServer/note");*/
}
void XQMainWindow::init_systemSetupInfo()
{
	//命令行参数解析
	XQArguments args;
	auto settingsPath = args.settingsPath();
	auto AutoStartType = args.autoStartType();
	auto settingsInfo = new XQSettingsInfo(this);
	appInfo()->setSettingsInfo(settingsInfo);
	/*XQDebug << settingsPath;*/
	if(!settingsPath.isEmpty())
		settingsInfo->setSaveFile(settingsPath);//设置保存到本地的文件
	else 
		settingsInfo->setSaveFile();//设置保存到本地的文件
}

void XQMainWindow::init_robot()
{
	appInfo()->addRobot(RobotType::DingDing,new XQRobotDing(this));
	auto robot = (XQRobotDing*)appInfo()->robot(RobotType::DingDing);
	if(robot)
	{
		robot->setToken(XQRobotSetupWidget::tokenDingDing());
		robot->setSecret(XQRobotSetupWidget::secretDingDing());
	}
}

void XQMainWindow::init_timerGroup()
{
	appInfo()->setTimerGroup(new XQTimerGroup(this));
	
}



