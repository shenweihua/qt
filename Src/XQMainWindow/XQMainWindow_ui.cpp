﻿#include"XQMainWindow.h"
#include"XQTitleBarWidget.h"
#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include"XQColourWidget.h"
#include"XQAppInfo.h"
#include"XQMySql.hpp"
#include"XQStatusBarWidget.h"
#include"XQSystemTray.h"
#include"XQSettingsInfo.h"
#include"XQMainCatalogueWidget.h"
#include"XQLogDockWidget.h"
#include"XSystemInfo.h"
#include"XQSystemInfoLabel.h"
#include<QBoxLayout>
#include<QComboBox>
#include<QPushButton>
#include<QSpinBox>
#include<QLabel>
#include<QMainWindow>
#include<QProgressBar>
#include<QStatusBar>
#include<QMenu>
void XQMainWindow::init_ui()
{
	//设置自定义标题栏
	auto title = new XQTitleBarWidget(this);
	title->setButtons(15);
	/*title->setButtons({XQTBWType::close,XQTBWType:: minimized});*/
	title->setBtnSpacing(30);
	
	//
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从上到下
	layout->setContentsMargins(15, 10, 15, 0);
	layout->addWidget(title);
	layout->addWidget(m_mainWindows);
	setLayout(layout);
	m_mainWindows->setParent(this);
	setWindowTitle("欢迎使用");
}

void XQMainWindow::init_systemTray()
{
	XQSystemTray* tray = nullptr;
	if(windowIcon().isNull())
		tray = new XQSystemTray(this);
	else
		tray = new XQSystemTray(windowIcon(), this);
	appInfo()->setSystemTray(tray);
	if (appInfo()->settingsInfo()->isShowSystemTray())
		tray->show();
	//菜单设置
	auto menu = tray->contextMenu();
	menu->clear();
	{
		tray->menuAddShowHideWindow(menu, this);
		tray->menuAddQuit(menu);
	}
	//处理激活原因
	connect(tray, &XQSystemTray::activated, [=](QSystemTrayIcon::ActivationReason reason) {
		switch (reason)
		{
		case QSystemTrayIcon::Unknown:
			break;
		case QSystemTrayIcon::Context://右键
			break;
		case QSystemTrayIcon::DoubleClick://双击
			break;
		case QSystemTrayIcon::Trigger://点击
			if (!isVisible() || isMinimized())
			{//当前是隐藏或最小化状态
				if (isMinimized())
					//setWindowState(Qt::WindowNoState);
					showNormal();
				if (!isVisible())
					setVisible(true);
				activateWindow();
			}
			else
			{//当前是显示状态
				setVisible(false);
			}
			break;
		case QSystemTrayIcon::MiddleClick://中键点击
			break;
		default:
			break;
		}
		
		});
}

void XQMainWindow::init_stackedWidget()
{
	m_StackedWidget = new XQStackedWidget(this);
	m_mainWindows->setCentralWidget(m_StackedWidget);
	appInfo()->setStackedWidget(m_StackedWidget);
	m_StackedWidget->titleWidget()->setIndependentWidget(true);
	m_StackedWidget->titleWidget()->setMoveWidget(true);
}

void XQMainWindow::init_catalogue()
{
	m_catalogue = new XQMainCatalogueWidget(this);
	m_mainWindows->addDockWidget(Qt::LeftDockWidgetArea, m_catalogue);
}

void XQMainWindow::init_statusbar()
{
	m_statusbar =new XQStatusBarWidget(this);
	m_mainWindows->setStatusBar(m_statusbar);
	//链接状态栏显示提示信息
	connect(this, &XQMainWindow::showMessage, m_statusbar, &QStatusBar::showMessage, Qt::QueuedConnection);
	connect(appInfo(), &XQAppInfo::showMessage, m_statusbar, &QStatusBar::showMessage,Qt::QueuedConnection);//链接发送信息
	//状态栏中的进度条
	connect(appInfo(), &XQAppInfo::setProgressRange, m_statusbar, &XQStatusBarWidget::setProgressRange, Qt::QueuedConnection);
	connect(appInfo(), &XQAppInfo::setProgressValue, m_statusbar, QOverload<int>::of(&XQStatusBarWidget::setProgressValue), Qt::QueuedConnection);
	connect(appInfo(), &XQAppInfo::setProgressValueSize, m_statusbar, &XQStatusBarWidget::setProgressValueSize, Qt::QueuedConnection);
	connect(appInfo(), &XQAppInfo::setProgressValueCount, m_statusbar, &XQStatusBarWidget::setProgressValueCount, Qt::QueuedConnection);
	//设置显示系统信息
	m_statusbar->setOpenSystemInfoWidget(true);
	////绑定mysql的信息输出
	auto logWidget = new XQLogDockWidget("主窗口日志");
	logWidget->addLog(XQMysqlInfo);
	m_statusbar->setLogWidget(logWidget);
	m_statusbar->setVisible_logButton(false);
	 
}


void XQMainWindow::init_affiche()
{
	
}
