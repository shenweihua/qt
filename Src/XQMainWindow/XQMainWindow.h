﻿#ifndef XQFUNDMAINWINDOW_H
#define XQFUNDMAINWINDOW_H
#include"XQHead.h"
#include"XQUserInfo.h"
#include<QWidget>
#include<QMap>
#include<QTimer>
#define XQMainWindowSystemSetup 0 //设置小部件
//主窗口 
class XQMainWindow :public QWidget
{
	Q_OBJECT
public:
	XQMainWindow(QWidget* parent=nullptr, bool AutoInit = true);
	virtual ~XQMainWindow();
	//获取app基本共享数据
	XQAppInfo* appInfo()const;
	//获取在堆栈上的小部件指针(唯一类型的，单例)
	QWidget* stackedWidget(int type);
	//显示堆栈上的小部件返回是否成功
	bool showStackedWidget(int type);
public slots:
	//堆栈窗口显示小部件
	void showStackedWidget(int type, QWidget* widget, const QString& title=QString(), const QPixmap& icon = QPixmap());
	//用户登录
	virtual void userLogin(bool autoLogin = false);
	//打开小部件
	virtual void openWidget(int type);
	//系统设置小部件
	virtual void systemSetupWidget();
public slots:
	//用户状态改变
	virtual void userStateChange(userState state);
	virtual void runCommand(int type);
signals://信号
	//状态栏显示提示信息
	void showMessage(const QString& text, int timeout = 0);
protected://隐藏的函数
	//初始化
	virtual void init();
	//绑定函数
	virtual void bindFunc();
	//模型初始化
	virtual void init_Model();
	//连接mysql数据库初始化
	virtual void init_mysql();
	//连接ftp服务器初始化
	virtual void init_ftp();
	//系统设置信息初始化
	virtual void init_systemSetupInfo();
	//机器人初始化
	virtual void init_robot();
	//定时器任务组初始化
	virtual void init_timerGroup();
protected://初始化ui
	virtual void init_ui();
	virtual void init_systemTray();//托盘菜单初始化
	virtual void init_stackedWidget();//堆栈小部件初始化
	virtual void init_catalogue();//目录导航窗口初始化
	virtual void init_statusbar();//状态栏初始化
	virtual void init_affiche();//公告初始化
protected://事件
	void showEvent(QShowEvent* event)override;
	void closeEvent(QCloseEvent* event)override;
protected://变量
	QMainWindow* m_mainWindows= nullptr;//主窗口
	XQAfficheWidget* m_affiche = nullptr;//公告
	XQStatusBarWidget* m_statusbar = nullptr;//状态栏
	XQMainCatalogueWidget* m_catalogue = nullptr;//目录浮动窗口
	XQStackedWidget* m_StackedWidget= nullptr;//堆栈小部件
	QMap<int, QWidget*> m_widgets;//堆栈小部件的widget集合
	QMap<QWidget* , int> m_widgetTypes;//堆栈小部件的类型集合
};
#endif // !XQNoteMainWindow_H
