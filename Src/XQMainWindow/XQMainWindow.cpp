﻿#include"XQMainWindow.h"
#include"XQAppInfo.h"
#include"XQStackedWidget.h"
#include"XQAlgorithm.h"
#include"XQMySql.hpp"
#include<QThreadPool>
#include<QProgressBar>
#include<QMainWindow>
XQMainWindow::XQMainWindow(QWidget* parent, bool AutoInit)
	:QWidget(parent)
{
	if(AutoInit)
	{
		XQAppInfo::setGlobal(new XQAppInfo(this));
		init();
	}
}

XQMainWindow::~XQMainWindow()
{
	if (m_mainWindows != nullptr)
		m_mainWindows->deleteLater();
	//等待线程池退出
	appInfo()->threadPool()->waitForDone();
}

XQAppInfo* XQMainWindow::appInfo() const
{
	return XQAppInfo::Global();
}

QWidget* XQMainWindow::stackedWidget(int type)
{
	if (m_widgets.find(type) == m_widgets.end())
		return nullptr;
	return m_widgets[type];
}

bool XQMainWindow::showStackedWidget(int type)
{
	auto widget = stackedWidget(type);
	if(widget==nullptr)
		return false;
	m_StackedWidget->setCurrentWidget(widget);
	return true;
}


