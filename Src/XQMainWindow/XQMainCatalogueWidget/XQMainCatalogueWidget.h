﻿#ifndef XQMAINCATALOGUEWIDGET_H
#define XQMAINCATALOGUEWIDGET_H
#include"XQHead.h"
#include<QDockWidget>
#include<QPixmap>
#include"XQUserInfo.h"
//主窗口目录浮动窗口
class XQMainCatalogueWidget:public QDockWidget
{
	Q_OBJECT
public:
	XQMainCatalogueWidget(QWidget* parent=nullptr, bool AutoInit = true);
	~XQMainCatalogueWidget();
	XQAppInfo* appInfo();
public :
	//设置头像
	virtual void setHeadPortrait(const QPixmap& image);
	//用户状态改变
	virtual void userStateChange(userState state);
signals://信号
	//打开小部件
	void openWidget(int type);
	//运行命令
	void runCommand(int type);
protected://初始化
	//初始化
	virtual void init();
	virtual void init_catalogue();//目录初始化
	virtual void catalogue_user();//用户管理
	virtual void catalogue_system();//系统管理
	virtual void init_ui();
	virtual QBoxLayout* init_headPortrait();//头像初始化
protected://事件
protected://变量
	QLabel* m_title = nullptr;//标题
	QLabel* m_head=nullptr;//头像
	QTreeWidget* m_catalogue = nullptr;//目录浮动窗口
};
#endif