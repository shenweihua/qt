﻿#include"XQMainCatalogueWidget.h"
#include"XQAppInfo.h"
#include"XQMainWindow.h"
#include<QTreeWidgetItem>
void XQMainCatalogueWidget::catalogue_user()
{
	// 用户管理
	QTreeWidgetItem* userItem = new QTreeWidgetItem(m_catalogue);
	userItem->setText(0, "用户管理");
	userItem->setIcon(0, QIcon(":/icon/root.png"));
	//userItem->setHidden(true);
	// 登录
	QTreeWidgetItem* loginItem = new QTreeWidgetItem(userItem);
	loginItem->setText(0, "登录");
	loginItem->setIcon(0, QIcon(":/icon/child.png"));
	/*loginItem->setHidden(true);*/
	// 用户信息
	QTreeWidgetItem* userInfoItem = new QTreeWidgetItem(userItem);
	userInfoItem->setText(0, "用户信息");
	userInfoItem->setIcon(0, QIcon(":/icon/child.png"));
	userInfoItem->setHidden(true);
	// 修改用户密码
	QTreeWidgetItem* changePasswordItem = new QTreeWidgetItem(userItem);
	changePasswordItem->setText(0, "修改密码");
	changePasswordItem->setIcon(0, QIcon(":/icon/child.png"));
	changePasswordItem->setHidden(true);
	//退出登录
	QTreeWidgetItem* logOutItem = new QTreeWidgetItem(userItem);
	logOutItem->setText(0, "退出登录");
	logOutItem->setIcon(0, QIcon(":/icon/child.png"));
	logOutItem->setHidden(true);

	// 添加根节点到 QTreeWidget 中
	m_catalogue->addTopLevelItem(userItem);
	auto userInfo = appInfo()->userInfo();
	// 绑定 itemClicked 信号
	connect(m_catalogue, &QTreeWidget::itemClicked, this, [=](QTreeWidgetItem* clickedItem, int column) {
	/*if (clickedItem == loginItem) emit runCommand(CommandType::Login);
	else if (clickedItem == userInfoItem) emit openWidget(WidgetType::userInfo);
	else if (clickedItem == changePasswordItem) emit openWidget(WidgetType::changePassword);*/
	if (clickedItem == logOutItem) userInfo->setState(userState::Logout);//更改状态为退出
		});
	//绑定用户登录状态信号
	if (userInfo != nullptr)
	{
		connect(userInfo, &XQUserInfo::stateChange, [=](userState state) {
			/*userItem->setHidden();*/
			loginItem->setHidden(!state);
			userInfoItem->setHidden(state);
			changePasswordItem->setHidden(state);
			logOutItem->setHidden(state);
			m_catalogue->expandAll();
			});
	}
}


void XQMainCatalogueWidget::catalogue_system()
{
	//系统管理
	QTreeWidgetItem* systemItem = new QTreeWidgetItem(m_catalogue);
	systemItem->setText(0, "系统管理");
	systemItem->setIcon(0, QIcon(":/icon/root.png"));

	QTreeWidgetItem* systemSetup= new QTreeWidgetItem(systemItem);
	systemSetup->setText(0, "系统设置");
	systemSetup->setIcon(0, QIcon(":/icon/root.png"));
	// 绑定 itemClicked 信号
	connect(m_catalogue, &QTreeWidget::itemClicked, this, [=](QTreeWidgetItem* clickedItem, int column) {
		if (clickedItem == systemSetup) emit openWidget(XQMainWindowSystemSetup); 
		});
	// 添加根节点到 QTreeWidget 中
	m_catalogue->addTopLevelItem(systemItem);
}
