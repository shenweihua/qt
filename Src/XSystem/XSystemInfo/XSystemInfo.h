﻿#ifndef XQSYSTEMINFO_H
#define XQSYSTEMINFO_H
//查询系统CPU和内存占用情况
class XSystemInfo 
{
public:
    //构造和系统函数
    static XSystemInfo& instance();
    virtual ~XSystemInfo();

    //初始化函数
    virtual void init() = 0;

    //cpu平均使用率
    virtual double cpuLoadAverage() = 0;
    //cpu频率
    virtual double cpuSpeed() = 0;
    //内存使用率
    virtual double memoryUsed() = 0;

protected:
    explicit XSystemInfo();
private:
    XSystemInfo(const XSystemInfo& rhs);
    //XSystemInfo& operator=(const XSystemInfo& rhs);
};
#endif // !XQSystemInfo_H
