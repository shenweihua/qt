﻿#ifdef __APPLE__
#ifndef SYSINFOMACIMPL_H
#define SYSINFOMACIMPL_H
#include "XSystemInfo.h"
#include <QtGlobal>
#include <QVector>

class SysInfoMacImpl : public XSystemInfo
{
public:
    SysInfoMacImpl();

    void init() override;
    double cpuLoadAverage() override;
    double memoryUsed() override;

private:
    QVector<qulonglong> cpuRawData();
private:
    QVector<qulonglong> mCpuLoadLastValues;
};

#endif // SYSINFOMACIMPL_H
#endif // __APPLE__

