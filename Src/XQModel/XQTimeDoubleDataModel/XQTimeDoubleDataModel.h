﻿#ifndef XQTIMEDOUBLEDATAMODEL_H
#define XQTIMEDOUBLEDATAMODEL_H
#include<QObject>
#include<QDateTime>
struct XQHistoryTrendItem;
struct XQHistoryTrend;
#define getStartEndTime_min 1
#define getStartEndTime_max 2
#define getStartEndTime_all 3
//时间/double数据模型
class XQTimeDoubleDataModel :public QObject
{
	Q_OBJECT
public:
	XQTimeDoubleDataModel(QObject*parent=nullptr);
	~XQTimeDoubleDataModel();
public:
	//获取正确的时间
	static bool getStartEndTime(int type,const QList<QDateTime>&dataTimes , const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);
	bool getStartEndTime(const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);
	//计算一个范围内的涨跌幅度
	static double changePercent(int type, const QMap<QDateTime, double>& datas, const QDateTime& start, const QDateTime& end, QDateTime* startOut = nullptr, QDateTime* endOut = nullptr);//涨跌幅度
	//计算指定范围内的涨跌幅
	double changePercent(const QDateTime& start, const QDateTime& end, QDateTime* startOut, QDateTime* endOut)const;
	//获取历史趋势
	XQHistoryTrend getHistoryTrend()const;
public:
	const QMap<QDateTime, double>& datas()const;
	double data(QDateTime time)const;
	QPair<double, double> value_min_max(const QDateTime& startDate, const QDateTime& endDate);
	double value_max()const;
	double value_min()const;
	QDateTime time_max()const;
	QDateTime time_min()const;
public:
	virtual void insert(QDateTime time, double value);
	virtual void remove(QDateTime time);
	virtual	void clear();
	virtual	void updateMaxMin();
protected:
	QMap<QDateTime, double> m_data;
	double m_max = -INT_FAST64_MAX;
	double m_min = INT_FAST64_MAX;
};
//历史趋势项
struct XQHistoryTrendItem
{
	bool isEmpty() const;
	double value = 0.0;//值
	QDateTime startTime;//开始日期
	QDateTime endTime;//结束日期
	QString startToString(const QString& format = "yyyy-MM-dd", const QString& defaultValue = "0000-00-00")const;
	QString endToString(const QString& format = "yyyy-MM-dd", const QString& defaultValue = "0000-00-00")const;
	QString toString()const;
};
struct XQHistoryTrend//历史趋势
{
	XQHistoryTrend() = default;
public:
	XQHistoryTrendItem OneWeek;//近一周
	XQHistoryTrendItem OneMonth;//近一月
	XQHistoryTrendItem ThreeMonth;//近三月
	XQHistoryTrendItem SixMonth;//近六月
	XQHistoryTrendItem OneYear;//近一年
	XQHistoryTrendItem TwoYear;//近两年
	XQHistoryTrendItem ThreeYear;//近三年
	XQHistoryTrendItem FiveYear;//近五年
	XQHistoryTrendItem ThisYear;//今年来
	XQHistoryTrendItem Hitherto;//至今
	bool isNull = true;
};

#endif // !XQChangePercentDockModel_H
