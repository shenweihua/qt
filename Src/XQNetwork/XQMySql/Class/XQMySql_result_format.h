﻿#ifndef XQMYSQL_RESULT_FORMAT_H
#define XQMYSQL_RESULT_FORMAT_H
#include<QStringList>
void format(const QStringList& ret, int nSel);//递归终止
//格式转化
void format(const QString& ret, short int& value);
void format(const QString& ret, unsigned short int& value);
void format(const QString& ret, int& value);
void format(const QString& ret, unsigned int& value);
void format(const QString& ret, long& value);
void format(const QString& ret, unsigned long& value);
void format(const QString& ret, long long& value);
void format(const QString& ret, unsigned long long& value);
void format(const QString& ret, float& value);
void format(const QString& ret, double& value);
void format(const QString& ret, QString& value);
#endif