﻿#ifndef XQHTTPOBJECT_H
#define XQHTTPOBJECT_H
#include<QObject>
#include"XQNework.h"
#include<QHash>
#define SignalsRequestHeader "SignalsRequest" //信号请求
#define SignalsReplyHeader "SignalsReply" //信号响应返回

class XQHttpObject :public QObject
{
	Q_OBJECT
public:
	XQHttpObject(QObject* parent = nullptr);
	virtual ~XQHttpObject();
	static QJsonDocument QVariantList_toJson(const QVariantList& list);
public:
	//获取host和port
	QString hostPort(QAbstractSocket* socket);
	NetworkType serverType()const;
	bool isTcpNetwork()const;
	bool isLocalNetwork()const;

public:
	//调试模式输出信息
	void setDebugModel(int model);
	void setDebugModel(Http::Debug model,bool open=true);
	//发送信号  客户端时向服务器发送信息 服务器向全体客户端发送信息
	virtual void sendSignals(const QString& name, const QVariantList& data);
	//添加槽方法
	void addSlotFunc(const QString& name, std::function<QVariantList(const QVariantList&)> slotFunc);
	//删除槽方法
	void removeSlotFunc(const QString& name);
	//清空槽方法
	void clearSlotFunc();
public:
	
signals://信号
	void error(const QString& error);
	//来自其他主机的请求
	void signalsRequest(const QString& name,const QVariantList& data);
	//请求头可读 用在服务端读取客户端请求
	void headRequestRead(const XQHttpHeadRequest& head);
	//响应头可读 用在客户端读取服务端的响应
	void headReplyRead(const XQHttpHeadReply& head);
protected:
	virtual void init();
	//是否是信号需要处理
	bool ishandlingSignalsRequest(XQHttpHeadObject* header);
	//是否是信号需要处理
	QByteArray handlingSignalsRequest(XQHttpHeadObject* header, const QByteArray& data);
	//读取内容完成
	bool isReadContentFinish(const XQHttpHeadObject& head, const QByteArray& data, qlonglong startIndex);
	//转码
	QByteArray GbkToUtf8(const QByteArray& data);
protected:
	int m_debug = Http::Debug::error;//调试模式
	NetworkType m_serverType = NetworkType::Tcp;//服务器类型
	QHash<QString, std::function<QVariantList(const QVariantList&)>> m_slotFunc;//槽方法
};
#endif // !XQHttpObject_H


