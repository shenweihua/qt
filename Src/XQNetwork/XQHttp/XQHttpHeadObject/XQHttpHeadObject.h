﻿#ifndef XQHTTPHEADOBJECT_H
#define XQHTTPHEADOBJECT_H
#include<QNetworkRequest>
#include<QString>
#include<QHash>
#include<QJsonDocument>
#include"XQNework.h"
using HttpRequestType = Http::RequestType;
using HttpProtocol = Http::Protocol;
//using Header = QHash<QByteArray, QByteArray>;
class  XQHttpHeadObject
{
public:
	XQHttpHeadObject(const QByteArray& header = QByteArray());
	virtual ~XQHttpHeadObject()=default;
public:
	static Http::Versions getVersions(const QString& text);
public:
	virtual QByteArray toByteArray()const;
	//头部转字符串输出
	virtual QString toStrinng()const;
	//http获取版本
	Http::Versions versions()const;
	//协议版本转字符串输出
	QByteArray versions_toByteArray()const;
	QString versions_toString()const;
	//头部数据链接转QByteArray
	virtual QByteArray header_toByteArray()const;
public:
	//返回查看头部
	const QHash<QByteArray, QByteArray>& header()const;
	QHash<QByteArray, QByteArray>& header();
	QByteArray header(const QByteArray& headerName)const;
	QByteArray header(QNetworkRequest::KnownHeaders header)const;
	//返回主机地址
	QString host()const;
	//返回主机端口
	quint16 port()const;
	//返回主机地址和端口
	QString hostPort()const;
	//内容长度
	size_t contentLength()const;
	//返回内容
	const QByteArray& content()const;
	//内容转QString
	QString content_toString()const;
	//内容转json
	QJsonDocument content_toJson()const;

public:
	//解析设置http头部数据
	virtual void setData(const QByteArray& header);
	//设置版本信息
	void setVersions(Http::Versions type);
	//设置头部
	void setHeader(const QByteArray& headerName, const QByteArray& headerValue);
	void setHeader(QNetworkRequest::KnownHeaders header, const QByteArray& headerValue);
	void setHeader(const QByteArrayList& data);
	//头部数据清空
	void clear_header();
	//清空
	virtual void clear();
	//设置内容长度
	void setContentLength(size_t len);
	//设置内容
	void setContent(const QByteArray& data);
	//设置主机(带端口)
	void setHostPort(QString host,quint16 port);
	
protected:
	virtual void init();
protected:
	QHash<QByteArray, QByteArray> m_header;//头部数据
	Http::Versions m_versions;//http版本类型
	QByteArray m_content;//内容
};
#endif