﻿#ifndef XQROBOT_H
#define XQROBOT_H
#include<QJsonDocument>
#include<QObject>
#include"XQHead.h"
enum RobotType
{
	DingDing//钉钉
};
//机器人
class XQRobot :public QObject
{
	Q_OBJECT
public:
	XQRobot(RobotType type,QObject* parent = nullptr);
	virtual ~XQRobot()=default;
	//是否启用
	bool isEnabled()const;
public:
	void setEnabled(bool Enabled);
public:
	//转钉钉机器人
	XQRobotDing* toDingDingRobot()const;
public:
	virtual bool send(const QJsonDocument& json)=0;
protected:
	bool m_Enabled = true;//是否启用
	RobotType m_type;//机器人类型
};
#endif // !XQRobot_H
