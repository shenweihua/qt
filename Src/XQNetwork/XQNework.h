﻿#ifndef XQNEWORK_H
#define XQNEWORK_H
class XQLog;
class XQHttpHeadObject;
class QAbstractSocket;
class QNetworkRequest;
class QNetworkAccessManager;
class QNetworkReply;
class XQHttpHeadRequest;
class QTcpServer;
class QTcpSocket;
class QLocalServer;
class QLocalSocket;
class XQTaskPool;
class XQHttpHeadReply;
//网络类型
enum class NetworkType
{
	Tcp,
	Local
};
namespace Http
{
	//debug模式
	enum Debug
	{
		error=1,//错误
		Run=2,//运行
		Connection =4,//连接
		Disconnect=8,//断开
		HeadRequest=16,//请求头
		HeadReply=32,//响应头
		
	};
	//http协议版本
	enum Versions
	{
		HTTP1,//HTTP/1.0：最早的 HTTP 协议版本，提供了基本的请求和响应功能。它使用短暂的连接，在每个请求/响应后都会关闭连接。
		HTTP11,//HTTP/1.1 是当前广泛使用的 HTTP 协议版本，引入了持久连接（keep - alive）机制，允许多个请求 / 响应通过同一连接复用，从而减少了连接建立的开销。它还支持管道化（pipelining）技术，允许客户端同时发送多个请求而无需等待响应。
		HTTP2,//HTTP/2
		HTTP3,//HTTP/3
	};
	//请求类型
	enum RequestType
	{
		GET,
		POST
	};
	//协议
	enum Protocol
	{
		http,
		https
	};
}
#endif // !XQNework_H
