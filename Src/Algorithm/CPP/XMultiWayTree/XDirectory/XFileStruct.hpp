﻿#ifndef XFILESTRUCT_H
#define XFILESTRUCT_H
#include"XMultiWayTree.hpp"
#include<functional>
//文件类型
enum class XFileType
{
	file,//文件
	dir//目录
};
//文件结构
template<typename Ty>
class XFileStruct:protected XMultiWayTree<Ty>
{
public:
	XFileStruct(const QString& name, XFileStruct<Ty>* parent=nullptr);
	virtual~XFileStruct()=default;
	//设置父目录
	void setParentDirectory(XFileStruct<Ty>* parent);
	//获取父目录
	XFileStruct<Ty>* parentDirectory();
	//设置名字
	void setName(const QString& name);
	//获取名字
	const QString& name()const;
	//设置数据
	void setData(const Ty& data);
	//获取数据
	const Ty& data()const;
	//获取类型
	XFileType type()const;
protected:
	QString m_name;//名字
	XFileType m_type;//文件类型
};

template<typename Ty>
inline XFileType XFileStruct<Ty>::type()const
{
	return m_type;
}

template<typename Ty>
inline XFileStruct<Ty>::XFileStruct(const QString& name, XFileStruct<Ty>* parent)
	:m_name(name),
	XMultiWayTree<Ty>(0,1, parent)
	,m_type(XFileType::file)
{

}

template<typename Ty>
inline void XFileStruct<Ty>::setParentDirectory(XFileStruct<Ty>* parent)
{
	XMultiWayTree<Ty>::setParent(parent);
}

template<typename Ty>
inline XFileStruct<Ty>* XFileStruct<Ty>::parentDirectory()
{
	return XMultiWayTree<Ty>::parent();
}

template<typename Ty>
inline void XFileStruct<Ty>::setName(const QString& name)
{
	m_name = name;
}

template<typename Ty>
const QString& XFileStruct<Ty>::name() const
{
	return m_name;
}

template<typename Ty>
inline void XFileStruct<Ty>::setData(const Ty& data)
{
	XMultiWayTree<Ty>::setData(data,0);
}

template<typename Ty>
const Ty& XFileStruct<Ty>::data() const
{
	return XMultiWayTree<Ty>::data(0);
}
#endif // !Directory_H
