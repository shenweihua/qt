﻿#include"XZip.h"
//目录
//在压缩数据中写入目录信息
void XZip::writeGZipDirectoryInof()
{
	DirectoryInof info = { m_DirectoryName.size(),m_file.size() };//目录信息
	size_t currentSize = m_zipData.size();//当前字节大小
	m_zipData.resize(currentSize + sizeof(DirectoryInof));
	DirectoryInof* LPinfo = (DirectoryInof*)(&m_zipData[0] + currentSize);
	*LPinfo = info;//写入目录信息
	writeGZipData(m_DirectoryName.c_str(), m_DirectoryName.size());//写入目录名字
}
//读取目录信息
size_t XZip::readGZipDirectoryInof(const char* data, const size_t size, size_t offset)
{
	DirectoryInof* LPinfo = (DirectoryInof*)(data + offset);
	m_fileSize = LPinfo->fileCount;//调整文件信息个数
	m_file.resize(LPinfo->fileCount);
	offset += sizeof(DirectoryInof);//调整偏移后面读取名字
	if (LPinfo->DirectoryNameSize != 0)
	{
		std::vector<char> name;
		offset = readUnZipData(data, size, offset, LPinfo->DirectoryNameSize, name);
		name.push_back('\0');
		m_DirectoryName = &name[0];
	}
	return offset;
}
//从文件读取读取目录信息
size_t XZip::readGZipDirectoryInof(std::fstream& readFile)
{
	//获取目录信息
	DirectoryInof info;
	if (readFile.read((char*) & info, sizeof(DirectoryInof)).gcount() != sizeof(DirectoryInof))
	{
		return 0;
	}
	m_fileSize = info.fileCount;//调整文件信息个数
	m_file.resize(info.fileCount);
	//读取目录名字
	m_DirectoryName.resize(info.DirectoryNameSize);
	if (readFile.read(&m_DirectoryName[0], info.DirectoryNameSize).gcount() != info.DirectoryNameSize)
	{
		return 0;
	}
	return readFile.tellg();
}