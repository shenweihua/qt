﻿#include "XZip.h"
#include"XAlgorithm.h"
#include<fstream>
void XZip::setDirectory(std::string name)
{
	m_DirectoryName = name;
}

void XZip::addFile(std::string name, const char* file, const size_t size)
{
	Info info;
	info.name = name;
	info.filePointer = (char*)file;
	info.fileSize = size;
	m_file.push_back(info);
}
void XZip::addFile(std::string filePath)
{
	if (!IsFileExist(filePath))
		return;
	//打开待压缩的文件
	std::fstream read(filePath, std::ios::in | std::ios::binary | std::ios::ate);
	Info info;
	int nSel = filePath.rfind("/");
	info.name = filePath.substr(nSel+1);
	info.filePath = filePath;
	info.fileSize = read.tellg();
	m_file.push_back(info);
	read.close();
}
std::vector<char>& XZip::gzipData()
{
	return m_zipData;
}
const std::vector<Info>& XZip::unzipDatas()
{
	return m_file;
}
const std::string& XZip::directory()
{
	return m_DirectoryName;
}
void XZip::clear()
{
	tree.clear();
	m_Type = 0;
	m_zipData.clear();
	m_DirectoryName.clear();
	m_file.clear();
	m_fileSize = 0;
	m_file_nSel = 0;
	m_readOffset = 0;
}
void XZip::setGZipDictionaries()
{
	//写入目录名字
	tree.addDictionaries(m_DirectoryName.c_str(), m_DirectoryName.size());
	for (auto&data:m_file)
	{
		//写入文件名字
		tree.addDictionaries(data.name.c_str(), data.name.size());
		//指针指向的数据
		if (data.filePointer != NULL)
		{
			tree.addDictionaries(data.filePointer,data.fileSize);
		}
		//文件路径的方式读取
		else if (!data.filePath.empty())
		{
			tree.addDictionaries(data.filePath,1024);
		}
	}
}