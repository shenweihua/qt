﻿#ifndef XZIP_MACRO_H
#define XZIP_MACRO_H
#include<vector>
// 0  0  0  0  0  0  0  0  
// 1  0  1  0  0  0  0  0  普通压缩包
// 1  0  1  0  0  0  0  1  递归压缩包，需要递归解压
// 是压缩文件   
// 
//是压缩文件
#define GZIP			(unsigned char)160       
//文件/普通压缩包
#define GZIPUSER		(unsigned char)0
//目录/递归压缩包，需要递归解压
#define GZIPSYSTEM   (unsigned char)1
using XZipData = std::vector<char>;//数据
//目录信息
struct DirectoryInof
{
	unsigned short int DirectoryNameSize;//目录名字大小
	size_t fileCount;//文件数量
};
//文件信息
struct FileInfo
{
	unsigned short int fileNameSize;//文件名字大小
	size_t fileSize;//文件大小
};
struct Info
{
	std::string name;//文件名
	//unsigned short int fileNameSize;//文件名字大小
	XZipData unZipfile;//解压的文件数据
	std::string filePath;//待压缩/解压的文件路径
	char* filePointer=NULL;//待压缩的文件数据指针
	size_t fileSize=0;//待压缩/解压的文件大小
};
#endif // !XZIP_MACRO_H
