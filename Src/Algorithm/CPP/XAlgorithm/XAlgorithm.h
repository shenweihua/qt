﻿#ifndef XALGORITHM_H
#define XALGORITHM_H
#include<stdio.h>
#include<iostream>
#define PRINT(fmt,...) printf("[FILE:%s][FUNC:%s][LINE:%d]\n->"fmt"\n",__FILE__,__FUNCTION__,__LINE__,__VA_ARGS__)
#define isNULLInfo(args,str) args,#args,str ,__FUNCTION__,__FILE__,__LINE__
#define ISNULL(args,str)(isNULL(isNULLInfo(args,str)))
bool isNULL(const void* args/*参数数值*/, const char* argsName/*参数名字*/, const char* str/*附加参数*/, const char* funcName/*函数名字*/, const char* filePath/*所在文件路径*/, int line/*所在行号*/);
//延迟毫秒
void XDelay(const size_t msec);
//判断文件是否存在
bool IsFileExist(const std::string& name);
//四舍五入
double round(double number, int decimals);
#endif // !XALGORITHM_H

