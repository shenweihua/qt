﻿#ifndef _STRINGMAP_H_
#define _STRINGMAP_H_
#include<string>
#include<vector>
using namespace std;
class StringMap
{
public:
	StringMap();
	~StringMap();
	void insert(const string& name,const string&val );
	void erase(const string& name);
	const string getString(const string& prefix,const string& separator,const string& suffix);
	string& operator[](const string& name);
	vector<pair<string, string>>& operator()();
	void clear();
private:
	vector<pair<string, string>> GetParamStr;
};
#endif // !_FINDSTR_H_

