﻿#include "XStringOperation.hpp"

#ifdef QT_VERSION
const vector<QString> XStringOperation::strtok(const QString& Text, const QString& Delimiter)
{
    vector<QString> ret;
    if (Text.isEmpty() || Delimiter.isEmpty())
        return ret;
    int len = strlen(Text.toUtf8().data());
    char* buf = new char[len + 1] {0};
    ::strcpy(buf, Text.toUtf8().data());
    for (char* p = ::strtok(buf, Delimiter.toLocal8Bit().data()); p != NULL; p = ::strtok(NULL, Delimiter.toLocal8Bit().data()))
    {
        ret.push_back(p);
    }
    delete[] buf;
    return ret;
}
QStringList XStringOperation::strtok(const QString& Text, const QString& Delimiter, const bool space)
{
    QStringList list;
    size_t size = Text.size();
    size_t start=0;
    size_t nSel=0;
    //qInfo() << Text;
   do
    {
       nSel =Text.indexOf(Delimiter, start);
       if (nSel == -1)//没找到
       {
           return list;
       }
       if (nSel ==start && space)
           list << QString();
       else
           list << Text.mid(start, nSel-start);
       start = nSel + Delimiter.size();
   } while (start <size);
    return list;
}
#endif
const vector<string> XStringOperation::strtok(const string& Text, const string& Delimiter)
{
    vector<string> ret;
    if (Text.empty() || Delimiter.empty())
        return ret;
    char* buf = new char[Text.size() + 1] {0};
    ::strcpy(buf, Text.c_str());
    for (char* p = ::strtok(buf, Delimiter.c_str()); p != NULL; p = ::strtok(NULL, Delimiter.c_str()))
    {
        ret.push_back(p);
    }
    delete[] buf;
    return ret;
}

const QList<QStringList> XStringOperation::regex_extract( const QString& regex,  const QString& PlainText)
{
    QList<QStringList> ret;
    return regex_extract(regex,PlainText,ret);
}

QList<QStringList>& XStringOperation::regex_extract( const QString& regex, const QString& PlainText, QList<QStringList>& list)
{
    list.clear();
    //list.resize(size);
    std::string Text = PlainText.toStdString();
    smatch mat;//用作匹配的对象
    string::const_iterator start = Text.begin();//起始位置
    string::const_iterator end = Text.end();//结束位置
    size_t count = 0;//提取个数
    size_t size = 0;
    while (std::regex_search(start, end, mat, std::regex(regex.toStdString())))
    {
        size = mat.size()-1;
        for (size_t i = 0; i < size; i++)
        {
            string temp(mat[i + 1].first, mat[i + 1].second);
            list[i] << temp.c_str();
        }
        start = mat[0].second;//改变起始位置
        count++;
    }
    //qInfo() << count;
    return list;
}

QString XStringOperation::regex_replace(const QString& regex, const QString& Text, const QStringList& replace)
{
    std::string textCopy=Text.toStdString();
   // textCopy.resize(textCopy.size()+ );
    smatch mat;//用作匹配的对象
    string::const_iterator start = textCopy.begin();//起始位置
    string::const_iterator end = textCopy.end();//结束位置
    QList<QList<QPair<size_t, size_t>>> pos;
    size_t size = 0;
    while (std::regex_search(start, end, mat, std::regex(regex.toStdString())))
    {
        size= mat.size();
        QList<QPair<size_t, size_t>> temp;
        for (size_t i = 1; i < size; i++)
        {
            temp << QPair<size_t, size_t>(mat[i].first - textCopy.begin(), mat[i].second- mat[i].first);
        }
        pos << temp;
        start = mat[0].second;//改变起始位置
    }
    //qInfo() << pos;
    qint64 offset = 0;
    for (auto& row:pos)
    {
        for (size_t i=0;i<row.size();i++)
        {
            textCopy.replace(row[i].first + offset, row[i].second, replace[i].toStdString());
            offset += (replace[i].toStdString().size() - row[i].second);
        }
    }
    return QString(textCopy.c_str());
}
