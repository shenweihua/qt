﻿#ifndef XQTIMERTASK_H
#define XQTIMERTASK_H
#include<QDateTime>
//定时器任务
class XQTimerTask
{
public:
	XQTimerTask()=default;
	XQTimerTask(qint64 sec, std::function<void()> task);
	XQTimerTask(QDateTime runTime, qint64 sec, std::function<void()> task);
	XQTimerTask(QDateTime runTime, qint64 count, qint64 sec, std::function<void()> task);
	XQTimerTask(QDateTime runTime, qint64 count, qint64 sec, std::function<void()> task,bool mainThread);
	XQTimerTask(QDateTime runTime, qint64 count, std::function<QDateTime(XQTimerTask*)> upTimeFunc,  std::function<void()> task, bool mainThread);
	//是否启用
	bool isEnabled() const;
	//是否可以运行任务
	bool isRunTask()const;
	//时间超时了？
	bool isTimeOut()const;
	//次数是否用完了
	bool isCountFinish()const;
	//是否在主线程运行
	bool isMainThreadRun()const;
	//获取剩余运行次数(-1表示永久运行)
	qint64 runCount()const;
	//次数用尽自动删除
	bool isAutoDelete()const;
	//获取运行时间
	QDateTime runTime()const;
	//设置定时时间-秒
	qint64 timerSec()const;
public:
	//执行任务
	void runTask();
	//设置是否启用
	void setEnabled(bool enabled);
	//设置是否在主线程运行
	void setMainThreadRun(bool main);
	//设置运行次数(-1表示永久运行)
	void setRunCount(qint64 count=-1);
	//设置到零次自动删除
	void setAutoDelete(bool dl);
	//设置运行时间(首次执行任务时间)
	void setRunTime(QDateTime time);
	//设置定时间隔(秒)
	void setTimerSec(qint64 sec);
	//设置更新时间方法(返回需要下一次执行的时间将会覆盖定时时间的方法)
	void setUpTimeFunc(std::function<QDateTime(XQTimerTask*)> func);
	//设置任务
	void setTask(std::function<void()> task);
	void setTask(std::function<void(XQTimerTask*)> task);
protected:
	bool m_enable = true;//启用
	qint64 m_count = -1;//循环次数    -1表示永久循环
	bool m_autoDelete=false;
	//std::atomic<qint64> m_count = -1;//循环次数    -1表示永久循环
	QDateTime m_runTime;//执行任务时间
	std::function<QDateTime(XQTimerTask*)> m_upTimeFunc = nullptr;
	qint64 m_timerSec=0;//下一次间隔时间秒
	bool m_mainThreadRun = false;//是否在主线程运行
	std::function<void(XQTimerTask*)> m_task=nullptr;//执行的任务
};
#endif