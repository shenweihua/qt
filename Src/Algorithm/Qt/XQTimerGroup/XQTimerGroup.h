﻿#ifndef XQTIMER_H
#define XQTIMER_H
#include"XQHead.h"
#include"XQTimerTask.h"
#include<QThread>
#include<QReadWriteLock>
#include<QWaitCondition>
//定时器轮询状态
enum class TimerPollState
{
	poll,//轮询
	sleep,//休眠
	wake,//唤醒
	run,//运行任务
	remove//删除任务
};
//定时器任务组类
class XQTimerGroup:public QThread
{
	Q_OBJECT
public:
	XQTimerGroup(QObject* parent = nullptr);
	~XQTimerGroup();
	//获取轮询时间(毫秒)
	int pollTime()const;
	//获取任务
	XQTimerTask* task(int id);
public:
	//设置轮询时间
	void setPollTime(int msec);
	//添加一个任务  id从0开始 -1表示自动id count  运行次数(-1永久运行)
	int addTask(const XQTimerTask& task, int id = -1);
	int addTask(QDateTime runTime,qint64 sec/*间隔-秒*/, std::function<void()> task,qint64 count/*运行次数(-1永久运行)*/, bool mainThread = false, int id = -1);
	int addTask(qint64 sec/*间隔-秒*/, std::function<void()> task,bool mainThread=false,int id = -1);
	int addTask(QDateTime runTime, std::function<QDateTime(XQTimerTask*)> upTimeFunc/*更新方法*/, std::function<void()> task, qint64 count/*运行次数(-1永久运行)*/, bool mainThread = false, int id = -1);
	bool removeTask(int id);
	void clearTask();
signals://信号
	void runTimeUpData(int id,QDateTime dataTime);
	void pollState(TimerPollState state,int id=-1);
protected://
	void init();
	//获取一个Id
	int getId(int id);
	//定时器的任务执行函数
	void timerSlot();
	void run()override;
protected://变量
	int m_pollTime = 500;//轮询时间毫秒
	QDateTime m_time;//时间
	QReadWriteLock m_lock;//管理任务组的互斥量
	QMap<int,XQTimerTask> m_taskGroup;//任务组
	QWaitCondition m_condition;//没任务的时候休眠线程
};
#endif // !XQTimer_H
