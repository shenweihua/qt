﻿#ifndef XQPANDASREADCSV_H
#define XQPANDASREADCSV_H
#include<QObject>
#include<QFile>
#include"XQPandasItem.h"
class QTextCodec;
class XQPandasReadCSV
{
public:
	XQPandasReadCSV() = default;
	XQPandasReadCSV(const QString& local, const QString& coding = "utf8");
public:
	// 读取csv文件到缓冲区
	bool readfile(const QString & path, const QString & coding = "utf8");
	class read_csv_iterator
	{
	public:
		read_csv_iterator();
		read_csv_iterator(const QString& path, QTextCodec* codec, qint64 pos=0);
		//文件路径
		QString filePath()const;
		// 解引用操作符重载
		XQPandasItem& operator*();
		// 后缀递增操作符重载
		read_csv_iterator operator++(int);
		read_csv_iterator& operator++();
		// 相等性比较操作符重载
		bool operator==(const read_csv_iterator& other) const;
		// 不等性比较操作符重载
		bool operator!=(const read_csv_iterator& other) const;
	protected:
		QTextCodec* m_codec = nullptr;
		QFile m_file;
		XQPandasItem m_buffLine;//一行数据
		qint64 m_pos=0;
		bool m_end = false;
	};
	read_csv_iterator begin();
	read_csv_iterator end();
protected:
	QTextCodec* m_codec = nullptr;
	QString m_path;
};
#endif // !XQPandasCSV_H
