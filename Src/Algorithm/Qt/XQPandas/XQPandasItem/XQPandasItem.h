﻿#ifndef XQPANDASITEM_H
#define XQPANDASITEM_H
#include"XQHead.h"
#include<QOBject>
#include<QVariant>
#include<QDebug>
class XQPandasItem
{
public:
	XQPandasItem();
	XQPandasItem(const QStringList& list, QHash<QString, int>* title=nullptr);
	XQPandasItem(const QVariantList& list);
	XQPandasItem(const XQPandasItem& other);
	QStringList toStringList()const ;
	QVariantList& data();
	const QVariantList& data()const;
public:
	void setTitle(QHash<QString, int>*title);
	void clear();
public:
	const QVariant& at(qsizetype i) const;
	qsizetype size() const;
	QVariantList::iterator begin();
	QVariantList::const_iterator begin()const;
	QVariantList::reverse_iterator rbegin();
	QVariantList::const_reverse_iterator rbegin()const;
	QVariantList::iterator end();
	QVariantList::const_iterator end()const;
	QVariantList::reverse_iterator rend();
	QVariantList::const_reverse_iterator rend()const;
	QVariant& front();
	const QVariant& front()const;
	QVariant& back();
	const QVariant& back()const;
public:
	friend QDebug operator<<(QDebug debug, const XQPandasItem& Obj);
	XQPandasItem& operator<<(QVariant&& other);
	XQPandasItem& operator<<(const QVariant& other);
	XQPandasItem& operator<<(QVariantList&& other);
	XQPandasItem& operator<<(const QVariantList& other);
	XQPandasItem& operator<<(XQPandasItem&& other);
	XQPandasItem& operator<<(const XQPandasItem& other);
	QVariant& operator[](qsizetype i);
	const QVariant& operator[](qsizetype i)const;
	QVariant& operator[](const QString& i);
	const QVariant& operator[](const QString& i)const;
protected:
	QVariant m_Variant;
	QVariantList m_data;//数据
	QHash<QString, int>* m_titleIndex=nullptr;//标题
};
#endif // ! XQPandasItem_H
