﻿#ifndef XQSETTINGSINFO_H
#define XQSETTINGSINFO_H
#include<QObject>
#include<atomic>
#include<QDir>
#include<QReadWriteLock>
#include<QMap>
#include"XQHead.h"
//系统设置信息
class XQSettingsInfo:public QObject
{
	Q_OBJECT
public:
	XQSettingsInfo(QObject* parent=nullptr);
	virtual ~XQSettingsInfo();
	//获取自启状态
	bool autorun()const;
	//获得自启的启动方式
	int autoStartType()const;
	//显示托盘菜单
	bool isShowSystemTray()const;
	//打开悬浮
	bool openSuspension()const;
	//记住主窗口位置 
	bool saveMainWindowPosition()const;
	//获得缓存路径
	QString bufferPath();
	//获得自动登录
	bool autoLogin(const QString& account);
	//获得保存密码
	bool savePassword(const QString& account);
	//获取用户名和密码
	QMap<QString, QByteArray> account();
	//获取当前账号的密码
	QByteArray password(const QString& account);
	//获取当前账号
	QString currentAccount();
	//获取值
	QVariant value(const QString& key, const QVariant& defaultValue = QVariant()) const;
	//获取QSettings指针
	QSettings* Settings()const;
public slots:
	//设置开机自启
	void setAutorun(bool yes);
	//设置自启时的启动方式
	void setAutoStartType(int type);
	//设置显示托盘图标
	void setShowSystemTray(bool show);
	//设置打开悬浮窗状态
	void setOpenSuspension(bool open);
	//设置保存主窗口位置
	void setSaveMainWindowPosition(bool yes);
	//设置缓存路径
	void setBufferPath(const QString& path);
	//设置自动登录
	void setAutoLogin(const QString& account,bool yes);
	//设置保存密码
	void setSavePassword(const QString& account,bool yes);
	//设置用户账号和密码
	void setAccount(const QString& account,const QByteArray& password= QByteArray());
	//删除账号
	void removeAccount(const QString& account);
	//清空账号
	void clearAccount();
	//设置当前账号(必须前面已经设置过了)
	void setCurrentAccount(const QString& account);
	//写入更新账号配置文件
	void updataAccountsFiles();
	//设置保存文件-ini文件
	virtual void setSaveFile(const QString& file= QDir::currentPath() + "/Settings.ini");
	//设置值
	void setValue(const QString& key, const QVariant& value);
protected:
	QSettings* m_setting = nullptr;
	//login 登录
	QString m_currentAccount;//当前账号
	QMap<QString, bool> m_autoLogin;//自动登录
	QMap<QString, bool> m_savePassword;//保存密码
	QMap<QString, QByteArray> m_Account;//账号和密码
	QReadWriteLock m_m_AccountLock;
	//系统
	std::atomic<bool> m_autoStart ;//开机自启
	std::atomic<int> m_autoStartType;//自启-启动方式
	std::atomic<bool> m_systemTray;//托盘图标
	std::atomic<bool> m_suspension;//打开悬浮窗
	std::atomic<bool> m_saveMainWindowPosition;//记住主窗口位置
	QString m_bufferPath;//缓存路径
	QReadWriteLock m_bufferPathLock;
};
#endif // !XQSystemSetupInfo_H
