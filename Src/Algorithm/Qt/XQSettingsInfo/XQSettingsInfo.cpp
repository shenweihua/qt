﻿#include"XQSettingsInfo.h"
#include"XQEventFilterObject.h"
#include"XQArguments.h"
#include<QSettings>
#include<QWidget>
#include<QFile>
#include<QDebug>
XQSettingsInfo::XQSettingsInfo(QObject* parent)
	:QObject(parent)
{
}

XQSettingsInfo::~XQSettingsInfo()
{
}

bool XQSettingsInfo::autorun() const
{
	return m_autoStart;
}

int XQSettingsInfo::autoStartType() const
{
	return m_autoStartType;
}

bool XQSettingsInfo::isShowSystemTray() const
{
	return m_systemTray;
}

bool XQSettingsInfo::openSuspension() const
{
	return m_suspension;
}

bool XQSettingsInfo::saveMainWindowPosition() const
{
	return m_saveMainWindowPosition;
}

QString XQSettingsInfo::bufferPath() 
{
	QReadLocker lock(&m_bufferPathLock);
	return m_bufferPath;
}

bool XQSettingsInfo::autoLogin(const QString& account)
{
	QReadLocker lock(&m_m_AccountLock);
	return m_autoLogin[account];
}

bool XQSettingsInfo::savePassword(const QString& account) 
{
	QReadLocker lock(&m_m_AccountLock);
	return m_savePassword[account];
}

QMap<QString, QByteArray> XQSettingsInfo::account()
{
	QReadLocker lock(&m_m_AccountLock);
	return m_Account;
}

QByteArray XQSettingsInfo::password(const QString& account)
{
	QReadLocker lock(&m_m_AccountLock);
	return m_Account[account];
}

QString XQSettingsInfo::currentAccount()
{
	QReadLocker lock(&m_m_AccountLock);
	return m_currentAccount;
}

QVariant XQSettingsInfo::value(const QString& key, const QVariant& defaultValue) const
{
	if(m_setting!=nullptr)
		return m_setting->value(key,defaultValue);
	return QVariant();
}

QSettings* XQSettingsInfo::Settings() const
{
	return m_setting;
}

void XQSettingsInfo::setAutoStartType(int type)
{
	m_autoStartType = type;
	m_setting->setValue("autoStartType", type);
}

void XQSettingsInfo::setShowSystemTray(bool show)
{
	m_systemTray = show;
	m_setting->setValue("showSystemTray", show);
}

void XQSettingsInfo::setOpenSuspension(bool yes)
{
	m_suspension = yes;
	m_setting->setValue("openSuspension", yes);
}

void XQSettingsInfo::setSaveMainWindowPosition(bool yes)
{
	m_saveMainWindowPosition = yes;
	m_setting->setValue("saveMainWindowPosition", yes);
}


void XQSettingsInfo::setBufferPath(const QString& path)
{
	QWriteLocker lock(&m_bufferPathLock);
	m_bufferPath = path;
	m_setting->setValue("bufferPath", m_bufferPath);
}

void XQSettingsInfo::setAutoLogin(const QString& account,bool yes)
{
	QWriteLocker lock(&m_m_AccountLock);
	m_autoLogin[account] = yes;
}

void XQSettingsInfo::setSavePassword(const QString& account,bool yes)
{
	QWriteLocker lock(&m_m_AccountLock);
	m_savePassword[account] = yes;
}

void XQSettingsInfo::setAccount(const QString& account, const QByteArray& password)
{
	QWriteLocker lock(&m_m_AccountLock);
	m_Account[account] = password;
}

void XQSettingsInfo::removeAccount(const QString& account)
{
	QWriteLocker lock(&m_m_AccountLock);
	m_Account.remove(account);
}

void XQSettingsInfo::clearAccount()
{
	QWriteLocker lock(&m_m_AccountLock);
	m_Account.clear();
	m_savePassword.clear();
	m_autoLogin.clear();
}

void XQSettingsInfo::setCurrentAccount(const QString& account)
{
	QWriteLocker lock(&m_m_AccountLock);
	if(m_Account.find(account)!=m_Account.end())
	{
		m_currentAccount = account;
		m_setting->setValue("currentAccount", m_currentAccount);
	}
}

void XQSettingsInfo::updataAccountsFiles()
{
	m_setting->remove("accounts");
	m_setting->beginWriteArray("accounts");
	int nSel = 0;
	for (auto it = m_Account.begin(); it !=m_Account.end(); it++)
	{
		m_setting->setArrayIndex(nSel);
		m_setting->setValue("account",it.key());
		m_setting->setValue("password", it.value());
		m_setting->setValue("autoLogin", m_autoLogin[it.key()]);
		m_setting->setValue("savePassword", m_savePassword[it.key()]);
		++nSel;
	}
	m_setting->endArray();
}

void XQSettingsInfo::setSaveFile(const QString& file)
{
	if (!QFile(file).exists())
		return;
	if (m_setting != nullptr)
		m_setting->deleteLater();
	m_setting=new QSettings (file, QSettings::IniFormat,this);
	XQArguments::setSettingsPath(file);
	//读取数据
	m_autoStart=m_setting->value("autorun").toBool();
	m_autoStartType= m_setting->value("autoStartType").toInt();
	m_systemTray=m_setting->value("showSystemTray",true).toBool();
	m_suspension = m_setting->value("openSuspension").toBool();
	m_saveMainWindowPosition= m_setting->value("saveMainWindowPosition",true).toBool();
	m_bufferPath=m_setting->value("bufferPath", QDir::currentPath()).toString();
	//读取账号和密码
	clearAccount();
	int count=m_setting->beginReadArray("accounts");
	for (int i=0;i<count;i++)
	{
		m_setting->setArrayIndex(i);
		QString account=m_setting->value("account").toString();
		QByteArray password =m_setting->value("password").toByteArray();
		bool autoLogin = m_setting->value("autoLogin").toBool();
		bool savePassword = m_setting->value("savePassword").toBool();
		m_Account[account] = password;
		m_autoLogin[account] = autoLogin;
		m_savePassword[account] = savePassword;
	}
	m_setting->endArray();
	m_currentAccount=m_setting->value("currentAccount").toString();
}

void XQSettingsInfo::setValue(const QString& key, const QVariant& value)
{
	if (m_setting != nullptr)
		m_setting->setValue(key,value);
}

void XQSettingsInfo::setAutorun(bool yes)
{
	m_autoStart = yes;
	m_setting->setValue("autorun", yes);
}
