﻿#ifndef XQGraphicsViewWidget_H
#define XQGraphicsViewWidget_H
#include<QGraphicsView>
#include"XQHead.h"
//图片查看器小部件
class XQGraphicsViewWidget:public QGraphicsView
{
	Q_OBJECT
public:
	XQGraphicsViewWidget(QWidget* parent = nullptr);
	~XQGraphicsViewWidget();
	//获取是否自动释放
	bool autoDelete();
public slots://槽函数
	void addPixmap(const QPixmap& pixmap);
	void addPixmap(const QString& pixmap);
	//开启自动释放
	void setAutoDelete(bool flag);
signals://信号

protected://初始化
	void init();
	//void init_UI();
protected://事件
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* ev)override;
	//滚轮事件
	void wheelEvent(QWheelEvent* ev)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
protected://变量
	QGraphicsScene* m_scene = nullptr;//图片管理
	bool m_autoDelete = false;
};
#endif // !XQGraphicsViewWidget_H
