﻿#include "XQGraphicsViewWidget.h"
#include"XQGraphicsViewWidget.h"
#include<QResizeEvent>
#include<QWheelEvent>
void XQGraphicsViewWidget::resizeEvent(QResizeEvent* event)
{
    fitInView(m_scene->sceneRect(), Qt::KeepAspectRatio);
}

void XQGraphicsViewWidget::wheelEvent(QWheelEvent* ev)
{
    int delta = ev->angleDelta().y();
    // 计算缩放因子
    qreal scaleFactor = qPow(2.0, delta / 240.0);
    scaleFactor = qMin(scaleFactor, 10.0);     // 缩放因子最大为10倍
    scaleFactor = qMax(scaleFactor, 0.1);       // 缩放因子最小为0.1倍

    scale(scaleFactor, scaleFactor);
}

void XQGraphicsViewWidget::closeEvent(QCloseEvent* event)
{
    if (m_autoDelete)
        deleteLater();
}
