﻿#include "XQGraphicsViewWidget.h"
#include<QGraphicsScene>
#include<QFileInfo>
XQGraphicsViewWidget::XQGraphicsViewWidget(QWidget* parent)
	:QGraphicsView(parent)
{
	init();
}

XQGraphicsViewWidget::~XQGraphicsViewWidget()
{
}
bool XQGraphicsViewWidget::autoDelete()
{
	return m_autoDelete;
}
void XQGraphicsViewWidget::addPixmap(const QString& pixmap)
{
	addPixmap(QPixmap(pixmap));
	setWindowTitle(QFileInfo(pixmap).fileName());
}
void XQGraphicsViewWidget::setAutoDelete(bool flag)
{
	m_autoDelete = false;
}
void XQGraphicsViewWidget::init()
{
	m_scene = new QGraphicsScene(this);
	setScene(m_scene);
	setDragMode(QGraphicsView::ScrollHandDrag);
	setWindowTitle("图像查看");
}
void XQGraphicsViewWidget::addPixmap(const QPixmap& pixmap)
{
	m_scene->addPixmap(pixmap);
	fitInView(m_scene->sceneRect(), Qt::KeepAspectRatio);
}