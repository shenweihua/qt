﻿#ifndef XQCHANGEPERCENTDOCKWIDGET_H
#define XQCHANGEPERCENTDOCKWIDGET_H
#include"XQHead.h"
#include"XQDockWidget.h"

//涨跌值浮动信息显示窗口
class XQChangePercentDockWidget :public XQDockWidget
{
	Q_OBJECT
public:
	XQChangePercentDockWidget(QWidget*parent=nullptr);
	XQChangePercentDockWidget(const QString& title,QWidget* parent = nullptr);
	~XQChangePercentDockWidget();
public://获取一些信息
	XQTimeChartsWidget* charts()const;
	XQTimeDoubleDataModel* model()const;
	QString timeFormat()const;
public://设置 
	void setModel(XQTimeDoubleDataModel* model);
	void setTimeFormat(const QString&format);
	//刷新ui
	virtual void updataUI();
	//清空表格
	virtual void clearTable();
	//清空用户的表格
	virtual void clearUserTable();
	//显示图表
	virtual void showCharts();
signals://信号
	void openCharts();
protected://初始化
	virtual void init();
	virtual void init_ui();
	virtual void init_tableWidget();
	virtual void init_charts();
	virtual QLayout* init_dateSelect();
protected://
	//表格弹出菜单
	virtual void contextMenuRequested();
	//添加一行文本
	bool addRowText(int row, const QDateTime& start, const QDateTime& end);
	bool addRowText(int row, XQHistoryTrendItem* item);
	//用户添加一行
	void userAddRow();
protected:
	XQTimeDoubleDataModel* m_model = nullptr;//模型
	QString m_timeFormat = "yyyy-MM-dd";
	QLabel* m_timeInfo = nullptr;//时间信息
	QTableWidget* m_tableWidget = nullptr;//表格
	QDateTimeEdit* m_startDate = nullptr;//开始日期选择
	QDateTimeEdit* m_endDate = nullptr;//开始日期选择
	QPushButton* m_statsBtn = nullptr;//自定义统计按钮
	QPushButton* m_chartBtn = nullptr;//图表按钮
	XQTimeChartsWidget* m_charts = nullptr;//图表
};
#endif // !XQChangePercentDockWidget_H
