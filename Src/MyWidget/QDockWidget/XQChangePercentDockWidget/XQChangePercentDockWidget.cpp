﻿#include"XQChangePercentDockWidget.h"
#include"XQTitleBarWidget.h"
#include"XQTimeDoubleDataModel.h"
#include"XQTimeChartsWidget.h"
#include"XQTimeSelectDockWidget.h"
#include<QLineSeries>
#include<QTableWidget>
#include<QHeaderView>
#include<QDateTimeEdit>
#include<QPushButton>
#include<QBoxLayout>
#include<QLabel>
#include<QMenu>
XQChangePercentDockWidget::XQChangePercentDockWidget(QWidget* parent) :XQDockWidget(parent)
{
	init();
}

XQChangePercentDockWidget::XQChangePercentDockWidget(const QString& title, QWidget* parent) :XQDockWidget(title,parent)
{
	init();
}

XQChangePercentDockWidget::~XQChangePercentDockWidget()
{
}


XQTimeChartsWidget* XQChangePercentDockWidget::charts() const
{
	return m_charts;
}

XQTimeDoubleDataModel* XQChangePercentDockWidget::model() const
{
	return m_model;
}

QString XQChangePercentDockWidget::timeFormat() const
{
	return m_timeFormat;
}

void XQChangePercentDockWidget::setModel(XQTimeDoubleDataModel* model)
{
	/*if (m_model)
		m_model->deleteLater();*/
	m_model = model;
	
	if (model == nullptr)
	{
		m_statsBtn->setEnabled(false);
		m_chartBtn->setEnabled(false);
		return;
	}

	if (!m_model->datas().isEmpty())
	{
		m_charts->selectWidget()->startDateTimeEdit()->setDateTime(m_model->datas().begin().key());
		m_charts->selectWidget()->endDateTimeEdit()->setDateTime((--m_model->datas().end()).key());
	}
}

void XQChangePercentDockWidget::setTimeFormat(const QString& format)
{
	m_timeFormat = format;
}


void XQChangePercentDockWidget::updataUI()
{
	auto Series= new QLineSeries(m_charts);
	m_charts->clearModel();
	m_charts->addModel(windowTitle(), m_model);
	m_charts->addCharts(windowTitle(), Series);
	Series->setName(windowTitle());//设置线段名字
	auto trend =m_model->getHistoryTrend();
	if (trend.isNull)//是空的
	{
		setEnabled(false);
		m_statsBtn->setEnabled(false);
		m_chartBtn->setEnabled(false);
		return;
	}
	//添加显示
	addRowText(0, &trend.OneWeek);
	addRowText(1, &trend.OneMonth);
	addRowText(2, &trend.ThreeMonth);
	addRowText(3, &trend.SixMonth);
	addRowText(4, &trend.OneYear);
	addRowText(5, &trend.TwoYear);
	addRowText(6, &trend.ThreeYear);
	addRowText(7, &trend.FiveYear);
	addRowText(8, &trend.ThisYear);
	addRowText(9, &trend.Hitherto);
	//设置一些属性
	auto dates = m_model->datas().keys();//所有的日期
	m_timeInfo->setText(QString("%1 %2").arg(dates.front().toString(m_timeFormat)).arg(dates.back().toString(m_timeFormat)));
	auto startDate = dates.front();//已有的最近时间
	auto endDate = dates.back();//已有的最近时间
	m_startDate->setDateTime(startDate);
	m_endDate->setDateTime(endDate);
	m_startDate->setMinimumDateTime(startDate);
	m_startDate->setMaximumDateTime(endDate);
	m_endDate->setMinimumDateTime(startDate);
	m_endDate->setMaximumDateTime(endDate);
	setEnabled(true);
	m_statsBtn->setEnabled(true);
	m_chartBtn->setEnabled(true);
}

void XQChangePercentDockWidget::clearTable()
{
	m_tableWidget->clearContents();
	clearUserTable();
}

void XQChangePercentDockWidget::clearUserTable()
{
	//到10
	int rowCount = m_tableWidget->rowCount();
	for (size_t i = rowCount - 1; i > 9 && rowCount > 10; i--)
	{
		m_tableWidget->removeRow(i);
	}
}

void XQChangePercentDockWidget::showCharts()
{
	//m_charts->setWindowTitle(windowTitle());
	/*m_charts->AxisY()->setTitleText("收盘价");*/
	m_charts->show();
}

void XQChangePercentDockWidget::init()
{
	//m_model = new XQTimeDoubleDataModel(this);
	init_tableWidget();
	init_charts();
	XQDockWidget::init();
}

void XQChangePercentDockWidget::init_ui()
{
	XQDockWidget::init_ui();
	/*m_title->setMouseMoveWidget(false);*/
	auto widget = new QWidget();
	m_chartBtn = new QPushButton("查看图表", this);
	m_chartBtn->setEnabled(false);
	m_timeInfo = new QLabel(this);
	m_timeInfo->setAlignment(Qt::AlignCenter);
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom);
	layout->setContentsMargins(10, 10, 10, 0);
	layout->addWidget(m_timeInfo);
	layout->addLayout(init_dateSelect());
	layout->addWidget(m_tableWidget);
	layout->addWidget(m_chartBtn);
	widget->setLayout(layout);
	setWidget(widget);
	//参函数连接
	connect(m_chartBtn, &QPushButton::pressed, this, &XQChangePercentDockWidget::showCharts);
}

void XQChangePercentDockWidget::init_tableWidget()
{
	m_tableWidget = new QTableWidget(this);
	m_tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_tableWidget, &QWidget::customContextMenuRequested, this, &XQChangePercentDockWidget::contextMenuRequested);
	//m_tableWidget->horizontalHeader()->setHidden(true);   // 隐藏水平标题
	//m_tableWidget->verticalHeader()->setHidden(true);    // 隐藏垂直标题
	m_tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置只读
	m_tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	m_tableWidget->setColumnCount(1);
	m_tableWidget->setRowCount(10);
	QStringList title = { "近一周","近一月","近三月","近六月","近一年","近两年","近三年","近五年","今年来","成立来" };
	for (int i = 0; i < title.size(); i++)
	{//设置垂直标题
		m_tableWidget->setVerticalHeaderItem(i, new QTableWidgetItem(title[i]));
	}
	m_tableWidget->setHorizontalHeaderLabels({ "涨跌幅" });
}

void XQChangePercentDockWidget::init_charts()
{
	m_charts = new XQTimeChartsWidget();
	
	connect(this,&QWidget::destroyed, m_charts, &QWidget::deleteLater);
}

QLayout* XQChangePercentDockWidget::init_dateSelect()
{
	m_startDate = new QDateTimeEdit(this);
	m_endDate = new QDateTimeEdit(this);
	m_statsBtn = new QPushButton("添加", this);
	m_statsBtn->setEnabled(false);
	//创建布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom);
	auto layoutOne = new QBoxLayout(QBoxLayout::LeftToRight);
	layoutOne->addWidget(new QLabel("开始日期"));
	layoutOne->addWidget(m_startDate, 1);
	auto layoutTwo = new QBoxLayout(QBoxLayout::LeftToRight);
	layoutTwo->addWidget(new QLabel("结束日期"));
	layoutTwo->addWidget(m_endDate, 1);
	layout->addLayout(layoutOne);
	layout->addLayout(layoutTwo);
	layout->addWidget(m_statsBtn);
	//属性初始化
	m_startDate->setCalendarPopup(true);
	m_endDate->setCalendarPopup(true);
	m_startDate->setDisplayFormat("yyyy年MM月dd日");
	m_endDate->setDisplayFormat("yyyy年MM月dd日");
	//槽函数
	connect(m_statsBtn, &QPushButton::pressed, this, &XQChangePercentDockWidget::userAddRow);
	return layout;
}

void XQChangePercentDockWidget::contextMenuRequested()
{
	auto tablet = m_tableWidget;
	int row = tablet->currentRow();
	if (row == -1)
		return;
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	if (row > 9)
		menu->addAction("删除当前行", [=] {tablet->removeRow(row); });
	if (tablet->rowCount() > 10)
		menu->addAction("清空自定义", this, &XQChangePercentDockWidget::clearUserTable);
	menu->popup(QCursor::pos());
}

bool XQChangePercentDockWidget::addRowText(int row, const QDateTime& start, const QDateTime& end)
{
	QDateTime startOut, endOut;
	auto value = m_model->changePercent(start, end, &startOut, &endOut);
	if (startOut.isNull() || endOut.isNull())
		return false;//失败了
	QString text = QString("%2/%3  %1%").arg(QString::number(value * 100, 'f', 2).rightJustified(6, ' ')).arg(startOut.toString(m_timeFormat)).arg(endOut.toString(m_timeFormat));
	auto item = new QTableWidgetItem(text);
	m_tableWidget->setItem(row, 0, item);
	return true;
}

bool XQChangePercentDockWidget::addRowText(int row, XQHistoryTrendItem* trendItem)
{
	if (trendItem == nullptr || trendItem->endTime.isNull() || trendItem->startTime.isNull())
		return false;
	QString text = QString("%2/%3  %1%").arg(QString::number(trendItem->value * 100, 'f', 2).rightJustified(6, ' ')).arg(trendItem->startTime.toString(m_timeFormat)).arg(trendItem->endTime.toString(m_timeFormat));
	auto item = new QTableWidgetItem(text);
	m_tableWidget->setItem(row, 0, item);
	return true;
}

void XQChangePercentDockWidget::userAddRow()
{
	int row = m_tableWidget->rowCount();
	m_tableWidget->insertRow(row);
	if (addRowText(row, m_startDate->dateTime(), m_endDate->dateTime()))
	{//成功
		m_tableWidget->setVerticalHeaderItem(row, new QTableWidgetItem("自定义"));
	}
	else
	{
		m_tableWidget->removeRow(row);
	}
}
