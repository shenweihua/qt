﻿#ifndef XQLOGDOCKWIDGET_H
#define XQLOGDOCKWIDGET_H
#include"XQDockWidget.h"
#include"XQLog.hpp"
//日志窗口
class XQLogDockWidget:public XQDockWidget
{
	Q_OBJECT
public:
	XQLogDockWidget(const QString& title,QWidget* parent = nullptr);
	XQLogDockWidget(QWidget* parent=nullptr);
	~XQLogDockWidget();
	qint64 maxSize()const;//最大存储数量
public:
	//添加一个日志
	void addLogInfo(const XQLogDate& data);
	void setMaxSize(int size);
	void addLog(const QString& name);
	void addLog(const XQLog& log);
	void removeLog(const QString& name,bool deleteLater=false);
	void removeLog(const XQLog& log, bool deleteLater = false);
	void clearLog(bool deleteLater = false);
protected://初始化
	virtual void init();
	virtual void init_ui();
	virtual void tableWidget_init();
protected://事件
	void showEvent(QShowEvent*)override;
	void closeEvent(QCloseEvent* event)override;
	void resizeEvent(QResizeEvent* event)override;
protected:
	XQTablePagingWidget* m_tableWidget = nullptr;
	qint64 m_maxSize = 99999;//最大显示数量
	QSet<XQLog*>m_log;
};
#endif // !XQLogDockWidget_H
