﻿#include"XQLogDockWidget.h"
#include"XQTablePagingWidget.h"
#include"XQTitleBarWidget.h"
XQLogDockWidget::XQLogDockWidget(const QString& title, QWidget* parent)
	:XQDockWidget(title,parent)
{
	init();
}

XQLogDockWidget::XQLogDockWidget(QWidget* parent)
	:XQDockWidget(parent)
{
	init();
}

XQLogDockWidget::~XQLogDockWidget()
{
}
qint64 XQLogDockWidget::maxSize() const
{
	return m_maxSize;
}
void XQLogDockWidget::addLogInfo(const XQLogDate& data)
{
	XQTablePagingItem item;
	item.push_back(data.key);
	item.push_back(XQLog::logType_toString(data.logType),int(data.logType));
	item.push_back(data.time.toString("yyyy-MM-dd hh:mm:ss"), data.time);
	item.push_back(data.info);
	m_tableWidget->push_frontRow(std::move(item));
	if (m_tableWidget->dataSize() > m_maxSize)
		m_tableWidget->pop_backRow();
	m_title->setWindowTitle(windowTitle() + QString("(%1)").arg(m_tableWidget->dataSize()));
}

void XQLogDockWidget::setMaxSize(int size)
{
	m_maxSize = size;
}
void XQLogDockWidget::addLog(const QString& name)
{
	//创建绑定日志
	auto log = XQLog::Create(name);
	if (m_log.find(log) != m_log.end())
		return;
	connect(log, &XQLog::LogOut, this, &XQLogDockWidget::addLogInfo);
	m_log << log;
}

void XQLogDockWidget::addLog(const XQLog& log)
{
	addLog(log.key());
}

void XQLogDockWidget::removeLog(const QString& name,bool deleteLater)
{
	auto log = XQLog::Global(name);
	if (m_log.find(log) == m_log.end())
		return;
	disconnect(log, &XQLog::LogOut, this, &XQLogDockWidget::addLogInfo);
	m_log.remove(log);
	if (deleteLater)
		XQLog::remove(name);
}

void XQLogDockWidget::removeLog(const XQLog& log, bool deleteLater)
{
	removeLog(log.key(), deleteLater);
}

void XQLogDockWidget::clearLog(bool deleteLater)
{
	for (auto&log:m_log)
	{
		removeLog(log->key(),deleteLater);
	}
}
