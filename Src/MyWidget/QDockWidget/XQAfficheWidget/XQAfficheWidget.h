﻿#ifndef XQAFFICHEWIDGET_H
#define XQAFFICHEWIDGET_H
#include"XQHead.h"
#include<QDockWidget>
//公告栏
class XQAfficheWidget :public QDockWidget
{
	Q_OBJECT
public:
	XQAfficheWidget(QWidget* parent = nullptr);
	~XQAfficheWidget();
	//获取文本
	QString text()const;
	XQScrollLabel* scrollLabel()const;
public :
	//设置文本
	void setText(const QString& text);
	//设置标题文本
	void setTitleText(const QString& text);
//signals://信号
protected://初始化
	//初始化
	void init();
	void init_ui();
protected://事件

protected://变量
	XQScrollLabel* m_label = nullptr;//正文
	QLabel* m_title = nullptr;//标题
	QMenu* m_menu = nullptr;//弹出式菜单
};
#endif // !XQAfficheWidget_H
