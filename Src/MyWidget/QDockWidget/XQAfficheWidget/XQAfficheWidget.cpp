﻿#include"XQAfficheWidget.h"
#include"XQScrollLabel.h"
#include"XQAppInfo.h"
#include<QBoxLayout>
#include<QMainWindow>
#include<QLabel>
#include<QMenu>
#include<QAction>
XQAfficheWidget::XQAfficheWidget(QWidget* parent)
	:QDockWidget("信息区",parent)
{
	init();
}

XQAfficheWidget::~XQAfficheWidget()
{
}
QString XQAfficheWidget::text() const
{
	return m_label->text();
}

XQScrollLabel* XQAfficheWidget::scrollLabel() const
{
	return m_label;
}


void XQAfficheWidget::setText(const QString& text)
{
	m_label->setText(text);
}

void XQAfficheWidget::setTitleText(const QString& text)
{
	m_title->setText(text);
}

void XQAfficheWidget::init()
{
	init_ui();
	m_menu = new QMenu(this);
	QAction* dock=m_menu->addAction("向下停靠", [=] {
		auto window = ((QMainWindow*)parentWidget());
	if(window->dockWidgetArea(this)== Qt::TopDockWidgetArea)
		window->addDockWidget(Qt::BottomDockWidgetArea, this);
	else
		window->addDockWidget(Qt::TopDockWidgetArea, this);
		});
	
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QDockWidget::customContextMenuRequested, [=](const QPoint& pos) {
	if (((QMainWindow*)parentWidget())->dockWidgetArea(this) == Qt::TopDockWidgetArea)
		dock->setText("向下停靠");
	else
		dock->setText("向上停靠");
		m_menu->popup(this->mapToGlobal(pos));
		});
}

void XQAfficheWidget::init_ui()
{
	//设置内容布局
	m_title = new QLabel("公告:",this);
	m_label = new XQScrollLabel(this);
	setTitleBarWidget(m_title);

	setWidget(m_label);
	setFeatures(QDockWidget::DockWidgetVerticalTitleBar);
	
}

