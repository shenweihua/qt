﻿#include"XQTimeSelectDockWidget.h"
#include<QBoxLayout>
#include<QButtonGroup>
#include<QPushButton>
#include<QDateTimeEdit>
#include<QLabel>
XQTimeSelectDockWidget::XQTimeSelectDockWidget(QWidget* parent) :XQDockWidget(parent)
{
	init();
}

XQTimeSelectDockWidget::XQTimeSelectDockWidget(const QString& title, QWidget* parent) :XQDockWidget(title, parent)
{
	init();
}

XQTimeSelectDockWidget::~XQTimeSelectDockWidget()
{
}

QDateTimeEdit* XQTimeSelectDockWidget::startDateTimeEdit() const
{
	return m_startDateTime;
}

QDateTimeEdit* XQTimeSelectDockWidget::endDateTimeEdit() const
{
	return m_endDateTime;
}

TimeInterval XQTimeSelectDockWidget::selectTimeType() const
{
	if (m_selectTimeBtn == nullptr)
		return TimeInterval::null;
	return TimeInterval(m_timeButtons->id(m_selectTimeBtn));
}

QDateTime XQTimeSelectDockWidget::startDateTime()const
{
	return m_startDateTime->dateTime();
}

QDateTime XQTimeSelectDockWidget::endDateTime()const
{
	return m_endDateTime->dateTime();
}

QDateTime XQTimeSelectDockWidget::selectDateTime(QDateTime time)
{
	if (m_selectTimeBtn == nullptr)
		return time;
	switch (TimeInterval(m_timeButtons->id(m_selectTimeBtn)))
	{
	case TimeInterval::oneWeek:return time.addDays(-7);
		break;
	case TimeInterval::oneMonth:return time.addMonths(-1);
		break;
	case TimeInterval::threeMonth:return time.addMonths(-3);
		break;
	case TimeInterval::sixMonth:return time.addMonths(-6);
		break;
	case TimeInterval::oneYear:return time.addYears(-1);
		break;
	case TimeInterval::threeYear:return time.addYears(-3);
		break;
	case TimeInterval::fiveYear:return time.addYears(-5);
		break;
	case TimeInterval::thisYear:return QDateTime(QDate(time.date().year(),1,1),QTime());
		break;
	case TimeInterval::userDefined:return m_startDateTime->dateTime();
		break;
	default:
		break;
	}

	return time;
}

void XQTimeSelectDockWidget::timeButtonPressed(TimeInterval time)
{
	timeButtonPressed(int(time));
}

void XQTimeSelectDockWidget::setStartTime(const QDateTime& startTime)
{
	m_startDateTime->setDateTime(startTime);
}

void XQTimeSelectDockWidget::setEndTime(const QDateTime& endTime)
{
	m_endDateTime->setDateTime(endTime);
}

void XQTimeSelectDockWidget::timeButtonPressed(int id)
{
	m_selectTimeBtn =static_cast<QPushButton*>(m_timeButtons->button(id));
	for (auto btn: m_timeButtons->buttons())
	{
		/*auto PushButton=static_cast<QPushButton*>(btn);*/
		if (m_selectTimeBtn == btn)
			btn->setEnabled(false);
		else
			btn->setEnabled(true);
	}
	m_timeButtons->button(int(TimeInterval::userDefined))->setEnabled(true);
	emit selectTime(TimeInterval(id));
}

void XQTimeSelectDockWidget::locationChanged(Qt::DockWidgetArea area)
{
	if (area == Qt::LeftDockWidgetArea || area == Qt::RightDockWidgetArea)
	{
		m_timeLayout->setDirection(QBoxLayout::TopToBottom);
		m_userLayout->setDirection(QBoxLayout::TopToBottom);
		setFixedWidth(200);
		setMaximumHeight(16777215); 
	}
	else if (area == Qt::TopDockWidgetArea || area == Qt::BottomDockWidgetArea)
	{
		m_timeLayout->setDirection(QBoxLayout::LeftToRight);
		m_userLayout->setDirection(QBoxLayout::LeftToRight);
		setMaximumWidth(16777215);
		setFixedHeight(100);
	}
	else
	{
		setMinimumSize(0, 0);
		setMaximumSize(16777215, 16777215);
	}
}

void XQTimeSelectDockWidget::init()
{
	init_timeGroup();
	init_userGroup();
	XQDockWidget::init();
}

void XQTimeSelectDockWidget::init_ui()
{
	XQDockWidget::init_ui();
	auto widget = new QWidget();
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, widget);
	layout->addLayout(m_timeLayout);
	layout->addLayout(m_userLayout);
	widget->setLayout(layout);
	setWidget(widget);
}

void XQTimeSelectDockWidget::init_timeGroup()
{
	//开内存
	m_timeButtons = new QButtonGroup(this);
	m_timeButtons->addButton(new QPushButton("近1周"), int(TimeInterval::oneWeek));
	m_timeButtons->addButton(new QPushButton("近1月"), int(TimeInterval::oneMonth));
	m_timeButtons->addButton(new QPushButton("近3月"), int(TimeInterval::threeMonth));
	m_timeButtons->addButton(new QPushButton("近6月"), int(TimeInterval::sixMonth));
	m_timeButtons->addButton(new QPushButton("近1年"), int(TimeInterval::oneYear));
	m_timeButtons->addButton(new QPushButton("近3年"), int(TimeInterval::threeYear));
	m_timeButtons->addButton(new QPushButton("近5年"), int(TimeInterval::fiveYear));
	m_timeButtons->addButton(new QPushButton("今年来"), int(TimeInterval::thisYear));
	m_timeButtons->addButton(new QPushButton("至今"), int(TimeInterval::hitherto));
	//初始化
	//加入布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);
	layout->addWidget(m_timeButtons->button(int(TimeInterval::oneWeek)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::oneMonth)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::threeMonth)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::sixMonth)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::oneYear)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::threeYear)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::fiveYear)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::thisYear)));
	layout->addWidget(m_timeButtons->button(int(TimeInterval::hitherto)));
	m_timeLayout=layout;

	//绑定信号和槽处理
	connect(m_timeButtons, &QButtonGroup::idPressed, this,QOverload<int>::of (&XQTimeSelectDockWidget::timeButtonPressed));
}

void XQTimeSelectDockWidget::init_userGroup()
{
	m_startDateTime = new QDateTimeEdit();
	m_endDateTime = new QDateTimeEdit();
	m_userDefinedBtn = new QPushButton("查询");
	m_timeButtons->addButton(m_userDefinedBtn, int(TimeInterval::userDefined));

	//创建布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);
	layout->addStretch(1);
	layout->addWidget(new QLabel("开始日期"));
	layout->addWidget(m_startDateTime);
	layout->addWidget(new QLabel("结束日期"));
	layout->addWidget(m_endDateTime);
	layout->addWidget(m_userDefinedBtn);
	layout->addStretch(1);
	//属性初始化
	m_startDateTime->setCalendarPopup(true);
	m_endDateTime->setCalendarPopup(true);
	m_startDateTime->setDisplayFormat("yyyy年MM月dd日");
	m_endDateTime->setDisplayFormat("yyyy年MM月dd日");
	m_userLayout = layout;
}
