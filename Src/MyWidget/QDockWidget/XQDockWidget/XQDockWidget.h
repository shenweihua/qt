﻿#ifndef XQDOCKWIDGET_H
#define XQDOCKWIDGET_H
#include"XQHead.h"
#include<QDockWidget>
class XQDockWidget :public QDockWidget
{
	Q_OBJECT
public:
	XQDockWidget(QWidget* parent = nullptr);
	XQDockWidget(const QString& title, QWidget* parent = nullptr);
	virtual~XQDockWidget();
	//设置是否为普通子窗口
	void setChildWidget(bool child);
protected://初始化
	virtual void init();
	virtual void init_ui();
protected://信号槽绑定
	//停靠位置改变
	virtual void locationChanged(Qt::DockWidgetArea area);
	//浮动改变
	virtual void topChanged();
protected:
	XQTitleBarWidget* m_title = nullptr;
};
#endif // !XQDockWidget_H
