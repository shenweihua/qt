﻿#ifndef XQWIDGET_H
#define XQWIDGET_H
#include<QWidget>
#include"XQHead.h"
class XQWidget :public QWidget
{
	Q_OBJECT
public:
	XQWidget(QWidget* parent = nullptr);
	XQWidget(const QString& title, XQWidget* parent = nullptr);
	~XQWidget();
	//标题栏
	XQTitleBarWidget* titleBarWidget()const;
	//堆栈小部件
	virtual XQStackedWidget* stackedWidget();
public:
	//堆栈小部件
	virtual void setStackedWidget(XQStackedWidget* Widget);
protected://初始化
	//初始化
	virtual void init();
	virtual void init_ui();
	virtual void init_titleBarWidget();
protected:
	XQTitleBarWidget* m_title = nullptr;//标题栏小部件
};
#endif