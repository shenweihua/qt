﻿#include"XQWidget.h"
#include"XQTitleBarWidget.h"
#include<QBoxLayout>
XQWidget::XQWidget(QWidget* parent)
	:QWidget(parent)
{
	/*init();*/
}

XQWidget::XQWidget(const QString& title, XQWidget* parent)
	:QWidget(parent)
{
	/*init();*/
	setWindowTitle(title);
}

XQWidget::~XQWidget()
{

}

XQTitleBarWidget* XQWidget::titleBarWidget() const
{
	return m_title;
}

XQStackedWidget* XQWidget::stackedWidget()
{
	if(m_title)
		return m_title->stackedWidget();
	return nullptr;
}

void XQWidget::setStackedWidget(XQStackedWidget* Widget)
{
	if (m_title)
		m_title->setStackedWidget(Widget);
}

void XQWidget::init()
{
	init_titleBarWidget();
	init_ui();
}

void XQWidget::init_ui()
{
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom,this);//从左到右
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(m_title);
	layout->addStretch(1);
	setLayout(layout);
}

void XQWidget::init_titleBarWidget()
{
	m_title = new XQTitleBarWidget(this);
	m_title->setButtons(15);
}
