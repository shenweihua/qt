﻿#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include<QBoxLayout>
#include<QLabel>
#include<QStackedLayout>
void XQStackedWidget::init_ui()
{
	m_StackedLayout = new QStackedLayout();
	m_StackedTitleWidget = new XQStackedTitleWidget(QBoxLayout::LeftToRight,this);
	m_StackedTitleWidget->setStackedWidget(this);
	
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从上到下
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(m_StackedTitleWidget);
	layout->addLayout(m_StackedLayout);
	setLayout(layout);
	m_windowLayout = layout;
	setWindowDirection(QBoxLayout::TopToBottom);
}


