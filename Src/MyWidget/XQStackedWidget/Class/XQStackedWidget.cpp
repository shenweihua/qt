﻿#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include"XQLabelButton.h"
#include<QStackedLayout>
#include<QCloseEvent>
#include<QIcon>
#include<QDebug>
XQStackedWidget::XQStackedWidget(QWidget* parent)
	:QWidget(parent)
{
	init();

}

XQStackedWidget::XQStackedWidget(QBoxLayout::Direction dirWindow, QBoxLayout::Direction dirButton, QWidget* parent)
	:QWidget(parent)/*,m_dirWindow(dirWindow),m_dirButton(dirButton)*/
{
	init();
	setWindowDirection(dirWindow);
	setButtonDirection(dirButton);
}

XQStackedWidget::~XQStackedWidget()
{	
}

QString XQStackedWidget::titleText(QWidget* widget)const
{
	if (m_TitleBtns.find(widget) != m_TitleBtns.end())
	{
		return m_TitleBtns[widget]->text();
	}
	return QString();
}

QPixmap XQStackedWidget::icon(QWidget* widget)const
{
	if (m_TitleBtns.find(widget) != m_TitleBtns.end())
	{
		return m_TitleBtns[widget]->icon();
	}
	return QPixmap();
}

size_t XQStackedWidget::count() const
{
	return m_TitleBtns.count();
}

XQLabelButton* XQStackedWidget::labelButton(QWidget* widget) const
{
	if(m_TitleBtns.find(widget)== m_TitleBtns.end())
		return nullptr;
	return m_TitleBtns[widget];
}

QWidget* XQStackedWidget::widget(XQLabelButton* btn) const
{
	if (m_widgets.find(btn) == m_widgets.end())
		return nullptr;
	return m_widgets[btn];
}

XQStackedTitleWidget* XQStackedWidget::titleWidget() const
{
	return m_StackedTitleWidget;
}

void XQStackedWidget::setCurrentWidget(XQLabelButton* btn)
{
	if (m_widgets.end() == m_widgets.find(btn))
		return;
	//标签改变信号来修改显示的小部件
	//选择对应的按钮使其变色显示  选中对应的按钮
	m_StackedTitleWidget->selectButton(btn);
}

void XQStackedWidget::setCurrentIndex(int index)
{
	m_StackedTitleWidget->selectIndex(index);
}

void XQStackedWidget::IndependentWidget(QWidget* widget)
{
	if (widget == nullptr)
		return;
	if (m_TitleBtns.find(widget) == m_TitleBtns.end())
		return ;
	m_StackedLayout->removeWidget(widget);//从堆栈布局中删除
	widget->setWindowFlag(Qt::Window, true);
	widget->setParent(nullptr);
	widget->show();
	titleWidget()->removeOnlyButton(m_TitleBtns[widget]);
	m_widgets.remove(m_TitleBtns[widget]);
	m_TitleBtns.remove(widget);
}

void XQStackedWidget::addWidget(QWidget* widget)
{
	if (widget == nullptr)
		return;
	auto btn = this->labelButton(widget);
	widget->setWindowFlag(Qt::Window, false);
	if (btn)
		addWidget(widget,btn->text(),btn->icon());
	else
		addWidget(widget,QString());
}

void XQStackedWidget::addWidget(QWidget* widget, const QString& title, const QPixmap& icon)
{
	if (widget == nullptr)
		return;
	m_StackedLayout->addWidget(widget);
	{
		auto btn = this->labelButton(widget);
		if (btn)
		{
			btn->setText(title);
			btn->setIcon(icon);
			btn->setVisible(true);
			setCurrentWidget(widget);
			return;
		}
	}
	QString Title = title;
	if (Title.isEmpty())
		Title = widget->windowTitle();
	else
		widget->setWindowTitle(Title);
	auto btn = m_StackedTitleWidget->addButton(Title);
	btn->setTextFixedWidth(100);
	m_TitleBtns[widget] = btn;//建立对应的映射
	m_widgets[m_TitleBtns[widget]] = widget;//建立对应的映射

	//按钮关闭时信号
	connect(btn, &XQLabelButton::closeWidgetRequest, [=] {if(widget->close())m_StackedTitleWidget->buttonClose(m_TitleBtns[widget]); });
	//链接小部件窗口标题和图标改变信号
	connect(widget, &QWidget::windowTitleChanged, btn, &XQLabelButton::setText);
	connect(widget, &QWidget::windowIconChanged, btn, QOverload<const QIcon&>::of(&XQLabelButton::setIcon));
	connect(widget, &QWidget::destroyed, [=] {m_StackedTitleWidget->removeOnlyButton(m_TitleBtns[widget]); removeOnlyWidget(widget, false); });
	setIcon(icon, widget);
}

void XQStackedWidget::removeWidget(QWidget* widget)
{
	if (m_TitleBtns.end() == m_TitleBtns.find(widget))
		return;
	if (!widget->close())//如果拒绝关闭
		return;
	m_StackedTitleWidget->removeOnlyButton(m_TitleBtns[widget]);//关闭按钮
	removeOnlyWidget(widget);
}

void XQStackedWidget::removeWidget(XQLabelButton* btn)
{
	removeWidget(m_widgets[btn]);
}
void XQStackedWidget::disconnectClose(QWidget* widget)
{
	auto btn = m_TitleBtns[widget];
	disconnect(btn, &XQLabelButton::closeWidgetRequest,nullptr,nullptr);
}
void XQStackedWidget::clearWidget()
{
	auto list = m_widgets.values();
	//////清理打开的窗口
	for (auto& widge : list)
	{
		removeWidget(widge);
	}
}
void XQStackedWidget::connectClose(QWidget* widget, std::function<bool()>func)
{
	auto btn = m_TitleBtns[widget];
	disconnectClose(widget);
	connect(btn, &XQLabelButton::closeWidgetRequest, [=] {if (func())removeWidget(widget); });
}
void XQStackedWidget::removeOnlyWidget(QWidget* widget, bool AutoDelete)
{
	if (m_TitleBtns.end() == m_TitleBtns.find(widget))
		return;
	m_StackedLayout->removeWidget(widget);//从堆栈布局中删除
	if(AutoDelete)
		widget->deleteLater();//关闭小部件

	//从map容器中释放
	m_widgets.remove(m_TitleBtns[widget]);
	m_TitleBtns.remove(widget);
	emit deleteWidget(widget);
	
}
void XQStackedWidget::removeOnlyWidget(XQLabelButton* btn)
{
	removeOnlyWidget(m_widgets[btn]);
}
void XQStackedWidget::closeEvent(QCloseEvent* event)
{
	clearWidget();//清理下窗口
	if (count() != 0)
	{
		event->ignore();
	}
	/*else
	{
		deleteLater();
	}*/
}
void XQStackedWidget::setTitleText(const QString& text, QWidget* widget)
{
	if (m_TitleBtns.find(widget) != m_TitleBtns.end())
	{
		m_TitleBtns[widget]->setText(text);
	}
}

void XQStackedWidget::setIcon(QPixmap icon, QWidget* widget)
{
	if (m_TitleBtns.find(widget) == m_TitleBtns.end())
		return;
	if (icon.isNull())
	{
		icon = widget->windowIcon().pixmap(100,100);
	}
	else
	{
		widget->setWindowIcon(icon);
	}
	m_TitleBtns[widget]->setIcon(icon);
}

void XQStackedWidget::setWindowDirection(QBoxLayout::Direction dir)
{
	m_windowLayout->setDirection(dir);
	if(dir ==QBoxLayout::TopToBottom|| dir == QBoxLayout::BottomToTop)
	{
		m_StackedTitleWidget->setMaximumHeight(40);
		m_StackedTitleWidget->setMaximumWidth(16777215);
	}
	else
	{
		m_StackedTitleWidget->setMaximumHeight(16777215);
		m_StackedTitleWidget->setMaximumWidth(160);
	}
}

void XQStackedWidget::setButtonDirection(QBoxLayout::Direction dir)
{
	m_StackedTitleWidget->setButtonDirection(dir);
}

void XQStackedWidget::setCurrentWidget(QWidget* widget)
{
	if (m_TitleBtns.end() == m_TitleBtns.find(widget))
		return;
	//标签改变信号来修改显示的小部件
	//选择对应的按钮使其变色显示  选中对应的按钮
	m_StackedTitleWidget->selectButton(m_TitleBtns[widget]);
}
