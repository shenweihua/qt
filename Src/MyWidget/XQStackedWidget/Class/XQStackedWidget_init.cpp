﻿#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include"XQLabelButton.h"
#include"XQEventFilterObject.h"
#include<QStackedLayout>
#include<QCloseEvent>
void XQStackedWidget::init()
{
	init_ui();

	//绑定标签按钮关闭信号
	connect(m_StackedTitleWidget, &XQStackedTitleWidget::deleteButton,this,QOverload<XQLabelButton*>().of(&XQStackedWidget::removeOnlyWidget));
	//绑定标签按钮选中信号
	connect(m_StackedTitleWidget, &XQStackedTitleWidget::selectChange, [=](XQLabelButton* btn) {
		if(m_StackedLayout->currentWidget()!= m_widgets[btn])
			m_StackedLayout->setCurrentWidget(m_widgets[btn]);//标签改变切换显示的小部件 
		});
	//{//绑定父窗口关闭信号
	//	parent->installEventFilter(new XQEventFilterObject([=](QEvent* ev) {
	//		clearWidget();//清理下窗口
	//		if (count() == 0)
	//			ev->accept();
	//		},QEvent::Close, parent));
	//}
}