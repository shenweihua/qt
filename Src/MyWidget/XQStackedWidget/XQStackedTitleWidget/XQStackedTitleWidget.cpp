﻿#include "XQStackedTitleWidget.h"
#include"XQLabelButton.h"
#include<QBoxLayout>
#include<QPushButton>
#include<QLabel>
#include<QDebug>
XQStackedTitleWidget::XQStackedTitleWidget(QWidget* parent)
	:QWidget(parent)
{
	init();
}

XQStackedTitleWidget::XQStackedTitleWidget(QBoxLayout::Direction dirButton, QWidget* parent)
	:QWidget(parent),m_dirButton(dirButton)
{
	init();
}

XQStackedTitleWidget::~XQStackedTitleWidget()
{
	/*for (auto&btn: m_buttons)
	{
		btn->deleteLater();
	}
	m_buttons.clear();*/
}

QList<XQLabelButton*> XQStackedTitleWidget::buttons() const
{
	return m_buttons;
}

bool XQStackedTitleWidget::isButtonCenter() const
{
	return m_buttonCenter;
}

bool XQStackedTitleWidget::isShowButtonCount() const
{
	return m_buttonCount->isVisible();
}

XQStackedWidget* XQStackedTitleWidget::stackedWidget()
{
	return m_StackedWidget;
}

void XQStackedTitleWidget::setStackedWidget(XQStackedWidget* StackedWidget)
{
	m_StackedWidget = StackedWidget;
}

void XQStackedTitleWidget::selectButton(XQLabelButton* btn)
{
	if (m_buttons.indexOf(btn) == -1)
		return;//找不到按钮不存在
	if (btn != m_selectBtn)//我选择按钮与我原先的按钮不同时
	{
		//设置为选中
		btn->setChecked(true);
		//上次有按钮是选中的
		if (m_selectBtn != nullptr)
		{
			m_selectBtn->setChecked(false);//取消选中
		}
		m_selectBtn = btn;//更新
		emit selectChange(btn);
	}
}

void XQStackedTitleWidget::selectIndex(int index)
{
	if(index>=0&&index<m_buttons.size())
	{
		selectButton(m_buttons[index]);
	}
}

void XQStackedTitleWidget::buttonClose(XQLabelButton* btn)
{
	if (m_buttons.indexOf(btn) == -1)
		return;//找不到按钮不存在
	removeOnlyButton(btn);
	//发送信号
	emit countChange(m_buttons.count());
	emit deleteButton(btn);
}

void XQStackedTitleWidget::removeOnlyButton(XQLabelButton* btn)
{
	if (m_buttons.indexOf(btn) == -1)
		return;//找不到按钮不存在
	auto layout = (QBoxLayout*)m_background->layout();
	//当前如果是选中的
	if (btn->checked())
	{
		//现有至少两个按钮
		if (m_buttons.count() > 1)
		{
			XQLabelButton* newSelectBtn = nullptr;//要替换显示选择的按钮
			//获取一个要显示的小部件索引
			int nSel = layout->indexOf(btn) - 1;//定位到前一个
			if (nSel < 0)//选中的是第一个
				nSel = 1;//重新定位到下一个
			newSelectBtn = (XQLabelButton*)layout->itemAt(nSel)->widget();
			//设置新的选中按钮
			if (newSelectBtn != nullptr)
				selectButton(newSelectBtn);
		}
		//只有一个按钮并且删除的是选中按钮
		else if (m_selectBtn == btn)
		{
			m_selectBtn = nullptr;
		}
	}
	//从数组中删除
	m_buttons.remove(m_buttons.indexOf(btn));
	//从布局中删除 
	layout->removeWidget(btn);
	//释放按钮
	btn->deleteLater();
	layout->invalidate();
	hide();
	show();
}

void XQStackedTitleWidget::setButtonCenter(bool center)
{
	if (m_buttonCenter == center)
		return;//无需设置
	auto layout = (QBoxLayout*)m_background->layout();
	if (center == true)
	{//设置居中
		layout->insertStretch(0, 1);
	}
	else
	{
		layout->removeItem(layout->itemAt(0));
	}
	m_buttonCenter = center;
}

void XQStackedTitleWidget::setShowButtonCount(bool show)
{
	if (show)
		m_buttonCount->show();
	else
		m_buttonCount->hide();
}

void XQStackedTitleWidget::setIndependentWidget(bool Independent)
{
	m_IndependentWidget = Independent;
}

void XQStackedTitleWidget::setMoveWidget(bool move)
{
	m_moveWidget = true;
}

void XQStackedTitleWidget::setButtonDirection(QBoxLayout::Direction dir)
{
	static_cast<QBoxLayout*>(m_background->layout())->setDirection(dir);
}

XQLabelButton* XQStackedTitleWidget::addButton(const QString& text)
{
	auto layout = (QBoxLayout*)m_background->layout();
	auto btn=new XQLabelButton(text, this);
	/*btn->setFixedWidth(200);*/
	layout->insertWidget(m_buttons.count(), btn);//插入布局
	m_buttons << btn;//加入数组
	emit countChange(m_buttons.count());//数量改变信号
	//按钮关闭时信号
	connect(btn, &XQLabelButton::closeWidgetRequest, [=] {emit closeButtonRequest(btn);});
	//按钮选择信号
	connect(btn, &XQLabelButton::selectWidget, [=] {selectButton(btn);});
	return btn;
}
