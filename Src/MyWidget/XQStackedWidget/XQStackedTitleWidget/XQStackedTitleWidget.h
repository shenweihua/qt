﻿#ifndef XQSTACKEDTITLEWIDGET_H
#define XQSTACKEDTITLEWIDGET_H
#include<QWidget>
#include<QMap>
#include<QBoxLayout>
#include"XQHead.h"
//堆栈标题小部件
class XQStackedTitleWidget:public QWidget
{
	Q_OBJECT
public:
	XQStackedTitleWidget(QWidget* parent = nullptr);
	XQStackedTitleWidget(QBoxLayout::Direction dirButton, QWidget* parent = nullptr);
	~XQStackedTitleWidget();
	QList<XQLabelButton*> buttons()const;
	//查询按钮是否居中排列显示
	bool isButtonCenter()const;
	//是否显示按钮计数
	bool isShowButtonCount()const;
	//堆栈小部件
	XQStackedWidget* stackedWidget();
public :
	void setStackedWidget(XQStackedWidget* StackedWidget);
	//添加一个按钮
	XQLabelButton* addButton(const QString& text);
	//选择按钮
	void selectButton(XQLabelButton* btn);
	void selectIndex(int index);
	//关闭一个按钮
	void buttonClose(XQLabelButton* btn);
	//仅仅删除按钮不发送信号
	void removeOnlyButton(XQLabelButton* btn);
	//设置按钮居中排列显示
	void setButtonCenter(bool center);
	//设置是否显示按钮计数
	void setShowButtonCount(bool show);
	//是否可以独立窗口
	void setIndependentWidget(bool Independent);
	void setMoveWidget(bool move);
	//设置按钮方向
	void setButtonDirection(QBoxLayout::Direction dir);
signals://信号
	//数量改变
	void countChange(size_t count);
	//选择按钮改变
	void selectChange(XQLabelButton* btn);
	//按钮关闭请求信号
	void closeButtonRequest(XQLabelButton* btn);
	//释放按钮
	void deleteButton(XQLabelButton* btn);
protected://初始化
	//初始化
	void init();
	void init_ui();
protected://事件
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
protected://变量
	bool m_IndependentWidget = false;
	bool m_moveWidget = false;
	QPoint m_pressPos;
	QPoint m_selectWidgetPos;
	bool m_moveBtn = false;//移动
	bool m_buttonCenter = false;//按钮是否居中显示
	XQStackedWidget* m_StackedWidget = nullptr;
	QBoxLayout::Direction m_dirButton = QBoxLayout::LeftToRight;//按钮方向
	QWidget* m_background = nullptr;//背景
	XQLabelButton* m_selectBtn = nullptr;//选中的按钮
	QList<XQLabelButton*> m_buttons;//按钮列表
	QLabel* m_buttonCount=nullptr;//显示标题按钮数量
};
#endif // !XQStackedTitleWidget_H
