﻿#include"XQFileTableWidget.h"
#include"XQFileTableWidget_ItemDelegate.h"
#include<QHeaderView>
void XQFileTableWidget::UIInit()
{
	// 去掉所有边框线
	 setShowGrid(false);
	// 开启隔一行变色
	// setAlternatingRowColors(true);
	//列宽自动调整的方法如下
	 horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
	/* setColumnWidth(1, 50);
	 setColumnWidth(2, 50);*/
	 verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
	 setItemDelegate(new MyItemDelegate(this));
	// 去掉水平和垂直边框线
	 setStyleSheet(
		 "background: qlineargradient(x1:0, y1:0, x2:1, y2:1, m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);"
		 "QTableView::verticalHeader {border: none;}"
		"QTableView::horizontalHeader {border: none;}"
		"QTableView::item:selected { border: none; "
		"background: qlineargradient(x1:0, y1:0, x2:0, y2:1, m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);"
		"color: black; }");
	// 去掉垂直标题
	 verticalHeader()->setVisible(false);
	//设置整行选取
	 setSelectionBehavior(QAbstractItemView::SelectRows);
	//设置字体
	QFont font("Microsoft YaHei", 15); // 创建字体对象，设置字体为“微软雅黑”，大小为12
	 setFont(font); // 设置表格的字体为上面创建的字体对象
	 horizontalHeader()->setFont(font);//设置水平标题
	 horizontalHeader()->setStyleSheet("QHeaderView::section {"//background//section
		"background-color: qlineargradient(x1:0, y1:0, x2:1, y2:1,"
		"m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);"
		"}");

	 setColumnCount(5); // 设置列数为5
	// setRowCount(10);
	//设置标题
	 setHorizontalHeaderLabels({ "图标","名字", "大小", "本地","云端" });

	 setIconSize(QSize(50, 50));
	this->setColumnWidth(Type::icon, 50);
}
void XQFileTableWidget::setHorizontalHeading(const QStringList& list)
{
	setColumnCount(list.size());
	//设置标题
	setHorizontalHeaderLabels(list);
	m_sorts.resize(list.size());
}
void XQFileTableWidget::SetColumnWidth(int index)
{
	// 设置列宽
	// 获取表格中最长的文字
	int maxWidth = 0;
	for (int row = 0; row < this->rowCount(); ++row) {
		QTableWidgetItem* item = this->item(row, index);
		if (item) {
			QFontMetrics fontMetrics(font());
			int width = fontMetrics.horizontalAdvance(item->text());
			if (width > maxWidth) {
				maxWidth = width;
			}
		}
	}
	// 根据最长的文字设置列宽
	if(maxWidth!=0)
		this->setColumnWidth(index, maxWidth+10);
	
}

