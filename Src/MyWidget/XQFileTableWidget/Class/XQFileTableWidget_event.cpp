﻿#include"XQFileTableWidget.h"
#include<QDragEnterEvent>
#include<QMimeData>
#include<QDropEvent>
#include<QMenu>
void XQFileTableWidget::dragEnterEvent(QDragEnterEvent* ev)
{
    if (ev->mimeData()->hasUrls()) {
        ev->acceptProposedAction();
        qInfo() << "有文件进入控件";
        //ev->setDropAction(Qt::CopyAction);
        //// 获取拖拽的文件名列表
        //QList<QUrl> urls = ev->mimeData()->urls();
        //QList<QString> files;
        //for (int i = 0; i < urls.count(); i++)
        //{
        //    files.append(urls.at(i).toLocalFile());
        //}
        //addFile(files);
    }
}

void XQFileTableWidget::dropEvent(QDropEvent* ev)
{
    qInfo() << "接受列表";
    // 获取拖拽的文件名列表
   /* QList<QUrl> urls = event->mimeData()->urls();
    QList<QString> files;
    for (int i = 0; i < urls.count(); i++)
    {
        files.append(urls.at(i).toLocalFile());
    }
    qInfo() << files;
    event->acceptProposedAction();
     TODO: 处理拖拽的文件
    ev->accept();*/
}

void XQFileTableWidget::resizeEvent(QResizeEvent* event)
{
    //qInfo() << event->size();
    size_t widthSum = 0;
    for (int column = 0; column < this->columnCount(); ++column)
    {
        if (column != Type::name)
            widthSum += columnWidth(column);
    }
    setColumnWidth(Type::name, width() - widthSum-2);
}

void XQFileTableWidget::contextMenuEvent(QContextMenuEvent* event)
{
    m_menu->clear();
    bool Remote = isRemoteFile(currentRow());
    bool Local = isLocalFile(currentRow());
    int row = currentRow();//当前行
    if (Remote&&!Local)//远程存在本地不存在
    {
        m_menu->addAction("下载文件", [this] { emit downRemoteFile(this, &m_cloudFile[rowToMd5(currentRow())]); });//删除远程});
    }
    else if (!Remote &&Local)//本地存在远程不存在
    {
        m_menu->addAction("上传文件", [this] { emit upLocalFile(this,&m_localFile[rowToMd5(currentRow())]); });
    }
    m_menu->addSeparator();
    if (Local)//如果本地存在
    {
        m_menu->addAction("打开文件", this, &XQFileTableWidget::openFile);
        m_menu->addAction("复制链接地址", [this] {copyLinkPath(currentRow()); });
        m_menu->addSeparator();
        m_menu->addAction("删除本地文件", [this] {removeFile(currentRow()); });
    }
    if (Remote)
    {
        m_menu->addAction("删除远程文件", [this] {emit removeRemoteFile(this, &m_cloudFile[rowToMd5(currentRow())]); });
    }
    if (Remote && Local)//两端都存在
    {
        m_menu->addAction("删除两端文件", [this] {
            removeFile(currentRow()); //删除本地
            emit removeRemoteFile(this, &m_cloudFile[rowToMd5(currentRow())]);//删除远程
            });
    }
    m_menu->addSeparator();
    if (Remote || Local)//两端一个存在
         m_menu->addAction("重命名文件", [this] {editItem(item(currentRow(), Type::name));  });
    if(!selectedItems().isEmpty())//没有选择的时候不弹出菜单
        m_menu->popup(event->globalPos());
}

void XQFileTableWidget::showEvent(QShowEvent* event)
{
    fileChange();
}
