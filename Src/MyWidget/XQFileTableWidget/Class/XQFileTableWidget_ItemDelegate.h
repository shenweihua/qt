﻿#ifndef XFILETABLEWIDGET_ITEMDELEGATE_H
#define XFILETABLEWIDGET_ITEMDELEGATE_H
#include"XQFileTableWidget.h"
#include<QItemDelegate>
#include<QLineEdit>
#include<QFileInfo>
#include<QPainter>
class MyItemDelegate :public QItemDelegate
{
public:
	MyItemDelegate(QObject* parent = nullptr)
		: QItemDelegate(parent) {}
	void setEditorData(QWidget* editor, const QModelIndex& index)  const override
	{
		if (index.column() != XQFileTableWidget::Type::name)
		{ // 不是第一列的单元格使用默认编辑框
			QItemDelegate::setEditorData(editor, index); // 使用默认的设置方式
			return;
		}
		QLineEdit* lineEdit = static_cast<QLineEdit*>(editor); // 将编辑框指针转换为 QLineEdit 类型
		lineEdit->setAlignment(index.data(Qt::TextAlignmentRole).value<Qt::Alignment>());
		QString text = index.data().toString(); // 获取编辑框中的文本
		lineEdit->setText(QFileInfo(text).baseName());
	}
	// 重载 setModelData() 方法以重新定义编辑时传入的内容
	void setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const override 
	{
		if (index.column() != XQFileTableWidget::Type::name) { // 不是第一列的单元格使用默认编辑框
			QItemDelegate::setModelData(editor, model, index); // 使用默认的设置方式
			return;
		}

		QLineEdit* lineEdit = static_cast<QLineEdit*>(editor); // 将编辑框指针转换为 QLineEdit 类型
		QString text = lineEdit->displayText(); // 获取编辑框中的文本
		// 将新文本内容设置到数据模型中
		model->setData(index, text+"."+ QFileInfo(index.data().toString()).suffix());

	}
	// 重写 paint() 方法，绘制单元格外观
	void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override
	{
		// 检查单元格是否可编辑
		if (!(index.flags() & Qt::ItemIsEditable))
		{
			QString text = index.model()->data(index, Qt::DisplayRole).toString();
			// 如果单元格不可编辑，重绘文本颜色
			painter->save();
			painter->setPen(Qt::black);
			painter->drawText(option.rect, index.data(Qt::TextAlignmentRole).value<Qt::Alignment>(), text);
			painter->restore();
		}
		else
		{
			// 调用父类的 paint() 方法绘制默认的单元格外观
			QItemDelegate::paint(painter, option, index);
		}
		

		// 判断是否为图标列
		if (index.column() == 0)
		{
			// 获取当前单元格的图标，此处假设使用了 Qt 内置图标
			QIcon icon = qvariant_cast<QIcon>(index.data(Qt::UserRole));

			// 绘制彩色图标
			painter->save();
			painter->setRenderHint(QPainter::Antialiasing); // 抗锯齿
			painter->drawPixmap(option.rect, icon.pixmap(option.decorationSize));
			painter->restore();
		}
		
		// 判断当前行是否被选中，并绘制选中行的外观
		if (option.state & QStyle::State_Selected)
		{
			// 计算选中行的矩形区域
			QRect rect = option.rect;

			// 绘制选中行的背景色
			// 创建线性渐变
			QLinearGradient gradient(rect.topLeft(), rect.bottomLeft());
			gradient.setColorAt(0, 0xffffff);
			gradient.setColorAt(0.4, 0x00FFC8);
			gradient.setColorAt(0.7, 0x00C8C8);
			gradient.setColorAt(1, 0x00FFFF);

			// 使用渐变绘制背景
			painter->save();
			painter->setPen(Qt::NoPen);
			painter->setBrush(gradient);
			painter->drawRect(rect);
			painter->restore();

			// 绘制选中行的文本颜色
			painter->setPen(Qt::black);

			// 计算文本区域
			const QAbstractItemModel* model = index.model();
			QString text = model->data(index, Qt::DisplayRole).toString();
			QRect textRect = option.fontMetrics.boundingRect(rect, index.data(Qt::TextAlignmentRole).value<Qt::Alignment>(), text);
			//painter->pen().setAlignment();
				
			// 绘制文本
			painter->drawText(textRect, index.data(Qt::TextAlignmentRole).value<Qt::Alignment>(), text);
		}
	}
};
#endif