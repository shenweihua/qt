﻿#ifndef XQSCROLLLABEL_H
#define XQSCROLLLABEL_H
#include<QWidget>
#include"XQHead.h"
//滚动类型
enum class scrollType
{
	on=1,//开启滚动
	slopOn=2,//溢出的时候开启
	close=4,//关闭
};
//滚动标签
class XQScrollLabel:public QWidget
{
	Q_OBJECT
public:
	XQScrollLabel(QWidget* parent = nullptr);
	XQScrollLabel(const QString& text,QWidget* parent = nullptr);
	~XQScrollLabel();
	//获取文本
	QString text()const;
	//获取字体
	QFont font()const;
	//返回滚动方式
	scrollType textScrollType()const;
	//返回文本对齐方式
	Qt::Alignment alignment() const;
	//返回label
	QLabel* label()const;
public slots://槽函数
//设置文本
	void setText(const QString& text);
	//设置字体
	void setFont(const QFont& font);
	//设置一次持续时间
	void setDuration(int time);
	//设置一秒几个字符
	void setSecondChar(int n);
	//设置滚动
	void setScrollType(scrollType type);
	//设置基类名字
	void setObjectName(const QString& name);
	//设置文本对齐方式
	void setAlignment(Qt::Alignment align);
//signals://信号
protected://初始化
	void init();
	void init_ui();
	//调整位置和大小
	void adjust();
protected://事件
	void resizeEvent(QResizeEvent* event)override;
	void showEvent(QShowEvent* event)override;
protected://数据
	QLabel* m_label =nullptr;
	QPropertyAnimation* m_animation = nullptr;
	scrollType m_scrollType = scrollType::slopOn;//滚动类型
	int m_charCount = 5;//一秒滚动字符数
	//int textWidth;//当前字符总宽
};
#endif