﻿#include "XQSyntaxHiglighter.h"
#include<QRegExp>
XQSyntaxHiglighter::XQSyntaxHiglighter(QTextDocument* parent) :QSyntaxHighlighter(parent)
{

}

void XQSyntaxHiglighter::highlightBlock(const QString& PlainText)
{
    //代码高亮测试
    QTextCharFormat myClassFormat;
    myClassFormat.setFontWeight(QFont::Bold);
    myClassFormat.setForeground(Qt::darkMagenta);
    QString pattern = "\\b[A-Za-z]+\\b";
   // qInfo() << text;
    QRegExp expression(pattern);
    int index = expression.indexIn(PlainText, 0);//从位置0开始匹配字符串
    while (index >= 0) {
        int length = expression.matchedLength();
        setFormat(index, length, myClassFormat);
        index = expression.indexIn(PlainText, index + length);
    }
}