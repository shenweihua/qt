﻿#ifndef XQSYNTAXHIGLIGHTER_H
#define XQSYNTAXHIGLIGHTER_H
#include <QSyntaxHighlighter>
//正则匹配做语法高亮
class XQSyntaxHiglighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    explicit XQSyntaxHiglighter(QTextDocument* parent = 0);
protected:
    void highlightBlock(const QString& PlainText) override;//重新实现该函数
};

#endif // MYSYNTAXHIGLIGHTER_H