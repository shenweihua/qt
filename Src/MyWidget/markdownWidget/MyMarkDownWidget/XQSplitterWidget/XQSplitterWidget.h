﻿#ifndef XQSPLITTERWIDGET_H
#define XQSPLITTERWIDGET_H
#include<QSplitter>
#include<QSet>
//分割小部件组
class XQSplitterWidget:public QSplitter
{
	Q_OBJECT
public:
	XQSplitterWidget(QWidget* parent = nullptr);
	~XQSplitterWidget();
	//设置widget窗口宽度比例
	void setWidgetWidthRatio(const QList<int>& list);
	//获取widget窗口宽度比例
	QList<int> WidgetWidthRatio();
	//显示隐藏小部件
	void HiddenDisplayWidget(const size_t index);
	//添加小部件
	void addWidget(QWidget* widget);
	//获取小部件的宽度
	int widgetWidth(const size_t index);
signals:
	//小部件的宽度是0(隐藏起来了)
	void WidgetWidthZero(const size_t index);
protected:
	//显示事件
	void showEvent(QShowEvent* event)override;
private:
	QList<int> m_WidgetWidthRatio;//保存widget窗口宽度比例
	QList<int> m_SpEditlSize;//保存的分割器编辑框大小
	QList<int> m_MaxWidth;//最大宽度
};
#endif // !XQSplitterWidget_H
