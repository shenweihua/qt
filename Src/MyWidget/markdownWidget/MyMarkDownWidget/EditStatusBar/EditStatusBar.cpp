﻿#include "EditStatusBar.h"
#include"XQBubbleWidget.h"
#include<QHBoxLayout>
EditStatusBar::EditStatusBar(QWidget* parent)
	:QWidget(parent)
{
	QSize btnSize = QSize(20, 20);
	auto Layout = new QHBoxLayout(this);
	
	init_sidebarBtn();
	init_SourceBtn();
	init_makeDownBtn();

	m_words = new QLabel("0字",this);
	m_words->setFont(QFont("黑体", 15));
	Layout->addWidget(m_sidebarBtn,1);
	Layout->addWidget(m_SourceBtn,1);
	Layout->addWidget(m_markDownBtn, 1);
	Layout->addStretch(10);
	Layout->addWidget(m_words,1,Qt::AlignRight);
	Layout->setContentsMargins(0, 0, 0, 0);//设置边距
	this->setLayout(Layout);
	m_Bubble = new XQBubbleWidget(nullptr);
}
EditStatusBar::~EditStatusBar()
{
	if (m_sidebarBtn != nullptr)
		delete m_sidebarBtn;
	if (m_SourceBtn != nullptr)
		delete m_SourceBtn;
	if (m_markDownBtn != nullptr)
		delete m_markDownBtn;
	if (m_words != nullptr)
		delete m_words;
	if (m_Bubble != m_Bubble)
		delete m_Bubble;
}
void EditStatusBar::Bubble_show(const QString& Text, const QPoint& pos)
{
	size_t h=m_Bubble->size().height();
	m_Bubble->setText(pos-QPoint(0,h+10), Text);
	m_Bubble->show();
}
void EditStatusBar::Bubble_hide()
{
	m_Bubble->hide();
}


void EditStatusBar::setWordCount(const size_t size)
{
	m_words->setText(QString("%1字").arg(size));
}