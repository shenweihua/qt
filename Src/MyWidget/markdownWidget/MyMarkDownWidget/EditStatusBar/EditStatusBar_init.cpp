﻿#include"EditStatusBar.h"
#include"MyMarkDownWidget.h"
#include"XQMouseEventFilter_enter.hpp"
#include"XQMouseEventFilter_leave.hpp"
void EditStatusBar::init_sidebarBtn()
{
	auto parentWidget = static_cast<MyMarkdownWidget*>(this->parent());
	QSize btnSize = QSize(20, 20);
	//目录按钮初始化
	m_sidebarBtn = new XQAnimationButton(":/sidebar/image/sidebar/gardenOne.png", ":/sidebar/image/sidebar/gardenTwo.png", this);
	connect(m_sidebarBtn, &QPushButton::clicked, [=]() {parentWidget->HiddenDisplayWidget(MyMarkdownWidget::widget::TitleDir); });
	m_sidebarBtn->setFixedSize(btnSize);
	//鼠标进入事件处理
	m_sidebarBtn->installEventFilter(new XQMouseEventFilter_Enter([=](QEnterEvent* ev)->bool {
		Bubble_show("目录", mapToGlobal(m_sidebarBtn->pos()));
	return false;
		}, m_sidebarBtn));
	//鼠标离开事件
	m_sidebarBtn->installEventFilter(new XQMouseEventFilter_Leave([this] {Bubble_hide(); }, m_sidebarBtn));
}
void EditStatusBar::init_SourceBtn()
{
	auto parentWidget = static_cast<MyMarkdownWidget*>(this->parent());
	QSize btnSize = QSize(20, 20);
	//源码按钮初始化
	m_SourceBtn = new XQAnimationButton(":/EditBtn/image/EditBtn/SourceBtn.png", ":/EditBtn/image/EditBtn/EnterBtn.png", this);
	connect(m_SourceBtn, &QPushButton::clicked, [=]() {parentWidget->HiddenDisplayWidget(MyMarkdownWidget::widget::SourceEdit); });
	m_SourceBtn->setFixedSize(btnSize);
	//鼠标进入事件处理
	m_SourceBtn->installEventFilter(new XQMouseEventFilter_Enter([=](QEnterEvent* ev)->bool {
		Bubble_show("源码编辑框", mapToGlobal(m_SourceBtn->pos()));
	return false;
		}, m_SourceBtn));
	//鼠标离开事件
	m_SourceBtn->installEventFilter(new XQMouseEventFilter_Leave([this] {Bubble_hide(); }, m_SourceBtn));
}
void EditStatusBar::init_makeDownBtn()
{
	auto parentWidget = static_cast<MyMarkdownWidget*>(this->parent());
	QSize btnSize = QSize(20, 20);
	//makedown按钮初始化
	m_markDownBtn = new XQAnimationButton(":/EditBtn/image/EditBtn/MakeDownBtn.png", ":/EditBtn/image/EditBtn/EnterBtn.png", this);
	connect(m_markDownBtn, &QPushButton::clicked, [=]() {parentWidget->HiddenDisplayWidget(MyMarkdownWidget::widget::MarkDownEdit); });
	m_markDownBtn->setFixedSize(btnSize);
	//鼠标进入事件处理
	m_markDownBtn->installEventFilter(new XQMouseEventFilter_Enter([=](QEnterEvent* ev)->bool {
		Bubble_show("markDown编辑框", mapToGlobal(m_markDownBtn->pos()));
	return false;
		}, m_markDownBtn));
	//鼠标离开事件
	m_markDownBtn->installEventFilter(new XQMouseEventFilter_Leave([this] {Bubble_hide(); }, m_markDownBtn));
}