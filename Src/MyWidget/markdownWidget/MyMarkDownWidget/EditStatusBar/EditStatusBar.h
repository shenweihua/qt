﻿#ifndef EDITSTATUSBAR_H
#define EDITSTATUSBAR_H
#include "XQAnimationButton.h"
#include<QLabel>
class XQBubbleWidget;
//编辑框状态栏
class EditStatusBar :public QWidget
{
	Q_OBJECT
public:
	EditStatusBar(QWidget* parent = nullptr);
	~EditStatusBar();
public slots:
	//字数提示设置
	void setWordCount(const size_t size);
	//气泡提示显示
	void Bubble_show(const QString& Text, const QPoint& pos);
	//气泡隐藏
	void Bubble_hide();
protected:
	void init_sidebarBtn();
	void init_SourceBtn();
	void init_makeDownBtn();
private:
	XQAnimationButton*	 m_sidebarBtn = nullptr;//侧边栏
	XQAnimationButton* m_SourceBtn = nullptr;//源码显示
	XQAnimationButton* m_markDownBtn = nullptr;//makeDown显示
	bool SourceMode = false;
	QLabel*			 m_words = nullptr;//字数统计
	XQBubbleWidget* m_Bubble=nullptr;//悬浮气泡提示文字
};

#endif // !EDITSTATUSBAR_H