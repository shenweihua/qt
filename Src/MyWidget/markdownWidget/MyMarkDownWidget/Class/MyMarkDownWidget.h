﻿#ifndef MARKDOWNWIDGET_H
#define MARKDOWNWIDGET_H
#define ImageDirectoryName "image"//图片目录名字
#include<QTextEdit>
#include<QTimer>
#include<QDir>
class EditStatusBar;
class QSplitter;
class TitleDirectory;
class XQSplitterWidget;
class XQSyntaxHiglighter;
class XQMarkDownEditorDataInfo;
//markdown编辑小部件
class MyMarkdownWidget :public QWidget
{
	Q_OBJECT
public:
	MyMarkdownWidget(XQMarkDownEditorDataInfo* data, QWidget* parent = nullptr);
	~MyMarkdownWidget();
    //获取纯文本
	QString PlainText()const;
	//获取makeDown标记的文本
	QString text()const;
	QString imageDir()const;
	enum widget//窗口
	{
		TitleDir,//标题目录窗口 
		SourceEdit,//源码文本编辑窗口
		MarkDownEdit//makedown编辑窗口 
	};
public slots:
	//隐藏显示窗口
	void HiddenDisplayWidget(const widget& w);
	//设置文本
	void setText(const QString& Text);
	//读取文本文件
	bool readTextFile(const QString& fileName);
	//写入文本文件
	bool writeTextFile(const QString& fileName);
	//跳转标题
	void JumpHeader(const QModelIndex& index);
	//跳转标题
	void JumpHeader(QTextEdit* edit,const QString&Text,const size_t count);
	//设置widget窗口宽度比例
	void setWidgetWidthRatio(const QList<int>& list);
	//打开图片位置
	void openImagePosition();
	//上传图片
	void upImage();
	//插入图片
	void insertImage(QAction* action);
	//设置光标行号
	void setCursorRow(QTextEdit* edit,const size_t row);
	//源码框文本改变时
	void SourceEditTextChanged();
	//源码框光标位置改变
	void SourceEditCursorPositionChanged();
protected:
	//初始化
	void init();
	//编辑框初始化
	void init_Edit();
	//快捷键初始化
	void init_ShortcutKey();
	//显示事件
	void showEvent(QShowEvent* event)override;
	//编辑框菜单初始化
	void init_EditMenu();
	//编辑框菜单显示
	void show_EditMenu();
	//获取图片添加到插入图片菜单
	void getImageToImageMenu();
private:
	XQMarkDownEditorDataInfo* m_data=nullptr;
	TitleDirectory* m_TitleDirectory;//标题目录
	QTextEdit* m_MarkDownEdit = nullptr;//编辑框
	QTextEdit* m_SourceEdit=nullptr;//编辑框
	XQSplitterWidget* m_SpEdit;//编辑框分割器
	XQSyntaxHiglighter* m_syntaxHiglighter = nullptr;
	EditStatusBar* m_EditStatusBar=nullptr;//编辑框状态栏
	QMenu* m_EditMenu = nullptr;//编辑框的菜单
	QMenu* insertImageMenu = nullptr;//插入图片的菜单
	size_t m_currentLine = 0;//当前光标所在行号
};

#endif // !MARKDOWNWIDGET_H
