﻿#include"MyMarkDownWidget.h"
#include"EditStatusBar.h"
#include"TitleDirectory.h"
#include"XQSplitterWidget.h"
#include"XQSyntaxHiglighter.h"
#include<QBoxLayout>
#include<QMenu>
//初始化
void MyMarkdownWidget::init()
{
	init_Edit();
	init_ShortcutKey();
	init_EditMenu();
	//初始化状态栏
	m_EditStatusBar = new EditStatusBar(this);
	m_EditStatusBar->setMaximumHeight(20)
		;
	//初始化标题目录栏
	m_TitleDirectory = new TitleDirectory(this);
	connect(m_TitleDirectory, &QTreeView::clicked, this, QOverload<const QModelIndex&>::of(&MyMarkdownWidget::JumpHeader));
	m_TitleDirectory->setFont(QFont("黑体", 20));

	//初始化分割器
	m_SpEdit = new XQSplitterWidget(this);
	m_SpEdit->addWidget(m_TitleDirectory);
	m_SpEdit->addWidget(m_SourceEdit);
	m_SpEdit->addWidget(m_MarkDownEdit);
	m_SpEdit->setWidgetWidthRatio({ 1,2,2 });

	//connect(m_SpEditl, &QSplitter::splitterMoved, [=]() {m_SpEditlSize=m_SpEditl->sizes(); });

	//组件布局
	QBoxLayout* mainlayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom);
	mainlayout->addWidget(m_SpEdit);
	mainlayout->addWidget(m_EditStatusBar);
	this->setLayout(mainlayout);

	m_syntaxHiglighter = new XQSyntaxHiglighter(m_MarkDownEdit->document());
}
void MyMarkdownWidget::init_EditMenu()
{
	m_EditMenu = new QMenu(this);
	auto undo=m_EditMenu->addAction("撤销", m_SourceEdit, &QTextEdit::undo,QKeySequence("Ctrl+Z"));
	undo->setEnabled(false);
	connect(m_SourceEdit, &QTextEdit::undoAvailable, [=](bool available) {
		undo->setEnabled(available); 
		});

	auto redo=m_EditMenu->addAction("重做", m_SourceEdit, &QTextEdit::redo, QKeySequence("Ctrl+Y"));
	redo->setEnabled(false);
	connect(m_SourceEdit, &QTextEdit::redoAvailable, [=](bool available) {
		redo->setEnabled(available);
		});
	m_EditMenu->addSeparator();

//图片菜单
	m_EditMenu->addAction("打开图片位置",this,&MyMarkdownWidget::openImagePosition);
	m_EditMenu->addAction("上传图片",this, &MyMarkdownWidget::upImage);
	insertImageMenu = new QMenu("插入图片", m_EditMenu);
	m_EditMenu->addMenu(insertImageMenu);
	connect(insertImageMenu, &QMenu::triggered,this,&MyMarkdownWidget::insertImage);
	//m_EditMenu->addAction("插入图片");
}
void MyMarkdownWidget::init_Edit()
{
	m_SourceEdit = new QTextEdit(this);
	m_SourceEdit->setAcceptRichText(false);
	m_SourceEdit->setFont(QFont("黑体", 20));
	m_SourceEdit->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_SourceEdit, &QTextEdit::customContextMenuRequested, this, &MyMarkdownWidget::show_EditMenu);
	m_MarkDownEdit = new QTextEdit(this);
	m_MarkDownEdit->setAcceptRichText(true);
	m_MarkDownEdit->setFont(QFont("黑体", 20));


	//m_TextEdit->setCurrentFont(QFont("微软雅黑",40));
	//m_TextEdit->setLineWrapMode(QTextEdit::NoWrap);
	//m_TextEdit->setAutoFormatting(QTextEdit::AutoNone);
	//光标位置改变时
	connect(m_SourceEdit, &QTextEdit::cursorPositionChanged,this, &MyMarkdownWidget::SourceEditCursorPositionChanged);
	//编辑m_TextEdit时，文本内容改变时
	connect(m_SourceEdit, &QTextEdit::textChanged,this, &MyMarkdownWidget::SourceEditTextChanged);

	//文本内容改变时链接统计编辑框字数发送到状态栏
	connect(m_MarkDownEdit, &QTextEdit::textChanged, [=]() {m_EditStatusBar->setWordCount(m_MarkDownEdit->toPlainText().size());
	////编辑m_MakeDownEdit时
	//QString PlainText = m_MarkDownEdit->toMarkdown();
	////qInfo() <<"MarkDownEdit"<< text;
	//if (m_MarkDownEdit == focusWidget() && PlainText != m_SourceEdit->toPlainText())
	//{
	//	m_SourceEdit->setPlainText(PlainText);
	//}
		});
}
