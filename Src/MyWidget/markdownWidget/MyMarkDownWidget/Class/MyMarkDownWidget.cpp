﻿#include"MyMarkDownWidget.h"
#include"XQMarkDownEditorDataInfo.h"
#include"TitleDirectory.h"
#include"XQSplitterWidget.h"
#include"XStringOperation.hpp"
#include"EditStatusBar.h"
#include<QTextBlock>
#include<QStandardItem>
#include<QMenu>
MyMarkdownWidget::MyMarkdownWidget(XQMarkDownEditorDataInfo* data, QWidget* parent)
	:QWidget(parent)
	,m_data(data)
{
	init();
	readTextFile(m_data->mdFilePath());
}

MyMarkdownWidget::~MyMarkdownWidget()
{
	if (m_TitleDirectory != nullptr)
		delete m_TitleDirectory;
	if (m_MarkDownEdit != nullptr)
		delete m_MarkDownEdit;
	if (m_SourceEdit != nullptr)
		delete m_SourceEdit;
	if (m_SpEdit != nullptr)
		delete m_SpEdit;
	if (m_EditStatusBar != nullptr)
		 m_EditStatusBar->deleteLater();
}

QString MyMarkdownWidget::PlainText() const
{
	return m_MarkDownEdit->toPlainText();
}

QString MyMarkdownWidget::text() const
{
	return m_SourceEdit->toPlainText();
}
QString MyMarkdownWidget::imageDir() const
{
	return m_data->imageDir();
}
void MyMarkdownWidget::HiddenDisplayWidget(const widget& w)
{
	m_SpEdit->HiddenDisplayWidget(w);
}

void MyMarkdownWidget::setText(const QString& Text)
{
	m_SourceEdit->setPlainText(Text);
}

void MyMarkdownWidget::JumpHeader(const QModelIndex& index)
{
	auto item = m_TitleDirectory->itemFromIndex(index);
	/*qInfo()<< item->text();
	qInfo() << item->data().toString();*/
	QList<QVariant> list = item->data().toList();//获取携带的数据
	size_t count = list[0].toInt();//这是第几个
	QString Source = list[1].toString();//源码编辑框标题文本
	QString MakeDown = item->text();//makedown显示框标题文本

	JumpHeader(m_SourceEdit, Source,count);
	JumpHeader(m_MarkDownEdit, MakeDown,count);
	//设置焦点
	if (m_SpEdit->widgetWidth(SourceEdit)==0)
	{
		m_MarkDownEdit->setFocus();
	}
	else
	{
		m_SourceEdit->setFocus();
	}
}
void MyMarkdownWidget::JumpHeader(QTextEdit* edit, const QString& Text, const size_t count)
{
	edit->setFocus();//设置焦点
	auto cursor = edit->textCursor();
	auto start= cursor.position();//开始的光标位置
	cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor);//移动到文章开头
	QString Line ;//获取当前光标下的一整行文本
	size_t n = 0;//记录出现次数
	do
	{
		Line= cursor.block().text();//获取当前行文本
		if (Text == Line)//找到了一行
		{
			n++;//记录+1
			if(n== count)//当前这行就是要找的返回
			{
				cursor.select(QTextCursor::LineUnderCursor);//选中当前行
				edit->setTextCursor(cursor);
				return;
			}
		}
	} while (cursor.movePosition(QTextCursor::Down, QTextCursor::MoveAnchor));//每次移动一行
	cursor.setPosition(start);//没找到设置回原先的位置，没有bug不会执行到这
	edit->setTextCursor(cursor);
}
void MyMarkdownWidget::setWidgetWidthRatio(const QList<int>& list)
{
	int sum = list[0] + list[1] + list[2];
	int w = size().width();
	m_SpEdit->setSizes({ w/sum* list[0],w / sum * list[1],w / sum * list[2] });
}


void MyMarkdownWidget::showEvent(QShowEvent* event)
{
	auto PlainText = XStringOperation::regex_replace( "!\\[.+?\\]\\((.+?)/.+?\\.\\w+?\\)", text(), { imageDir() });
	PlainText = XStringOperation::regex_replace("<img src=\"(.+?)//.+?\\.\\w+?\"", PlainText, { imageDir() });
	m_MarkDownEdit->setMarkdown(PlainText);
	//SourceEditTextChanged();
	m_TitleDirectory->setDirectory(text());
	//qInfo() << m_SpEditlSize;
}
void MyMarkdownWidget::show_EditMenu()
{
	getImageToImageMenu();
	m_EditMenu->move(QCursor::pos());
	m_EditMenu->show();
}