﻿#include "XQTyporaWidget.hpp"
#include"XQMarkDownEditorDataInfo.h"
#include<QProcess>
#include<QTimer>
#include<QFileInfo>
#include<QWindow>
#include<QBoxLayout>
#ifdef WIN32
#include<windows.h>
size_t XQTyporaWidget::FindWindowHWND()
{
	QString mdFilePath = m_data->mdFilePath();
	QString windowTitle = QFileInfo(mdFilePath).fileName() + " - Typora";
	wchar_t* wstr = new wchar_t[windowTitle.length() + 10] {0};
	windowTitle.toWCharArray(wstr);
	HWND hwnd = FindWindow(L"Chrome_WidgetWin_1", wstr); // 查找窗口句柄
	if (hwnd == NULL)//没找到尝试另一个名字
	{
		windowTitle = QFileInfo(mdFilePath).fileName() + QString("• - Typora");
		windowTitle.toWCharArray(wstr);
		hwnd = FindWindow(L"Chrome_WidgetWin_1", wstr); // 查找窗口句柄
	}
	delete[]wstr;
	return (size_t)hwnd; // 如果未找到匹配的窗口，则返回NULL
}
#endif
XQTyporaWidget::XQTyporaWidget(XQMarkDownEditorDataInfo* data, QWidget* parent)
	:QWidget(parent)
	,m_data(data)
{
	init();
}

XQTyporaWidget::~XQTyporaWidget()
{
}

//QString XQTyporaWidget::typoraExePath() const
//{
//	return m_exePath;
//}
//
//QString XQTyporaWidget::mdFilePath() const
//{
//	return m_mdFilePath;
//}
//
//void XQTyporaWidget::setMdFilePath(const QString& file)
//{
//	m_mdFilePath = file;
//}

void XQTyporaWidget::open_md()
{
	auto typora = new QProcess(this);
	typora->start(m_data->typoraExePath(), { m_data->mdFilePath()});
	//connect(typora, &QProcess::finished, typora, &QProcess::deleteLater);
	connect(this, &XQTyporaWidget::destroyed, this, [=] {
		if (typora) {
			typora->kill();
			typora->waitForFinished();
					}
		});
	//定时器轮询等待窗口打开
	QTimer* timer = new QTimer(this);
	timer->callOnTimeout([=]() {
		m_typoraHWND = FindWindowHWND();
	if (m_typoraHWND != 0)
	{
		init_typora();//初始化typora窗口
		timer->deleteLater();//释放自己
	}
		});
	timer->start(500);
}

//void XQTyporaWidget::setTyporaExePath(const QString& path)
//{
//	m_exePath = path;
//}
void XQTyporaWidget::init()
{
#ifdef WIN32
	//获取windows的缩放因子
	HDC hdc = GetDC(NULL);
	int dpiX = GetDeviceCaps(hdc, LOGPIXELSX);
	int dpiY = GetDeviceCaps(hdc, LOGPIXELSY);
	ReleaseDC(NULL, hdc);
	float scaleX = (float)dpiX / 96.0f;
	float scaleY = (float)dpiY / 96.0f;
	m_scale = qMax(scaleX, scaleY);
#endif // !WIN32
	//setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	open_md();
}
void XQTyporaWidget::init_typora()
{
#ifdef WIN32
	HWND hwnd = (HWND)m_typoraHWND;
	// 设置窗口样式
	LONG style = GetWindowLong(hwnd, GWL_STYLE);
	style &= ~(WS_CAPTION | WS_THICKFRAME);
	style |= WS_POPUP;
	SetWindowLong(hwnd, GWL_STYLE, style);
	//设置在所有窗口之上
	//SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	/*;
	SetWindowAboveTargetWindow(hwnd, (HWND)this->FindWindowHWND());*/
	// 更新窗口
	SetWindowPos(hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);

	//嵌入typora
	if (!(m_data->model() == editModel::Read))
	{
		//将typora嵌入进去
		SetParent((HWND)m_typoraHWND, (HWND)winId());
		m_typoraWindow = QWindow::fromWinId((WId)m_typoraHWND); // 将句柄转换为QWindow对象
		m_typoraWindow->resize(size());
		m_typoraWindow->setFramePosition(QPoint(0, 0));
	}
	else
	{
		auto window = QWindow::fromWinId((WId)m_typoraHWND); // 将句柄转换为QWindow对象
		auto typora = QWidget::createWindowContainer(window, this);//将typora嵌入一个qwidget中
		QBoxLayout* mainlayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);
		mainlayout->addWidget(typora,1);
		mainlayout->setContentsMargins(0, 0, 0, 0);
		setLayout(mainlayout);
	}
#endif
}