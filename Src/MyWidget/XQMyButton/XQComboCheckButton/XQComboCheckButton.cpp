﻿#include"XQComboCheckButton.h"
#include"XQActivationChangeEventFilter.hpp"
#include"XQAlgorithm.h"
#include<QListView>
#include<QMouseEvent>
#include<QStandardItemModel>
#include<QBoxLayout>

XQComboCheckButton::XQComboCheckButton(QWidget* parent)
	:QPushButton(parent)
{
	init();
	
}

XQComboCheckButton::XQComboCheckButton(const QString& text, QWidget* parent)
	:QPushButton(text,parent)
{
	init();
}

XQComboCheckButton::~XQComboCheckButton()
{
	if (m_list_view != nullptr)
		m_list_view->deleteLater();
	if (m_model != nullptr)
		m_model->deleteLater();
}

QList<int> XQComboCheckButton::selectIndexs() const
{
	QList<int>items;
	for (size_t i = 0; i < m_model->rowCount(); i++)
	{
		if (item(i)->checkState() == Qt::Checked)
			items << i;
	}
	return std::move(items);
}

QStringList XQComboCheckButton::selectTexts() const
{
	QStringList items;
	for (size_t i = 0; i < m_model->rowCount(); i++)
	{	if(item(i)->checkState()== Qt::Checked)
			items << item(i)->text();
	}
	return std::move(items);
}

QStringList XQComboCheckButton::textAll() const
{
	QStringList items;
	for (size_t i = 0; i < m_model->rowCount(); i++)
	{
		items << item(i)->text();
	}
	return std::move(items);
}

QStandardItem* XQComboCheckButton::item(int index) const
{
	return  m_model->item(index);
}


void XQComboCheckButton::showView()
{
	if (m_list_view->isActiveWindow()|| m_model->rowCount()==0)
	{
		m_list_view->hide();
		return;
	}
	int row = m_model->rowCount();//显示多少行
	if (m_showRow > 0 && m_showRow < row)
		row = m_showRow;
	int w = 0, h = 0;
	for (size_t i = 0; i < row; i++)
	{
		auto rect = m_list_view->visualRect(m_model->index(i, 0));
		w = qMax(w, rect.width());
		h += rect.height();
	}
	//m_list_view->setFixedWidth(w);
	m_list_view->setFixedHeight(h + 10);

	auto  globalPos = this->mapToGlobal(QPoint(0, 0));
	/*m_list_view->setFixedWidth(width());
	auto h=m_list_view->visualRect(m_model->index(0,0)).height();
	m_list_view->setFixedHeight(h* m_model->rowCount()+h*0.4);*/
	m_list_view->move(globalPos+QPoint(0,height()));
	m_list_view->show();
	m_list_view->activateWindow();
}

void XQComboCheckButton::setShowRowCount(int count)
{
	m_showRow = count;
}

void XQComboCheckButton::addItem(const QStringList& items, bool on)
{
	for (auto&item: items)
	{
		addItem(item,on);
	}
	//for (const QString& item : items) {
	//	QStandardItem* list_item = new QStandardItem(item);
	//	list_item->setCheckState(on ? Qt::Checked : Qt::Unchecked);
	//	m_model->appendRow(list_item);
	//}
	////auto  globalPos = this->mapToGlobal(QPoint(0, 0));
	//
	//int w = 0, h = 0;
	//for (size_t i = 0; i < m_model->rowCount(); i++)
	//{
	//	auto rect = m_list_view->visualRect(m_model->index(i, 0));
	//	w = qMax(w, rect.width());
	//	h += rect.height();
	//}
	////m_list_view->setFixedWidth(w);
	//m_list_view->setFixedHeight(h+10);
}

int XQComboCheckButton::addItem(const QString& item, bool on)
{
	QStandardItem* list_item = new QStandardItem(item);
	list_item->setCheckState(on ? Qt::Checked : Qt::Unchecked);
	m_model->appendRow(list_item);
	return list_item->row();
}

int XQComboCheckButton::addItem(const QString& item, const QVariant& value, int role, bool on)
{
	QStandardItem* list_item = new QStandardItem(item);
	list_item->setData(value,role);
	list_item->setCheckState(on ? Qt::Checked : Qt::Unchecked);
	m_model->appendRow(list_item);
	return list_item->row();
}

void XQComboCheckButton::setCheckState(int index, bool on)
{
	m_model->item(index)->setCheckState(on? Qt::Checked: Qt::Unchecked);
}

void XQComboCheckButton::setCheckState(const QString& text, bool on)
{
	auto items=m_model->findItems(text);
	for (auto&item:items)
	{
		item->setCheckState(on ? Qt::Checked : Qt::Unchecked);
	}
}

void XQComboCheckButton::setSelectIndexs(const QList<int>& list)
{
	for (size_t i = 0; i < m_model->rowCount(); i++)
	{
		if (list.indexOf(i) == -1)
			setCheckState(i,false);
		else
			setCheckState(i, true);
	}
}

void XQComboCheckButton::clear()
{
	m_model->clear();
}

void XQComboCheckButton::init()
{
	setCssFile(this,":/XQComboCheckBox/style.css");
	init_ui();
	m_model = new QStandardItemModel(this);
	m_list_view = new QListView();
	m_list_view->setModel(m_model);
	m_list_view->setWindowFlags(Qt::FramelessWindowHint);//设置无边框
	m_list_view->setEditTriggers(QAbstractItemView::NoEditTriggers);//设置只读
	m_list_view->setStyleSheet(styleSheet());
	//不是激活窗口时自动隐藏
	m_list_view->installEventFilter(new XQActivationChangeEventFilter([=] {
		if (!m_list_view->isActiveWindow() )
			m_list_view->hide();
		}, this));

	//显示下拉时
	m_list_view->installEventFilter(new XQEventFilterObject([=] {
		m_comboBoxBtn->setObjectName("comboBoxOn");
		setCssFile(this, ":/XQComboCheckBox/style.css"); }, QEvent::Show, this));
	//隐藏下拉时
	m_list_view->installEventFilter(new XQEventFilterObject([=] {
		m_comboBoxBtn->setObjectName("comboBoxOff");
		setCssFile(this, ":/XQComboCheckBox/style.css");
		}, QEvent::Hide, this));

	//显示视图
	connect(m_comboBoxBtn, &QPushButton::pressed, this, &XQComboCheckButton::showView);

	//多选框交互设置
	connect(m_list_view, &QListView::clicked, [=](const QModelIndex& index) {
		auto  item = m_model->item(index.row());
		if (item->checkState() == Qt::Unchecked)//如果时未选中时
			item->setCheckState(Qt::Checked);//设置选中
		else
			item->setCheckState(Qt::Unchecked);
		});
}

void XQComboCheckButton::init_ui()
{
	//setStyleSheet("background-color: rgba(255, 255, 255, 0);");
	setObjectName("text");
	m_comboBoxBtn = new QPushButton(this);
	m_comboBoxBtn->setObjectName("comboBoxOff");
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight,this);//从左到右
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	layout->addStretch(1);
	layout->addWidget(m_comboBoxBtn);
	setLayout(layout);
}
