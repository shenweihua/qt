﻿#include "XQLabelButton.h"
#include"XQAlgorithm.h"
#include"XQMouseEventFilter_enter.hpp"
#include<QLabel>
#include<QPushButton>
#include<QDebug>
XQBubbleWidget* XQLabelButton::m_bubble = nullptr;
XQLabelButton::XQLabelButton(QWidget* parent)
	:QWidget(parent)
{
	init();
}

XQLabelButton::XQLabelButton(const QString& text, QWidget* parent)
	:QWidget(parent),
	m_text(new QLabel(text, this))
{
	init();
}

XQLabelButton::~XQLabelButton()
{
	/*qInfo() << "释放标签:"<<m_text->text();*/
	if (m_background != nullptr)
		m_background -> deleteLater();
	if (m_icon != nullptr)
		m_icon->deleteLater();
	if (m_text != nullptr)
		m_text->deleteLater();
	if (m_clsBtn != nullptr)
		m_clsBtn->deleteLater();
}

QString XQLabelButton::text() const
{
	return m_text->text();
}

QPixmap XQLabelButton::icon() const
{
	return m_icon->pixmap();
}

bool XQLabelButton::checked() const
{
	return m_checked;
}

QVariant XQLabelButton::data(int role)
{
	return m_data[role];
}

void XQLabelButton::setText(const QString& text)
{
	m_text->setText(text);
}
bool XQLabelButton::isShowBubble() const
{
	return m_isShowBubble;
}
bool XQLabelButton::isCloseButtonShow() const
{
	return m_clsBtn->isVisible();
}
void XQLabelButton::setIcon(const QPixmap& pixmap)
{
	if (pixmap.isNull())
	{
		m_icon->hide();
	}
	else
	{
		m_icon->show();
	}
	m_icon->setPixmap(pixmap);
}

void XQLabelButton::setIcon(const QIcon& icon)
{
	setIcon(icon.pixmap(100,100));
}

void XQLabelButton::setChecked(bool flag)
{
	if (flag)
	{
		setCssFile(this, ":/XQLabelButton/style_select.css");
		m_background->setStyleSheet("");
	}
	else
	{
		setCssFile(this, ":/XQLabelButton/style.css");
	}
	m_checked = flag;
}

void XQLabelButton::setData(const QVariant& value, int role)
{
	m_data[role] = value;
}

void XQLabelButton::setCloseButtonShow(bool show)
{
	if (show)
		m_clsBtn->show();
	else
		m_clsBtn->hide();
		
}

void XQLabelButton::setFixedWidth(int w)
{
	if(w>0)
	{
		w = w - m_icon->width() - m_clsBtn->width();
		if (w > 0)
			m_text->setFixedWidth(w);
	}
	else
	{
		// 取消设置的固定宽度
		m_text->setMinimumWidth(0);
		m_text->setMaximumWidth(QWIDGETSIZE_MAX);  // 或者使用 setMaximumWidth(16777215);
	}
}

void XQLabelButton::setTextFixedWidth(int w)
{
	if (w > 0)
	{
		m_text->setFixedWidth(w);
	}
	else
	{
		// 取消设置的固定宽度
		m_text->setMinimumWidth(0);
		m_text->setMaximumWidth(QWIDGETSIZE_MAX);  // 或者使用 setMaximumWidth(16777215);
	}
}

void XQLabelButton::setTextAlignment(Qt::Alignment flag)
{
	m_text->setAlignment(flag);
}

void XQLabelButton::setShowBubble(bool show)
{
	m_isShowBubble = show;
}
