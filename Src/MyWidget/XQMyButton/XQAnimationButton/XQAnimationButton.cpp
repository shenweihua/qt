﻿#include"XQAnimationButton.h"
#include"XQBubbleWidget.h"
#include<QPixmap>
#include<QLabel>
#include<QDebug>
//XQBubbleWidget*  XQAnimationButton::m_Bubble = nullptr;
void XQAnimationButton::init()
{
    m_pixmatps.resize(5);
    //setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_State = mouseState::mouseLeave;
    //创建气泡提示框
    if (m_Bubble == nullptr)
    {
        m_Bubble = new XQBubbleWidget(nullptr);
    }
}
XQAnimationButton::XQAnimationButton(QWidget* parent)
    :QPushButton(parent)
{
    init();
}
XQAnimationButton::XQAnimationButton(QString FileName, int xnum, QWidget* parent, int ynum, const QString& bkGrnd) :QPushButton(parent)
{
    /*QPixmap pixmap(FileName);

    for (int cnt = 0; cnt < xnum; ++cnt)
    {
        for (int y = 0; y < ynum; ++y)
        {
            m_pixmatps->push_back(pixmap.copy(cnt * (pixmap.width() / xnum),
                y * (pixmap.height() / ynum),
                pixmap.width() / xnum,
                pixmap.height() / ynum));
        }
    }

    if (bkGrnd.isEmpty())
        background = new QPixmap(bkGrnd);
    else
        background = NULL;

    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_State = mouseLeave;*/
}


XQAnimationButton::XQAnimationButton(const QVector<QPixmap>& Pixmaps, QWidget* parent)
    :QPushButton(parent)
{
    init();
    setPixmaps(Pixmaps);
}
XQAnimationButton::XQAnimationButton(const QString& LeavePixmap, const QString& EnterPixmap, QWidget* parent)
    :QPushButton(parent)
{
    init();
    setMouseLeavePixmap(LeavePixmap);
    setMouseEnterPixmap(EnterPixmap);
}
XQAnimationButton::~XQAnimationButton()
{
    if (m_Bubble != nullptr)
    {
        m_Bubble->deleteLater();
        m_Bubble = nullptr;
    }
}

QPixmap XQAnimationButton::getPixmap(mouseState state)
{
    return m_pixmatps[int(state)];
}

void XQAnimationButton::setMouseState(mouseState state)
{
    m_State = state; 
    update();
}
void XQAnimationButton::Bubble_show(const QString& Text, const QPoint& pos)
{
   /* size_t h = m_Bubble->size().height();*/
    //m_Bubble->setText(pos + QPoint(m_Bubble->width()/2, -h - 5), Text);
  /*  m_Bubble->show();*/
    m_Bubble->showText(pos, Text);
}
void XQAnimationButton::Bubble_hide()
{
    m_Bubble->hide();
}
void XQAnimationButton::setBubbleText(const QString& Text, bool flag)
{
    m_BubbleText = Text;
    m_BubbleEnabled = flag;
}
QString XQAnimationButton::bubbleText()
{
    return m_BubbleText;
}
void XQAnimationButton::setBubbleEnabled(bool flag)
{
    m_BubbleEnabled = flag;
}
bool XQAnimationButton::isBubbleEnabled(bool flag)
{
    return m_BubbleEnabled;
}

void XQAnimationButton::clearPixmap()
{
    m_pixmatps.clear();
    m_pixmatps.resize(5);
    m_State = mouseState::mouseLeave;
    update();
}
