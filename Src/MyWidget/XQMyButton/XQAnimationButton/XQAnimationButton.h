﻿#ifndef XQANIMATIONBUTTON_H
#define XQANIMATIONBUTTON_H
#include <QPushButton>
#include <QString>
#include <QWidget>
enum class mouseState//鼠标状态
{
    mouseLeave,//鼠标不在上面,离开后
    mouseEnter,//鼠标进入
    mousePress,//鼠标按下
    mouseRelease,//鼠标松开
    buttonForbidden//按钮禁用
};
class XQBubbleWidget;
//重写的按钮,实现普通状态是一种图片，鼠标在上面是一种图片
class XQAnimationButton : public QPushButton
{
    Q_OBJECT
       // Q_PROPERTY(int curIndex READ getcurIndex WRITE setcurIndex)
public:
   
    XQAnimationButton(QWidget* parent = 0);
    XQAnimationButton(QString FileName, int xnum , QWidget* parent,int ynum , const QString& bkGrnd);
    /*XQAnimationButton(QVector<QString>& Vector, QWidget* parent = 0, QString bkGrnd = NULL);*/
    XQAnimationButton(const QVector<QPixmap>& Pixmaps, QWidget* parent = 0);
    XQAnimationButton(const QString& LeavePixmap, const QString& EnterPixmap, QWidget* parent = 0);
    ~XQAnimationButton();
public slots://槽函数
    //设置背景图片数组
    void setPixmaps(const QVector<QPixmap>& LPvector);
    //设置鼠标离开后的背景图片
    void setMouseLeavePixmap(const QPixmap& pixmap);
    //设置鼠标进入后的背景图片
    void setMouseEnterPixmap(const QPixmap& pixmap);
    //设置鼠标按下后的背景图片
    void setMousePressPixmap(const QPixmap& pixmap);
    //设置鼠标松开后的背景图片
    void setMouseReleasePixmap(const QPixmap& pixmap);
    //设置鼠标禁用后背景图片
    void setButtonForbiddenPixmap(const QPixmap& pixmap);
    //设置按钮是否禁用
    void setEnabled(bool flag =false);
    //获取背景图片数组
    const QVector<QPixmap>& getPixmaps(void) { return m_pixmatps; }
    //获取图片
    QPixmap getPixmap(mouseState state= mouseState::mouseLeave);
    //设置当前鼠标状态
    void setMouseState(mouseState state);
    //返回当前鼠标状态
    mouseState getMouseState(void) { return m_State; }
    //设置气泡文字
    void setBubbleText(const QString& Text,bool flag=true);
    //获取气泡文字
    QString bubbleText();
    //设置是否显示气泡文字
    void setBubbleEnabled(bool flag);
    //是否显示气泡文字
    bool isBubbleEnabled(bool flag);
    //清空图像
    void clearPixmap();
signals://信号
    //弹出菜单信号
    void contextMenu();
protected:
    //显示悬浮窗
    void Bubble_show(const QString& Text, const QPoint& pos);
    //气泡隐藏
    void Bubble_hide();
protected:
    //按钮背景，绘图事件
    virtual void paintEvent(QPaintEvent* event)override;
    //鼠标进入事件
    virtual void enterEvent(QEnterEvent* event)override;
    //鼠标离开事件
    virtual void leaveEvent(QEvent* event)override;
    //鼠标按下事件
    virtual void mousePressEvent(QMouseEvent* event)override;
    //鼠标松开事件
    virtual void mouseReleaseEvent(QMouseEvent* event)override;
    //鼠标右键菜单事件
    virtual void contextMenuEvent(QContextMenuEvent* event) override;
    //初始化
    void init();
protected:
    QVector<QPixmap>m_pixmatps;//鼠标各种状态时按钮的背景
    mouseState m_State = mouseState::mouseLeave;

    XQBubbleWidget* m_Bubble=nullptr;//悬浮气泡提示文字
    QString m_BubbleText;//气泡文字
    bool m_BubbleEnabled=false;//悬浮气泡开关
};

#endif // CLOSEBUTTON_H