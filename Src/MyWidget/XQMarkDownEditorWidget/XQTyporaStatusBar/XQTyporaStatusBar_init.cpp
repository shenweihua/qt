﻿#include"XQTyporaStatusBar.h"
#include"XQBubbleWidget.h"
#include"XQAnimationButton.h"
#include<QBoxLayout>
#include<QProgressBar>
#include<QPushButton>
void XQTyporaStatusBar::init()
{
	auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight, this);
	init_saveButton();
	init_DownloadImageButton();
	init_openImageDirButton();
	init_settings();
	init_openAttachment();
	//进度条初始化
	m_ProgressBar = new QProgressBar(this);
	m_ProgressBar->setValue(50);
	m_ProgressBar->setFormat("0/0MB->%p%");
	m_ProgressBar->setFixedWidth(200);
	//信息框初始化
	m_info = new QLabel("信息测试", this);
	m_info->setFont(QFont("黑体", 15));
	//加入布局
	Layout->addWidget(m_saveButton);
	Layout->addWidget(m_DownloadImageButton );
	Layout->addWidget(m_openImageDirButton);
	Layout->addWidget(m_settings);
	Layout->addWidget(m_openAttachment);
	Layout->addWidget(m_info,4);
	Layout->addWidget(m_ProgressBar,1);
	Layout->setContentsMargins(0, 0, 0, 0);//设置边距
	Layout->setSpacing(height());
	this->setLayout(Layout);
	//创建气泡提示框
	m_Bubble = new XQBubbleWidget(nullptr);
}
void XQTyporaStatusBar::init_saveButton()
{
	QSize btnSize = QSize(height(), height());
	//按钮初始化
	m_saveButton = new XQAnimationButton(":/sidebar/image/sidebar/upOne.png", ":/sidebar/image/sidebar/upTwo.png", this);
	m_saveButton->setButtonForbiddenPixmap(QPixmap(":/sidebar/image/sidebar/upForbidden.png"));
	m_saveButton->setBubbleText("上传");
	m_saveButton->setFixedSize(btnSize);
	connect(m_saveButton, &QPushButton::clicked, [this] {saveData(m_saveButton); });
	//m_saveButton->setEnabled(false);
}
void XQTyporaStatusBar::init_DownloadImageButton()
{
	QSize btnSize = QSize(height(), height());
	//按钮初始化
	m_DownloadImageButton = new XQAnimationButton(":/sidebar/image/sidebar/downImageOne.png", ":/sidebar/image/sidebar/downImageTwo.png", this);
	m_DownloadImageButton->setButtonForbiddenPixmap(QPixmap(":/sidebar/image/sidebar/downImageForbidden.png"));
	m_DownloadImageButton->setBubbleText("下载图片");
	m_DownloadImageButton->setFixedSize(btnSize);
	connect(m_DownloadImageButton, &QPushButton::clicked, [this] {DownloadImage(m_DownloadImageButton); });
	//m_DownloadImageButton->setEnabled(false);
}
void XQTyporaStatusBar::init_openImageDirButton()
{
	QSize btnSize = QSize(height(), height());
	//按钮初始化
	m_openImageDirButton = new XQAnimationButton(":/sidebar/image/sidebar/openImageOne.png", ":/sidebar/image/sidebar/openImageTwo.png", this);
	m_openImageDirButton->setButtonForbiddenPixmap(QPixmap(":/sidebar/image/sidebar/openImageForbidden.png"));
	m_openImageDirButton->setBubbleText("打开图片目录");
	m_openImageDirButton->setFixedSize(btnSize);
	connect(m_openImageDirButton, &QPushButton::clicked, [this] {openImageDir(m_openImageDirButton); });
	//m_openImageDirButton->setEnabled(false);
}

void XQTyporaStatusBar::init_settings()
{
	QSize btnSize = QSize(height(), height());
	//按钮初始化
	m_settings = new XQAnimationButton(":/sidebar/image/sidebar/openSettingsOne.png", ":/sidebar/image/sidebar/openSettingsTwo.png", this);
	m_settings->setButtonForbiddenPixmap(QPixmap(":/sidebar/image/sidebar/openSettingsForbidden.png"));
	m_settings->setBubbleText("打开设置窗口");
	m_settings->setFixedSize(btnSize);
	connect(m_settings, &QPushButton::clicked, [this] {openSettings(m_settings); });
}

void XQTyporaStatusBar::init_openAttachment()
{
	QSize btnSize = QSize(height(), height());
	//按钮初始化
	m_openAttachment = new XQAnimationButton(":/sidebar/image/sidebar/AttachmentOne.png", ":/sidebar/image/sidebar/AttachmentTwo.png", this);
	m_openAttachment->setButtonForbiddenPixmap(QPixmap(":/sidebar/image/sidebar/AttachmentForbidden.png"));
	m_openAttachment->setBubbleText("打开附件窗口");
	m_openAttachment->setFixedSize(btnSize);
	connect(m_openAttachment, &QPushButton::clicked, [this] {openAttachment(m_openAttachment); });
	//m_openAttachment->setEnabled(false);
}
