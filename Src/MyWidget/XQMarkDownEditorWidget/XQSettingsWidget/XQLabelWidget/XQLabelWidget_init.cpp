﻿#include"XQLabelWidget.h"
#include"XQBubbleWidget.h"
#include"XQMouseEventFilter_enter.hpp"
#include"XQMouseEventFilter_leave.hpp"
#include"XQListSearchWidget.h"
#include<QPushButton>
#include<QTableWidget>
#include<QHeaderView>
void XQLabelWidget::init()
{
	init_UI();
	m_Bubble = new XQBubbleWidget();
	m_Bubble->hide();

	m_ListSearch = new XQListSearchWidget();
	m_ListSearch->setActivityAutoClose(false);
	connect(m_ListSearch, &XQListSearchWidget::selectText, [=](const QString& Text) {
		m_ListSearch->hide();
		auto btn = init_btn(Text);//初始化一个按钮
	m_btns.append(btn);//加入数组
	updataTable();//更新显示

		});
}
void XQLabelWidget::init_UI()
{
	setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	setWindowOpacity(1);  // 设置透明度

	m_Layout = new QTableWidget(this);
	m_Layout->setRowCount(m_row);
	m_Layout->setColumnCount(m_column);

	m_Layout->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	// 去掉垂直标题
	m_Layout->verticalHeader()->setVisible(false);
	m_Layout->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	// 去掉水平标题
	m_Layout->horizontalHeader()->setVisible(false);
	m_Layout->setStyleSheet("QTableWidget { border: none; }");//去掉外部边框
	// 禁止编辑
	m_Layout->setEditTriggers(QAbstractItemView::NoEditTriggers);
	// 禁止选择
	m_Layout->setSelectionMode(QAbstractItemView::NoSelection);
	// 设置焦点策略为无焦点
	m_Layout->setFocusPolicy(Qt::NoFocus);
	//设置整行选取
	m_Layout->setSelectionBehavior(QAbstractItemView::SelectRows);
	// 去掉所有边框线
	m_Layout->setShowGrid(false);
	m_Layout->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	m_Layout->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	m_Layout->setWindowOpacity(1);  // 设置透明度

	m_Layout->setCellWidget(0, 0, init_addBtn());
}
QPushButton* XQLabelWidget::init_addBtn()
{
	m_addBtn = new QPushButton("添加标签", this);
	m_addBtn->setIcon(QIcon(":/sidebar/image/sidebar/addOne.png"));
	m_addBtn->setStyleSheet("QPushButton:hover { background-color: #00C8C8 }");//开启鼠标悬浮变色
	m_addBtn->setFont(QFont("微软雅黑", 10));
	connect(m_addBtn, &QPushButton::pressed, this, &XQLabelWidget::addButton);
	return m_addBtn;
}
QPushButton* XQLabelWidget::init_btn(const QString& text)
{
	auto btn = new QPushButton(text, this);
	btn->setIcon(QIcon(":/sidebar/image/sidebar/fork.png"));
	btn->setStyleSheet("QPushButton:hover { background-color: #00C8C8 }");//开启鼠标悬浮变色
	btn->setFont(QFont("微软雅黑", 10));
	connect(btn, &QPushButton::pressed, [=] {this->removeButton(btn); });//释放自己
	btn->installEventFilter(new XQMouseEventFilter_Enter(//鼠标进入事件
		[=] {m_Bubble->setText(mapToGlobal(btn->pos() + QPoint(0, -btn->size().height())), btn->text());
	m_Bubble->show(); }, btn));
	//鼠标离开事件
	btn->installEventFilter(new XQMouseEventFilter_Leave([=] {m_Bubble->hide(); }, btn));
	return btn;
}