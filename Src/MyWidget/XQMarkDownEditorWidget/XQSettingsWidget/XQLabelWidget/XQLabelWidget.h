﻿#ifndef XQLabelWidget_H
#define XQLabelWidget_H
#include<QWidget>
class QPushButton;
class QTableWidget;
class XQBubbleWidget;
class XQListSearchWidget;
//标签选择小部件
class XQLabelWidget:public QWidget
{
	Q_OBJECT
public:
	XQLabelWidget(QWidget* parent = nullptr);
	~XQLabelWidget();
	//获取设置的标签列表
	QStringList labelList();
	//获取全部数据(全部标签)
	QStringList& labelListData();
public slots://槽函数
	void setRowCount(size_t row);
	void setColumnCount(size_t column);
	//删除一个标签按钮
	void removeButton(QPushButton* btn);
	//增加一个标签按钮
	void addButton();
	//更新表格显示
	void updataTable(bool copy=true);
	//设置标签列表
	void setLabelList(const QStringList&list);
	//设置全部数据(全部标签)
	void setLabelListData(const QStringList& list);
signals://信号

protected://
	
protected://初始化
	void init();
	void init_UI();
	//初始化获得一个标签按钮
	QPushButton* init_btn(const QString& text);
	//初始化添加按钮
	QPushButton* init_addBtn();
	
protected://事件
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* event)override;
protected://变量
	QWidget* m_parent = nullptr;
	QTableWidget* m_Layout = nullptr;//布局
	QPushButton* m_addBtn = nullptr;//添加按钮
	QPoint m_addPoint = QPoint(0,0);//添加按钮当前点
	size_t m_row = 3;//行
	size_t m_column = 3;//列
	QList<QPushButton*> m_btns;//按钮列表
	XQBubbleWidget* m_Bubble = nullptr;//悬浮气泡
	XQListSearchWidget* m_ListSearch = nullptr;//列表搜索小部件
};
#endif // !XQLabelWidget_H
