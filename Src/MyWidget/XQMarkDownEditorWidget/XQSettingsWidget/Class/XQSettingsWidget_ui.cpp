﻿#include"XQSettingsWidget.h"
#include"XQLabelWidget.h"
#include"XQSurfacePlotWidget.h"
#include"XQAnimationButton.h"
#include"XQTitleBarWidget.h"
#include<QLineEdit>
#include<QTextEdit>
#include<QBoxLayout>
#include<QLabel>
void XQSettingsWidget::init_UI()
{
	setStyleSheet("background: qlineargradient(x1:0, y1:0, x2:1, y2:1, m_stop:0 #ffffff, m_stop:0.4 #00FFC8, m_stop:0.7 #00C8C8, m_stop:1 #00FFFF);");//初始化背景渐变色
	setWindowTitle("设置");//设置标题
	auto title = new XQTitleBarWidget(this);
	title->setButtons(XQTBWType::top | XQTBWType::minimized | XQTBWType::close);
	title->setMouseStretchWidget(false);
	auto TopLayout = new QBoxLayout(QBoxLayout::Direction::TopToBottom,this);//从上到下
	TopLayout->addWidget(title);
	TopLayout->addLayout(init_title());
	TopLayout->addLayout(init_labelAndImage());
	TopLayout->addLayout(init_abstract());
	TopLayout->addLayout(init_saveAndClose());
	setLayout(TopLayout);
}

QBoxLayout* XQSettingsWidget::init_title()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("标题:",this);
	label->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	label->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	label->setWindowOpacity(1);  // 设置透明度
	label->setFont(QFont("微软雅黑", 15));
	LeftLayout->addWidget(label);

	m_title = new QLineEdit(this);
	m_title->setFont(QFont("微软雅黑", 15));
	m_title->setPlaceholderText("在此输入的标题将在主界面显示");
	LeftLayout->addWidget(m_title);
	return LeftLayout;
}

QBoxLayout* XQSettingsWidget::init_label()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("标签:", this);
	label->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	label->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	label->setWindowOpacity(1);  // 设置透明度
	label->setFont(QFont("微软雅黑", 15));
	LeftLayout->addWidget(label,0, Qt::AlignVCenter| Qt::AlignLeft);

	m_labelWidget = new XQLabelWidget(this);
	m_labelWidget->setFixedHeight(150);//设置高度
	LeftLayout->addWidget(m_labelWidget,1);
	return LeftLayout;
}

QBoxLayout* XQSettingsWidget::init_surfacePlot()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("封面:", this);
	label->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	label->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	label->setWindowOpacity(1);  // 设置透明度
	label->setFont(QFont("微软雅黑", 15));
	LeftLayout->addWidget(label, 0, Qt::AlignVCenter | Qt::AlignLeft);

	m_surfacePlot = new XQSurfacePlotWidget(this);
	m_surfacePlot->setFixedHeight(150);//设置高度
	m_surfacePlot->setImageDir(imageDir());
	LeftLayout->addWidget(m_surfacePlot,1);
	return LeftLayout;
}

QBoxLayout* XQSettingsWidget::init_labelAndImage()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	LeftLayout->addLayout(init_label(),1);
	LeftLayout->addLayout(init_surfacePlot(),1);
	return LeftLayout;
}

QBoxLayout* XQSettingsWidget::init_abstract()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto label = new QLabel("摘要:", this);
	label->setWindowFlags(Qt::FramelessWindowHint);  // 将窗口去除边框
	label->setAttribute(Qt::WA_TranslucentBackground);  // 设置背景透明
	label->setWindowOpacity(1);  // 设置透明度
	label->setFont(QFont("微软雅黑", 15));
	LeftLayout->addWidget(label);

	m_abstract = new QTextEdit(this);
	m_abstract->setFont(QFont("微软雅黑", 15));
	m_abstract->setPlaceholderText("在此输入的摘要的将在主界面鼠标悬浮时以气泡显示");
	LeftLayout->addWidget(m_abstract);
	return LeftLayout;
}

QBoxLayout* XQSettingsWidget::init_saveAndClose()
{
	auto LeftLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
	auto save = new XQAnimationButton(":/sidebar/image/sidebar/saveOne.png", ":/sidebar/image/sidebar/saveTwo.png", this);
	auto closeWidget = new XQAnimationButton(":/sidebar/image/sidebar/closeOne.png", ":/sidebar/image/sidebar/closeTwo.png", this);
	save->setFixedSize(50, 50);
	save->setBubbleText("保存");
	connect(save, &QPushButton::pressed,this,&XQSettingsWidget::saveSignals);//发送保存信号并且隐藏窗口
	closeWidget->setFixedSize(50, 50);
	closeWidget->setBubbleText("关闭");
	connect(closeWidget, &QPushButton::pressed,this,&QWidget::deleteLater);//直接关闭窗口
	LeftLayout->addStretch(1);
	LeftLayout->addWidget(save);
	LeftLayout->addStretch(1);
	LeftLayout->addWidget(closeWidget);
	LeftLayout->addStretch(1);
	return LeftLayout;
}
