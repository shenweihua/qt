﻿#ifndef XQSETTINGSWIDGET_H
#define XQSETTINGSWIDGET_H
#include<QWidget>
class QLineEdit;
class XQLabelWidget;
class XQSurfacePlotWidget;
class QTextEdit;
class QBoxLayout;
//设置窗口小部件
class XQSettingsWidget :public QWidget
{
	Q_OBJECT
public:
	XQSettingsWidget(QWidget* parent = nullptr);
	~XQSettingsWidget();
	//获取标题
	QString title()const;
	//获取图片
	QPixmap image()const;
	//获取图像目录
	QString imageDir();
	//获取文章摘要
	QString abstract()const;
	//获取设置的标签列表
	QStringList labelList();
	//获取全部数据(全部标签)
	QStringList& labelListData();
public slots://槽函数
	void setImageDir(const QString& path);
	//设置标题
	void setTitle(const QString& text);
	//设置图片
	void setImage(const QPixmap& pixmap);
	//设置文章摘要
	void setAbstract(const QString& text);
	//设置标签列表
	void setLabelList(const QStringList& list);
	//设置全部数据(全部标签)
	void setLabelListData(const QStringList& list);
signals://信号
	void saveSignals();//发送保存信号//窗口将隐藏由父窗口读取数据后关闭窗口
protected://初始化
	void init();
	void init_UI();
	QBoxLayout* init_title();//标题初始化
	QBoxLayout* init_label();//文章标签初始化
	QBoxLayout* init_surfacePlot();//封面图初始化
	QBoxLayout* init_labelAndImage();//标签和封面初始化
	QBoxLayout* init_abstract();//文章摘要初始化
	QBoxLayout* init_saveAndClose();//保存关闭初始化
protected://事件
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
protected://变量
	QLineEdit* m_title=nullptr;//标题
	XQLabelWidget* m_labelWidget = nullptr;//文章标签
	XQSurfacePlotWidget* m_surfacePlot = nullptr;//封面图
	QTextEdit* m_abstract = nullptr;//文章摘要
	QString m_imageDir;//本地缓存图像路径
};
#endif // !XSettingsWidget_H
