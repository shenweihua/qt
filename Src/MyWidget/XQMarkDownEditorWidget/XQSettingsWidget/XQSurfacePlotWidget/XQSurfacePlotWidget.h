﻿#ifndef XQSURFACEPLOTWIDGET_H
#define XQSURFACEPLOTWIDGET_H
#include<QWidget>
class XQAnimationButton;
class XQFileSelectionWidget;
class QMenu;
//封面选择小部件
class XQSurfacePlotWidget :public QWidget
{
	Q_OBJECT
public:
	XQSurfacePlotWidget(QWidget* parent = nullptr);
	~XQSurfacePlotWidget();
	//获取图像目录
	QString imageDir();
	//获取图像
	QString imagePath();
	//获取图像
	QPixmap image();
public slots://槽函数
	void setImageDir(const QString& path);
	//设置图片
	void setImage(const QString& image);
	//设置图片
	void setImage(const QPixmap& image);
	//打开图片选择列表
	void openList();
	//弹出菜单
	void contextMenu();
signals://信号
	//选择信号
	void selectFile(const QString& file);
protected://初始化
	void init();
	//初始化添加的图标
	void init_addPixmap();
protected://事件
	//窗口大小改变事件
	virtual void resizeEvent(QResizeEvent* event)override;
	//鼠标右键菜单事件
	virtual void contextMenuEvent(QContextMenuEvent* event) override;
protected://变量
	bool imageNull = true;//图片是空的
	XQAnimationButton* m_ImageBtn=nullptr;//显示的图片
	XQFileSelectionWidget* m_fileList=nullptr;//选择图片的表格列表
	QString m_imageFile;//要显示的图片
	QString m_imageDir;//本地缓存图像目录
	QMenu* menu=nullptr;//右键菜单
};
#endif // !XQSurfacePlotWidget_H
