﻿#include "XQSurfacePlotWidget.h"
#include"XQAnimationButton.h"
void XQSurfacePlotWidget::init_addPixmap()
{
	m_ImageBtn->setMouseLeavePixmap(QPixmap(":/sidebar/image/sidebar/addOne.png"));
	m_ImageBtn->setMouseEnterPixmap(QPixmap(":/sidebar/image/sidebar/addTwo.png"));
	//将按钮移动到中间选择图片
	m_ImageBtn->resize(QSize(50, 50));
	int x = (size().width() - 50) / 2;
	int y = (size().height() - 50) / 2;
	m_ImageBtn->move(x, y);
}

void XQSurfacePlotWidget::init()
{
	m_ImageBtn = new XQAnimationButton(this);
	m_ImageBtn->setBubbleText("选择图片");
	connect(m_ImageBtn, &QPushButton::pressed, this, &XQSurfacePlotWidget::openList);
	connect(m_ImageBtn, &XQAnimationButton::contextMenu, this, &XQSurfacePlotWidget::contextMenu);
}