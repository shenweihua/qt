﻿#ifndef XQMARKDOWNEDITORDATAINFO_H
#define XQMARKDOWNEDITORDATAINFO_H
#include<QObject>
#include<QDir>
//使用的编辑器小部件
enum class editWidget
{
	MyMarkDownEditor,//自己写的markDown编辑器
	typora//typora markDown编辑器
};
//编辑器模式
enum class editModel
{
	Readwrite,//可读写
	Read,//只读
	New//新建
};
//mark编辑器的数据类
class XQMarkDownEditorDataInfo:public QObject
{
	Q_OBJECT
public:
	XQMarkDownEditorDataInfo(QObject* parent = nullptr);
	XQMarkDownEditorDataInfo(editModel model = editModel::Readwrite, QObject* parent = nullptr);
	~XQMarkDownEditorDataInfo();
	
	//获取当前模式
	editModel model()const;
	//获取编辑器种类
	editWidget editType()const;
	//获取XTypora.exe路径
	QString typoraExePath()const;
	//获取目录路径
	QString dirPath()const;
	//获取md文本路径
	QString mdFilePath()const;
	//获取图像路径
	QString imageDir()const;
	//获取附件目录路径
	QString filePath()const;
	//查看md文件的md5值
	QString mdFileOfMD5()const;
	//md文件是否修改了
	bool isMdFileModify()const;
public slots:
	//设置模式
	void setModel(editModel model);
	//设置编辑器种类
	void setEditType(editWidget type);
	//设置XTypora.exe路径
	void setTyporaExePath(const QString& path);
	//设置路径(目录)
	void setDirPath(const QString& path);
	//设置md文件名
	void setMdFilePathName(const QString& name);
	//设置md文件路径
	void setMdFilePath(const QString& path);
	//设置图像目录名
	void setImageDirName(const QString& name);
	//设置图像目录
	void setImageDirPath(const QString& path);
	//设置附件文件目录名
	void setFileDirName(const QString& name);
	//设置附件文件目录
	void setFileDirPath(const QString& path);
	//更新md文件的md5值
	void upDataMd5();
signals://信号
	//编辑器模式改变
	void modelChanged(editModel model);
	//编辑器种类改变
	void editTypeChanged(const editWidget& type);
	//typora.exe程序路径改变
	void typoraExePathChanged(const QString& path);
	//目录改变
	void DirChanged(const QString& path);
	//md文件改变
	void mdFileChanged(const QString& path);
	//图像目录改变
	void ImageDirChanged(const QString& path);
	//附件文件目录改变
	void fileDirChanged(const QString& path);
protected:
	editWidget m_editType = editWidget::typora;//编辑器类型
	QString m_typoraExePath;//exe路径
	QDir m_path;//目录
	QString m_mdFilePath;//md文件路径
	QString m_mdFileMD5;//md文件的md5值
	QDir m_ImagePath;//图像目录
	QDir m_filePath;//附件目录
	editModel m_model= editModel::Readwrite;//当前模式
};
#endif // !XQMarkDownEditorData_H
