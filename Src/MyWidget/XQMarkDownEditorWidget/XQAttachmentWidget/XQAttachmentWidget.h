﻿#ifndef XQATTACHMENTWIDGET_H
#define XQATTACHMENTWIDGET_H
#include<QWidget>
class XQFileTableWidget;
class QStackedLayout;
class QBoxLayout;
class QFileSystemWatcher;
class XQAnimationButton;
class QLabel;
class XQMarkDownEditorDataInfo;
//附件窗口小部件
class XQAttachmentWidget:public QWidget
{
	Q_OBJECT
public:
	XQAttachmentWidget(QWidget* parent = nullptr,bool AutoUi=true);
public slots:
	//设置图片目录
	void setImageDir(const QString& dir);
	//设置文件路径
	void setFilePath(const QString& dir);
signals://信号
	/*void setFilePath(const QString& dir);*/
protected:
	//切换堆栈的小部件
	void toggleWidget(int nSel,const QPixmap& pixmap);
	//更新显示的统计信息
	void updateInfo(XQFileTableWidget* table);
protected:
	//初始化
	virtual void init();
	//初始化ui
	virtual void uiInit();
	//初始化左边的按钮组
	virtual QBoxLayout* leftButtonsInit();
	//初始化上边的按钮组
	virtual QBoxLayout* topButtonsInit();
	//初始化图片小部件
	virtual QWidget* imageWidgetInit();
	//初始化文件小部件
	virtual QWidget* fileWidgetInit();
protected://事件
	//窗口显示事件
	void showEvent(QShowEvent* event)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
	//拖拽进入事件
	void dragEnterEvent(QDragEnterEvent* event) override;
	//拖拽完成事件
	void dropEvent(QDropEvent* event) override;
protected://成员变量
	QStackedLayout* m_StackedLayout=nullptr;//堆栈布局
	XQFileTableWidget* m_imageTable = nullptr;//图片列表
	XQFileTableWidget* m_fileTable = nullptr;//文件列表
	XQAnimationButton* m_addFileBtn = nullptr;//添加文件按钮
	XQAnimationButton* m_deleteFileBtn = nullptr;//删除文件按钮
	QLabel* m_info = nullptr;//信息提示
	QLabel* m_ImageDisplay = nullptr;//图像展示
};
#endif