﻿#include "XQAttachmentWidget.h"
#include "XQAnimationButton.h"
#include"XQFileTableWidget.h"
#include"XSymbolOverload.h"
#include"XQMarkDownEditorDataInfo.h"
#include<QDebug>
#include<QStackedLayout>
#include<QFileDialog>

void XQAttachmentWidget::init()
{
	setAcceptDrops(true); // 启用拖拽功能
	uiInit();
	
	//添加文件按钮
	connect(m_addFileBtn, &XQAnimationButton::pressed, [=]() {
		QString options="All files (*.*)";
		if (m_StackedLayout->currentWidget() == (QWidget*)m_imageTable)//如果是图片
		{
			options = QString("Image Files(%1)").arg(QStringList(IMAGENAMEFILTERS).join(" "));
		}
		auto list=QFileDialog::getOpenFileNames(this,"添加文件","", options);
		((XQFileTableWidget*)m_StackedLayout->currentWidget())->addFile(list);
		});
	//删除文件按钮
	connect(m_deleteFileBtn, &XQAnimationButton::pressed, [=]() {
		auto table = ((XQFileTableWidget*)m_StackedLayout->currentWidget());
		QList<QTableWidgetItem*> items = table->selectedItems();
		QSet<int> selectedRows;//保存行号
		QStringList list;
		for (auto&item:items)//遍历行号
		{
			selectedRows.insert(item->row());
		}
		for (auto& row : selectedRows)//获取要删除的文件路径
		{
			list << table->item(row, XQFileTableWidget::Type::name)->data(Qt::UserRole).toString();
		}
		table->removeFile(list);
		});
	
	//表格内容更新了
	connect(m_imageTable, &XQFileTableWidget::tableFileChange, [this] {updateInfo(m_imageTable); });
	connect(m_fileTable, &XQFileTableWidget::tableFileChange, [this] {updateInfo(m_fileTable); });
	m_imageTable->setFileMaxSize(5_MB);//限制图片最大5MB
	m_imageTable->setFileSumMaxSize(200_MB);//限制图片最大200MB
	m_fileTable->setFileMaxSize(100_MB);//限制文件最大100MB
	m_fileTable->setFileSumMaxSize(1024_MB);//限制文件最大1024MB
	/*m_imageTable->addCloudFile("新海天.jpg", 121340);
	m_imageTable->addCloudFile("天天天.jpg", 121340);*/
}
