﻿#include"XQMarkDownEditorWidget.h"
#include"XQAttachmentWidget.h"
#include"XQSettingsWidget.h"
#include<QDesktopServices>
#include<QPushButton>
#include<QUrl>
void XQMarkDownEditorWidget::saveData(QPushButton* btn)
{
	m_editData->upDataMd5();//更新md文件的md5值，表示已经上传到服务器了
}

void XQMarkDownEditorWidget::DownloadImage(QPushButton* btn)
{

}

void XQMarkDownEditorWidget::openImageDir(QPushButton* btn)
{
	QDesktopServices::openUrl(QUrl(QString("file:%1").arg(m_editData->imageDir()), QUrl::TolerantMode));
}

void XQMarkDownEditorWidget::openSettings(QPushButton* btn)
{
	auto Widget = new XQSettingsWidget();
	Widget->setFixedSize(600, 500);
	Widget->setLabelListData({ "你好吗","在这里","你好吗","你好吗","在这里","你好吗" ,"你好吗","在这里","你好吗" ,"你好吗","在这里","你好吗","测试" });
	Widget->setImageDir(m_editData->imageDir());
	Widget->show();
	Widget->activateWindow();//激活窗口
	/*connect(m_SettingsWidget, &XQSettingsWidget::saveSignals, this, &XQMarkDownEditorWidget::saveSettingsData);*/
}

void XQMarkDownEditorWidget::openAttachment(QPushButton* btn)
{
	//初始化附件窗口
	auto widget = new XQAttachmentWidget(nullptr);
	widget->resize(700, 500);
	widget->setImageDir(m_editData->imageDir());
	widget->setFilePath(m_editData->filePath());
	widget->show();
	widget->activateWindow();//激活窗口
}
