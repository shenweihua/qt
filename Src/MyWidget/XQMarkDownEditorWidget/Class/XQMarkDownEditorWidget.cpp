﻿#include"XQMarkDownEditorWidget.h"
#include"XQAttachmentWidget.h"
#include"XMD5.h"
#include"XQSettingsWidget.h"
#include"XQAlgorithm.h"
#include<QDir>
#include<QFileInfo>
#include<fstream>
#include<QBoxLayout>
XQMarkDownEditorWidget::XQMarkDownEditorWidget( QWidget* parent) 
	:QWidget(parent)
{
	init();
}

XQMarkDownEditorWidget::XQMarkDownEditorWidget(XQMarkDownEditorDataInfo* editDataInfo, QWidget* parent)
	:QWidget(parent)
	,m_editData(editDataInfo)
{
	m_editData->setParent(this);
	init();
	/*setDirPath(path);*/
}

XQMarkDownEditorWidget::~XQMarkDownEditorWidget()
{
	if (m_editData != nullptr)
		m_editData->deleteLater();
}

XQMarkDownEditorDataInfo* XQMarkDownEditorWidget::editDataInfo()
{
	return m_editData;
}
QString XQMarkDownEditorWidget::text() const
{
	if(m_editData==nullptr)
		return QString();
	QFile file(m_editData->mdFilePath());
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return QString();
	return file.readAll();
}
void XQMarkDownEditorWidget::openEditWidget()
{
	
	if (m_EditTextWidget != nullptr)
	{
		m_EditTextWidget->deleteLater();
		m_Layout->removeWidget(m_EditTextWidget);
		m_EditTextWidget = nullptr;
	}
	m_EditTextWidget=init_editTextWidget();
	
	if (m_EditTextWidget != nullptr)
		m_Layout->insertWidget(0, m_EditTextWidget,1);
	m_editData->upDataMd5();//更新md5值
}

void XQMarkDownEditorWidget::setEditDataInfo(XQMarkDownEditorDataInfo* data)
{
	m_editData = data;
	data->setParent(this);
}
void XQMarkDownEditorWidget::setText(QString& text)
{
	if (m_editData == nullptr)
		return ;
	QFile file(m_editData->mdFilePath());
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return ;
	QTextStream in(&file);
	in << text;
}
void XQMarkDownEditorWidget::setWindowTitle(const QString& name)
{
	if (m_editData->model()==editModel::Read)
		QWidget::setWindowTitle(name + "-只读");
	else
		QWidget::setWindowTitle(name);
}
