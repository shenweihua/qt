﻿#ifndef XQMARKDOWNEDITORWIDGET_H
#define XQMARKDOWNEDITORWIDGET_H
#include"XQMarkDownEditorDataInfo.h"
#include<QDir>
#include<QWidget>
#include<QPushButton>
class QPushButton;
class XQTyporaStatusBar;
class XQSettingsWidget;
class XQAttachmentWidget;
class QBoxLayout;

//markdown编辑小部件
class XQMarkDownEditorWidget:public QWidget
{
	Q_OBJECT
public:
	XQMarkDownEditorWidget(QWidget* parent = nullptr);
	XQMarkDownEditorWidget(XQMarkDownEditorDataInfo* editDataInfo,QWidget* parent = nullptr);
	virtual~XQMarkDownEditorWidget();
	//获取编辑器数据信息
	XQMarkDownEditorDataInfo* editDataInfo();
	//获取文本
	QString text()const;
public slots:
	//设置窗口名字
	virtual void setWindowTitle(const QString& name);
	//打开文本编辑器窗口
	virtual void openEditWidget();
	//设置编辑器数据
	virtual void setEditDataInfo(XQMarkDownEditorDataInfo* data);
	//设置文本
	void setText(QString& text);
signals://信号
	//状态栏显示提示信息
	void showMessage(const QString& text, int timeout = 0);
protected:
	
protected:
	void init();//初始化
	void init_ui();//ui初始化
	//文本编辑器初始化
	QWidget* init_editTextWidget();
	//typora状态栏初始化
	QWidget* init_typoraStatusBarWidget();
protected://处理状态栏的信息
	//保存上传数据
	virtual void saveData(QPushButton* btn);
	//下载图片
	virtual void DownloadImage(QPushButton* btn);
	//打开图片目录信号
	virtual void openImageDir(QPushButton* btn);
	//打开设置窗口
	virtual void openSettings(QPushButton* btn);
	//打开附件窗口
	virtual void openAttachment(QPushButton* btn);
protected://事件
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* event)override;
	//窗口移动事件
	void moveEvent(QMoveEvent* event)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
	//窗口状态改变了
	void changeEvent(QEvent* event)override;
	//窗口显示时事件
	void showEvent(QShowEvent* event)override;
	//键盘按下事件
	void keyPressEvent(QKeyEvent* event)override;
protected:
	XQMarkDownEditorDataInfo* m_editData=nullptr;//编辑器数据
	QBoxLayout* m_Layout = nullptr;
	QWidget* m_EditTextWidget = nullptr;//编辑文本小部件
	XQTyporaStatusBar* m_TyporaStatusBar = nullptr;//状态栏
};

#endif // !
