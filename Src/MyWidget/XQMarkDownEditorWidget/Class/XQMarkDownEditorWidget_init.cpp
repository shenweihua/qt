﻿#include"XQMarkDownEditorWidget.h"
#include"MyMarkDownWidget.h"
#include"XQTyporaWidget.hpp"
#include"XQTyporaStatusBar.h"
#include"XQAttachmentWidget.h"
#include<QBoxLayout>
void XQMarkDownEditorWidget::init()
{
	init_ui();

	//提示信息
	connect(this, &XQMarkDownEditorWidget::showMessage, m_TyporaStatusBar, &XQTyporaStatusBar::showInfo, Qt::QueuedConnection);
	//保存数据
	connect(m_TyporaStatusBar, &XQTyporaStatusBar::saveData, this, &XQMarkDownEditorWidget::saveData);
	//下载图片
	connect(m_TyporaStatusBar, &XQTyporaStatusBar::DownloadImage, this, &XQMarkDownEditorWidget::DownloadImage);
	//打开图片目录
	connect(m_TyporaStatusBar, &XQTyporaStatusBar::openImageDir, this, &XQMarkDownEditorWidget::openImageDir);
	//打开设置窗口
	connect(m_TyporaStatusBar, &XQTyporaStatusBar::openSettings, this, &XQMarkDownEditorWidget::openSettings);
	//打开附件窗口
	connect(m_TyporaStatusBar, &XQTyporaStatusBar::openAttachment, this, &XQMarkDownEditorWidget::openAttachment);
	
}
void XQMarkDownEditorWidget::init_ui()
{
	m_Layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);//从上到下
	//m_Layout->addWidget(init_editTextWidget());
	m_Layout->addWidget(init_typoraStatusBarWidget());
	m_Layout->setContentsMargins(0, 0, 0, 0);//设置边距
	setLayout(m_Layout);
}

QWidget* XQMarkDownEditorWidget::init_editTextWidget()
{
	QWidget* widget = nullptr;
	if (m_editData->editType() == editWidget::MyMarkDownEditor)
		widget = new MyMarkdownWidget(m_editData, this);
	if (m_editData->editType() == editWidget::typora)
	{
		widget = new XQTyporaWidget(m_editData, this);
		//((XQTyporaWidget*)widget)->open_md();
	}
	return widget;
}

QWidget* XQMarkDownEditorWidget::init_typoraStatusBarWidget()
{
	m_TyporaStatusBar = new XQTyporaStatusBar(this);
	m_TyporaStatusBar->setFixedHeight(30);
	return m_TyporaStatusBar;
}
