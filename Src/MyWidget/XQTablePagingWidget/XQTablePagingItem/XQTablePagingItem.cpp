﻿#include"XQTablePagingItem.h"

XQTablePagingItem::XQTablePagingItem(const QList<QPair<QString,QVariant>>& data)
{
	m_data.append(data);
}
size_t XQTablePagingItem::size() const
{
	return m_data.size();
}
const QString XQTablePagingItem::text(int nSel) const
{
	if (m_data.size() <= nSel)
		return QString();
	return m_data[nSel].first;
}

const QVariant XQTablePagingItem::data(int nSel)const 
{
	if (m_data.size() <= nSel)
		return QVariant();
	return m_data[nSel].second;
}


void XQTablePagingItem::push_back(const QStringList& text)
{
	for (auto& t:text)
	{
		push_back(t);
	}
}

void XQTablePagingItem::push_back(double num, int precision)
{
	XQTablePagingItem::push_back(QString::number(num, 'f', precision), num);
}

void XQTablePagingItem::push_back(int num)
{
	XQTablePagingItem::push_back(QString::number(num), num);
}

void XQTablePagingItem::push_back(uint num)
{
	XQTablePagingItem::push_back(QString::number(num), num);
}

void XQTablePagingItem::push_back(qlonglong num)
{
	XQTablePagingItem::push_back(QString::number(num), num);
}

void XQTablePagingItem::push_back(qulonglong num)
{
	XQTablePagingItem::push_back(QString::number(num), num);
}

void XQTablePagingItem::push_back(const QString& text,const QVariant& value)
{
	m_data.push_back(QPair<QString,QVariant>(text,value));
}

void XQTablePagingItem::push_back(const QString&& text,QVariant&& value )
{
	m_data.push_back(QPair<QString, QVariant>(std::move(text), std::move(value)));
}

void XQTablePagingItem::push_front(const QStringList& text)
{
	for (auto& t : text)
	{
		push_front(t);
	}
}

void XQTablePagingItem::push_front(const QString& text, const QVariant& value)
{
	m_data.push_front(QPair<QString, QVariant>(text, value));
}

void XQTablePagingItem::push_front(const QString&& text, QVariant&& value)
{
	m_data.push_front(QPair<QString, QVariant>(std::move(text), std::move(value)));
}

void XQTablePagingItem::push_front(double num, int precision)
{
	XQTablePagingItem::push_front(QString::number(num, 'f', precision), num);
}

void XQTablePagingItem::push_front(int num)
{
	XQTablePagingItem::push_front(QString::number(num), num);
}

void XQTablePagingItem::push_front(uint num)
{
	XQTablePagingItem::push_front(QString::number(num), num);
}

void XQTablePagingItem::push_front(qlonglong num)
{
	XQTablePagingItem::push_front(QString::number(num), num);
}

void XQTablePagingItem::push_front(qulonglong num)
{
	XQTablePagingItem::push_front(QString::number(num), num);
}

QList<QPair<QString, QVariant>>::iterator XQTablePagingItem::begin()
{
	return m_data.begin();
}

QList<QPair<QString, QVariant>>::const_iterator XQTablePagingItem::begin() const
{
	return m_data.begin();
}

QList<QPair<QString, QVariant>>::reverse_iterator XQTablePagingItem::rbegin()
{
	return m_data.rbegin();
}

QList<QPair<QString, QVariant>>::const_reverse_iterator XQTablePagingItem::rbegin() const
{
	return m_data.rbegin();
}

QList<QPair<QString, QVariant>>::iterator XQTablePagingItem::end()
{
	return m_data.end();
}

QList<QPair<QString, QVariant>>::const_iterator XQTablePagingItem::end() const
{
	return m_data.end();
}

QList<QPair<QString, QVariant>>::reverse_iterator XQTablePagingItem::rend()
{
	return m_data.rend();
}

QList<QPair<QString, QVariant>>::const_reverse_iterator XQTablePagingItem::rend() const
{
	return m_data.rend();
}

QPair<QString, QVariant>& XQTablePagingItem::operator[](int nSel)
{
	return m_data[nSel];
}

const QPair<QString, QVariant>& XQTablePagingItem::operator[](int nSel) const
{
	return m_data[nSel];
}

XQTablePagingItem& XQTablePagingItem::operator<<(const QPair<QString, QVariant>& data)
{
	m_data.push_back(data);
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(QPair<QString, QVariant>&& data)
{
	m_data.push_back(std::move(data));
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(const QString& text)
{
	m_data.push_back(QPair<QString, QVariant>(text, QVariant()));
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(QString&& text)
{
	m_data.push_back(QPair<QString, QVariant>(std::move(text), QVariant()));
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(double num)
{
	push_back(num);
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(int num)
{
	push_back(num);
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(uint num)
{
	push_back(num);
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(qlonglong num)
{
	push_back(num);
	return *this;
}

XQTablePagingItem& XQTablePagingItem::operator<<(qulonglong num)
{
	push_back(num);
	return *this;
}
