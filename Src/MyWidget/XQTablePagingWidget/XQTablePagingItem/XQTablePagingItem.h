﻿#ifndef XQTABLEPAGINGITEM_H
#define XQTABLEPAGINGITEM_H
#include<QVariant>
//分页表格一行数据
class XQTablePagingItem
{
public:
	XQTablePagingItem() = default;
	XQTablePagingItem(const QList<QPair<QString,QVariant>>& data);
public:
	size_t size()const;
	const QString  text(int nSel)const;
	const QVariant data(int nSel)const;
public:
	void push_back(const QStringList& text);
	void push_back(const QString& text, const QVariant& value= QVariant());
	void push_back(const QString&& text, QVariant&& value=std::move(QVariant()));
	void push_back(double num, int precision = 2);
	void push_back(int num);
	void push_back(uint num);
	void push_back(qlonglong num);
	void push_back(qulonglong num);
	void push_front(const QStringList& text);
	void push_front(const QString& text, const QVariant& value = QVariant());
	void push_front(const QString&& text, QVariant&& value = std::move(QVariant()));
	void push_front(double num, int precision = 2);
	void push_front(int num);
	void push_front(uint num);
	void push_front(qlonglong num);
	void push_front(qulonglong num);
public:
	QList<QPair<QString, QVariant>>::iterator begin();
	QList<QPair<QString, QVariant>>::const_iterator begin()const;
	QList<QPair<QString, QVariant>>::reverse_iterator rbegin();
	QList<QPair<QString, QVariant>>::const_reverse_iterator rbegin()const;
	QList<QPair<QString, QVariant>>::iterator end();
	QList<QPair<QString, QVariant>>::const_iterator end()const;
	QList<QPair<QString, QVariant>>::reverse_iterator rend();
	QList<QPair<QString, QVariant>>::const_reverse_iterator rend()const;
public:
	QPair<QString, QVariant>& operator[](int nSel);
	const QPair<QString, QVariant>& operator[](int nSel)const;
	XQTablePagingItem& operator<<(const QPair<QString, QVariant>& data);
	XQTablePagingItem& operator<<(QPair<QString, QVariant>&& data);
	XQTablePagingItem& operator<<(const QString& text);
	XQTablePagingItem& operator<<(QString&& text);
	XQTablePagingItem& operator<<(double num);
	XQTablePagingItem& operator<<(int num);
	XQTablePagingItem& operator<<(uint num);
	XQTablePagingItem& operator<<(qlonglong num);
	XQTablePagingItem& operator<<(qulonglong num);
protected:
	QList<QPair<QString,QVariant>> m_data;
};
#endif // !XQTablePagingItem_H
