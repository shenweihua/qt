﻿#include"XQTablePagingWidget.h"
#include"XQComboCheckButton.h"
#include"XQAlgorithm.h"
#include"XQReturnEventFilter.hpp"
#include"XQMouseEventFilter_Wheel.hpp"
#include"XQFuncEventFilter.hpp"
#include<QCloseEvent>
#include<QLabel>
#include<QSpinBox>
#include<QComboBox>
#include<QTableWidget>
#include<QHeaderView>
#include<QBoxLayout>
#include<QWheelEvent>
#include<QTimer>
#include<QDebug>
void XQTablePagingWidget::init()
{
	installEventFilter(new XQFuncEventFilter(this));
	setCssFile(this, ":/XQTablePagingWidget/style.css");
	
	init_ui();
	tableWidget_init();
	updataTimer_init();
	//微调框变化
	connect(m_paging,&QSpinBox::valueChanged,this,&XQTablePagingWidget::showTable);
	//查询按钮按下
	connect(m_findBtn,&QPushButton::pressed,this,&XQTablePagingWidget::findSearch);
	//过滤按钮按下
	connect(m_filterBtn, &QPushButton::pressed, this, QOverload<>::of(&XQTablePagingWidget::filter));
	//搜索框加载回车搜索时间
	m_findEdit->installEventFilter(new XQReturnEventFilter([this] {emit m_findBtn->pressed(); }, m_findEdit));
	//单机逆序按钮
	connect(m_reverseBtn, &QPushButton::pressed, [=] {
		if (m_showData == nullptr)
			return;
		reverse();
		updataTable();
		});
	
}

void XQTablePagingWidget::init_ui()
{
	//设置内容布局
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, this);//从上到下
	layout->setContentsMargins(0, 0, 0, 0);
	layout->setSpacing(0);
	layout->addWidget(init_paging());
	layout->addWidget(m_tableWidget,1);
	setLayout(layout);
}

QWidget* XQTablePagingWidget::init_paging()
{
	m_tableWidget = new QTableWidget(this);
	m_count = new QLabel("数量:0");
	m_Pages = new QLabel("页数:0");
	m_paging = new  QSpinBox();
	m_paging->setRange(0, 0);

	m_filterBtn= new XQComboCheckButton("过滤");
	m_filterBtn->hide();
	m_findEdit = new QComboBox();
	//m_findEdit->setMinimumWidth(100);
	m_findEdit->setEditable(true);
	m_findBtn = new XQComboCheckButton("搜索");
	/*m_findBtn->setMinimumWidth(100);*/
	m_reverseBtn = new QPushButton("逆序");
	m_updataBox = new QComboBox(); 
	m_updataBox->setMinimumWidth(60);
	m_updataBox->addItem("停止", 0);
	for (size_t i = 0; i < 10; i++)
		m_updataBox->addItem(QString("%1秒").arg(i + 1), i + 1);
	//自动刷新
	auto updata = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	updata->addWidget(new QLabel("刷新:"));
	updata->addWidget(m_updataBox);


	//设置总内容布局
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->setContentsMargins(0, 0, 0, 0);
	//layout->addStretch(1);
	layout->addWidget(m_filterBtn);
	layout->addWidget(new QLabel("列表搜索"),0, Qt::AlignHCenter);
	layout->addWidget(m_findEdit,1);
	layout->addWidget(m_findBtn, 0);
	layout->addWidget(m_reverseBtn, 0);
	layout->addStretch(1);
	layout->addLayout(updata);
	layout->addWidget(m_count, 0, Qt::AlignCenter);
	layout->addWidget(m_paging, 0, Qt::AlignCenter);
	layout->addWidget(m_Pages, 0, Qt::AlignCenter);
	//layout->addStretch(1);
	m_paletteWidget = new QWidget();
	m_paletteWidget->setLayout(layout);

	return m_paletteWidget;
}

void XQTablePagingWidget::tableWidget_init()
{
	//设置只读
	m_tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
	//设置自定义菜单
	m_tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(m_tableWidget, &QWidget::customContextMenuRequested, this, &XQTablePagingWidget::contextMenuRequested);
	setContextMenuFunc();
	//设置自适应列宽
	m_tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Interactive);
	//设置禁用滚动条
	m_tableWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	//滚轮翻页
	m_tableWidget->installEventFilter(new XQMouseEventFilter_Wheel([=](QWheelEvent* ev) {
		// 获取鼠标滚动的步长
		int delta = ev->angleDelta().y();
		int min = m_paging->minimum();
		int max = m_paging->maximum();
		int value = m_paging->value();
		// 根据步长处理滚动事件
		if (delta > 0 && value > min) {
			// 向上滚动
			m_paging->setValue(value - 1);
		}
		else if (delta < 0 && value < max) {
			// 向下滚动
			m_paging->setValue(value + 1);
		}
		}, m_tableWidget));
	//双击标题栏排序
	connect(m_tableWidget->horizontalHeader(), &QHeaderView::sectionDoubleClicked, this, &XQTablePagingWidget::sort);
}

void XQTablePagingWidget::updataTimer_init()
{
	m_updataTimer = new QTimer(this);
	m_updataTimer->callOnTimeout(this,&XQTablePagingWidget::updataTable,Qt::QueuedConnection);
	//切换设置定时器间隔
	connect(m_updataBox, &QComboBox::currentIndexChanged, this, [=]{
		auto sec = m_updataBox->currentData().toInt();
		if (sec == 0)
			m_updataTimer->stop();
		else
			m_updataTimer->setInterval(sec*1000);
		});
}

void XQTablePagingWidget::resizeEvent(QResizeEvent* event)
{
	int row= rowNumber();
	if(m_onePageCount!= row&& m_showData!=nullptr&& row>0)
	{
		int page = m_paging->value();
		int selectRow = m_tableWidget->currentRow();
		int newPage = (page * m_onePageCount+selectRow) / row;
		init_PagingUI(m_showData->count());
		showTable();
		m_paging->setValue(newPage);
	}
}

void XQTablePagingWidget::closeEvent(QCloseEvent* event)
{
	if (!m_tableWidget->horizontalHeader()->isEnabled())
		event->ignore();
}

void XQTablePagingWidget::showEvent(QShowEvent*)
{
	//init_PagingUI(m_data.count());
	//updataTable();
}

void XQTablePagingWidget::contextMenuRequested()
{
	if(m_contextMenuFunc!=nullptr)
		m_contextMenuFunc();
}
