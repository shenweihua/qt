﻿#include"XQLogWidget.h"
XQLogWidget::XQLogWidget(const QString& title, QWidget* parent)
	:XQTablePagingWidget(parent, false)
{
	init();
	setWindowTitle(title);
}

XQLogWidget::XQLogWidget(QWidget* parent)
	:XQTablePagingWidget(parent,false)
{
	init();
}

XQLogWidget::~XQLogWidget()
{
}
qint64 XQLogWidget::maxSize() const
{
	return m_maxSize;
}
void XQLogWidget::addLogInfo(const XQLogDate& data)
{
	XQTablePagingItem item;
	item.push_back(data.key);
	item.push_back(XQLog::logType_toString(data.logType),int(data.logType));
	item.push_back(data.time.toString("yyyy-MM-dd hh:mm:ss"), data.time);
	item.push_back(data.info);
	push_frontRow(std::move(item));
	//showDataAll();
}

void XQLogWidget::setMaxSize(int size)
{
	m_maxSize = size;
}
void XQLogWidget::addLog(const QString& name)
{
	//创建绑定日志
	auto log = XQLog::Create(name);
	if (m_log.find(log) != m_log.end())
		return;
	connect(log, &XQLog::LogOut, this, &XQLogWidget::addLogInfo);
	m_log << log;
}

void XQLogWidget::addLog(const XQLog& log)
{
	addLog(log.key());
}

void XQLogWidget::removeLog(const QString& name,bool deleteLater)
{
	auto log = XQLog::Global(name);
	if (m_log.find(log) == m_log.end())
		return;
	disconnect(log, &XQLog::LogOut, this, &XQLogWidget::addLogInfo);
	m_log.remove(log);
	if (deleteLater)
		XQLog::remove(name);
}

void XQLogWidget::removeLog(const XQLog& log, bool deleteLater)
{
	removeLog(log.key(), deleteLater);
}

void XQLogWidget::clearLog(bool deleteLater)
{
	for (auto&log:m_log)
	{
		removeLog(log->key(),deleteLater);
	}
}
