﻿#ifndef XQTABLEEXPORTWIDGET_H
#define XQTABLEEXPORTWIDGET_H
#include<QScrollArea>
#include"XQHead.h"
#include"XQTablePagingItem.h"
//表格导出到本地的小部件
class XQTableExportWidget:public QScrollArea
{
	Q_OBJECT
public:
	XQTableExportWidget(QWidget* parent = nullptr);
	~XQTableExportWidget();
	QTableView* tableView()const;
	//获取水平标题
	QStringList HorizontalTitle()const;
public:
	//设置表格视图
	void setTableView(QTableView* tableView);
	//设置要导出的数据
	void setExportData(QList<XQTablePagingItem>* data);
	//开始
	void start();
	//结束
	void stop();
	//导出到Excel(多线程)
	void exportExcel(XQRunnable* Runnable);
signals://信号
	//状态栏显示提示信息
	void showMessage(const QString& text, int timeout = 0);
protected://初始化ui
	void init();
	void init_ui();
	void init_statusbar();//状态栏初始化
	void init_taskPool();//任务池初始化
	QBoxLayout* init_oneRow();//第一行的主要功能初始化
protected:
	QTableView* m_tableView = nullptr;//绑定的表格
	QList<XQTablePagingItem>* m_data=nullptr;//要导出的数据
	XQStatusBarWidget* m_statusbar = nullptr;//状态栏
	XQTaskPool* m_taskPool = nullptr;//任务池
	QCheckBox* m_ExportTitle = nullptr;//是否要导出标题栏
	QLineEdit* m_filePathEdit = nullptr;//保存的地址;
	QPushButton* m_startBtn = nullptr;//开始按钮
	QPushButton* m_stopBtn = nullptr;//结束按钮
};
#endif // !XQTableExportWidget_H
