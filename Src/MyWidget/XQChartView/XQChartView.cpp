﻿#include"XQChartView.h"
#include"XQLog.hpp"
#include<QMouseEvent>
#include<QMenu>
#include<QChart>
#include<QToolTip>
#include<QValueAxis>
#include<QXYSeries>
XQChartView::XQChartView(QWidget* parent)
	:QChartView(parent)
{
	
}

XQChartView::~XQChartView()
{
}

void XQChartView::init()
{
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QWidget::customContextMenuRequested, this, &XQChartView::contextMenuRequested);
	setMouseTracking(true);
	setRenderHint(QPainter::RenderHint::Antialiasing);//设置抗锯齿
	//设置内容布局
	auto chart = this->chart();
	chart->setTitleFont(QFont("黑体", 20));//视图标题
	chart->setDropShadowEnabled(true);//设置阴影
	//chart->setTheme(QChart::ChartTheme::ChartThemeLight);//主题
	//chart->setAnimationOptions(QChart::AnimationOption::AllAnimations);
	chart->setAnimationDuration(3000);//动画时间

	// 设置缩放的锚点
	setTransformationAnchor(QChartView::AnchorUnderMouse);
	setResizeAnchor(QChartView::AnchorUnderMouse);
}

void XQChartView::mousePressEvent(QMouseEvent* ev)
{
	if (ev->button() == Qt::LeftButton) {
		m_lastMousePos = ev->pos();
		m_isDragging = true;
	}

	QChartView::mousePressEvent(ev);
}

void XQChartView::mouseReleaseEvent(QMouseEvent* ev)
{
	if (ev->button() == Qt::LeftButton) {
		m_isDragging = false;
	}

	QChartView::mouseReleaseEvent(ev);
}

void XQChartView::mouseMoveEvent(QMouseEvent* ev)
{
	if (m_isDragging&& chart()->isZoomed()) {
		QPoint delta = ev->pos() - m_lastMousePos;
		chart()->scroll(-delta.x(), delta.y());

		m_lastMousePos = ev->pos();
	}

	QChartView::mouseMoveEvent(ev);
}

void XQChartView::wheelEvent(QWheelEvent* ev)
{
	qreal factor = 1.2; // 缩放因子

	if (ev->angleDelta().y() > 0) {
		// 向上滚动，放大
		chart()->zoom(factor);
	}
	else {
		// 向下滚动，缩小
		chart()->zoom(1 / factor);
	}

	ev->accept();
}

void XQChartView::contextMenuRequested()
{
	auto chart = this->chart();
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	if (this->chart()->isZoomed())
	{//是否缩放了
		menu->addAction("缩放还原", [=] {this->chart()->zoomReset(); });
		menu->addSeparator();
	}
	//主题
	QMenu* theme = new QMenu("主题", menu);
	{
		QStringList list = { "light","天蓝色","黑色","沙棕色","自然色","高对比度","冰蓝色","Qt" };
		for (size_t i = 0; i < list.size(); i++)
		{
			auto Action = theme->addAction(list[i], [=] {chart->setTheme(QChart::ChartTheme(i)); });
			if (chart->theme() == i)
			{
				Action->setCheckable(true);
				Action->setChecked(true);
			}
		}
		menu->addMenu(theme);
	}

	//动画
	QMenu* animation = new QMenu("动画", menu);
	menu->addMenu(animation);
	{
		QStringList list = { "禁用动画","启动网格动画","启用系列动画","启用所有动画" };
		for (size_t i = 0; i < list.size(); i++)
		{
			auto Action = animation->addAction(list[i], [=] {chart->setAnimationOptions(QChart::AnimationOption(i)); });
			if (chart->animationOptions() == i)
			{
				Action->setCheckable(true);
				Action->setChecked(true);
			}
		}
	}
	//阴影
	{
		if (chart->isDropShadowEnabled() == false)
			menu->addAction("开启阴影", [=] {chart->setDropShadowEnabled(true); });
		else
			menu->addAction("关闭阴影", [=] {chart->setDropShadowEnabled(false); });
	}
	menu->popup(QCursor::pos());
}

