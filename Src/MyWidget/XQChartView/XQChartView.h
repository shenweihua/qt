﻿#ifndef XQCHARTVIEW_H
#define XQCHARTVIEW_H
#include"XQHead.h"
#include<QChartView>
class XQChartView:public QChartView
{
	Q_OBJECT
public:
	XQChartView(QWidget* parent = nullptr);
	~XQChartView();
protected:
	virtual void init();
protected:
	virtual void mousePressEvent(QMouseEvent* ev);
	virtual void mouseReleaseEvent(QMouseEvent* ev);
	virtual void mouseMoveEvent(QMouseEvent* ev);
	virtual void wheelEvent(QWheelEvent* ev) ;
	//右键菜单
	virtual void contextMenuRequested();
protected:
	QPoint m_lastMousePos;
	bool m_isDragging = false;
};
#endif // !XQChartView_H
