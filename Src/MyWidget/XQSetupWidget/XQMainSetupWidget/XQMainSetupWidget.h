﻿#ifndef XQMAINSETUPWIDGET_H
#define XQMAINSETUPWIDGET_H
#include<QWidget>
#include"XQHead.h"
#include"XQSettingsInfo.h"
//系统设置主窗口小部件
class XQMainSetupWidget :public QWidget
{
	Q_OBJECT
public:
	XQMainSetupWidget(QWidget* parent = nullptr, bool AutoInit = true);
	virtual ~XQMainSetupWidget();
public:
	enum SetupWidgetType
	{
		System,//系统基本设置界面
		Robot,//机器人界面
		Timer//定时器任务界面
	};
public:
	//获取配置信息
	XQSettingsInfo* settingsInfo()const;
public :
	//添加一个窗口
	XQLabelButton* addWidget(QWidget* widget,const QString& title);
	QWidget* addWidget(SetupWidgetType type);
	void clearWidget();
	void setCurrentWidget(SetupWidgetType type);
signals://信号

protected://隐藏的函数
	//初始化
	virtual void init();
protected://初始化ui
	virtual void init_ui();
	//初始化堆栈里面的小部件
	virtual void init_StackedWidget();
protected://事件
	/*void showEvent(QShowEvent* event)override;*/
	void closeEvent(QCloseEvent* event)override;
protected://变量
	XQSystemSetupWidget* m_SystemSetup = nullptr;
	XQRobotSetupWidget* m_RobotSetup = nullptr;
	XQTimerTaskSetupWidget* m_TimerTaskSetup = nullptr;
	XQStackedWidget* m_StackedWidget = nullptr;//堆栈小部件
};
#endif // !XQMainSetupWidget
