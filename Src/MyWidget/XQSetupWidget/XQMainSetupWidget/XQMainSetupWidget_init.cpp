﻿#include"XQMainSetupWidget.h"
#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include"XQLabelButton.h"
#include"XQAppInfo.h"
#include"XQSystemSetupWidget.h"
#include"XQTimerTaskSetupWidget.h"
#include"XQTitleBarWidget.h"
#include<QCloseEvent>
#include<QBoxLayout>
#include<QIcon>
#include<QDebug>
void XQMainSetupWidget::init()
{
	init_ui();
	init_StackedWidget();
	m_TimerTaskSetup->init_UiData();
	/*setWindowIcon(QIcon());*/
}

void XQMainSetupWidget::init_ui()
{
	XQTitleBarWidget* title = new XQTitleBarWidget(this);
	title->setButtons(15);
	title->setStackedWidget(XQAppInfo::Global()->stackedWidget());
	m_StackedWidget = new XQStackedWidget(QBoxLayout::LeftToRight, QBoxLayout::TopToBottom, this);
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom);//从左到右
	layout->setContentsMargins(0, 0, 0, 0);
	layout->addWidget(title);
	layout->addWidget(m_StackedWidget);
	setLayout(layout);
}

void XQMainSetupWidget::init_StackedWidget()
{
	QWidget* widget = nullptr;
	for (int i = 0; i < 3; i++)
	{
		widget = addWidget(SetupWidgetType(i));
		widget->setWindowIcon(QIcon());
		{//按钮设置
			auto btn = m_StackedWidget->labelButton(widget);
			btn->setCloseButtonShow(false);
			btn->setShowBubble(false);
			btn->setTextAlignment(Qt::AlignCenter);
		}
	}
	m_StackedWidget->setCurrentIndex(System);
	{//标题栏设置
		auto title = m_StackedWidget->titleWidget();
		title->setButtonCenter(true);
		title->setShowButtonCount(false);
	}
}

void XQMainSetupWidget::closeEvent(QCloseEvent* event)
{
	if (m_StackedWidget != nullptr)
	{
		m_StackedWidget->clearWidget();//清理下窗口
		if (m_StackedWidget->count() != 0)
		{
			event->ignore();
		}
		else
		{
			deleteLater();
		}
	}
}
