﻿#ifndef XQTIMERTASKSETUPWIDGET_H
#define XQTIMERTASKSETUPWIDGET_H
#include"XQHead.h"
#include"XQTimerGroup.h"
#include<QScrollArea>
#include<QMap>
#define TimerTask_KEY "XQTimerTaskWidget"
//定时器任务设置小部件
class XQTimerTaskSetupWidget :public QScrollArea
{
	Q_OBJECT
public:
	XQTimerTaskSetupWidget(QWidget* parent = nullptr, bool AutoInit = true);
	virtual ~XQTimerTaskSetupWidget();
public:
	//获取配置信息
	XQSettingsInfo* settingsInfo()const;
	XQTimerGroup* timerGroup()const;
	XQTimerTaskWidget* taskWidget(int id);
public:
	//添加一个内置的预设任务
	void addTaskRoot(const QString& name , std::function<void()> func);
	void addTaskWidget(XQTimerTaskWidget* widget);
	void removeTaskWidget(XQTimerTaskWidget* widget);
	void clearTaskWidget();
	//初始化ui数据
	virtual void init_UiData();
protected://隐藏的函数
	//初始化
	virtual void init();
	//绑定函数
	virtual void bindFunc();
	//显示菜单
	void showMainContextMenu();
	void showTaskContextMenu();
protected://槽函数
	//add按钮触发添加一个
	virtual void addTask();
	//remove按钮触发删除一个
	virtual void removeTask();
	virtual void clearTask();
	//设置显示的数量文本
	void setShowCountText();
	//定时器组状态检测
	void timerState(TimerPollState state, int id = -1);
	void setInfoText(const QString& text);
protected://初始化ui
	virtual void init_ui();
	virtual QLayout* init_OneRow();//第一行
protected://事件
	//void showEvent(QShowEvent* event)override;
	void closeEvent(QCloseEvent* event)override;
protected://变量
	XQTimerTaskWidget* m_selectTask = nullptr;//当前选中的
	QList<XQTimerTaskWidget*> m_taskList;//任务组
	QBoxLayout* m_layout = nullptr;
	QPushButton* m_addBtn = nullptr;//添加
	QPushButton* m_removeBtn = nullptr;//删除
	QPushButton* m_clearBtn = nullptr;//清空
	QLabel* m_info = nullptr;//信息
	QLabel* m_currentTime = nullptr;//现在时间
	XQCircleLabel* m_taskCount = nullptr;//当前数量
	QMap<QString, std::function<void()>> m_taskGroup_root;//任务组
	//QMap<QString, int> m_taskId;//加入定时器的任务
};
#endif