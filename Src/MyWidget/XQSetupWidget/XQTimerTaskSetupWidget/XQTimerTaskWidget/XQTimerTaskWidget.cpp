﻿#include"XQTimerTaskWidget.h"
#include"XQComboCheckButton.h"
#include"XQAppInfo.h"
#include"XQTimerGroup.h"
#include<QComboBox>
#include<QStackedLayout>
#include<QBoxLayout>
#include<QCheckBox>
#include<QLabel>
#include<QTimeEdit>
#include<QLineEdit>
#include<QDebug>
XQTimerTaskWidget::XQTimerTaskWidget(QWidget* parent, bool Auto)
	:QWidget(parent)
{
	if(Auto)
		init();
}

XQTimerTaskWidget::~XQTimerTaskWidget()
{
	/*qInfo() << "释放内存";*/
	if (m_enableCheck != nullptr)
		m_enableCheck->deleteLater();
	if (m_taskName != nullptr)
		m_taskName->deleteLater();
	if (m_startDateTime != nullptr)
		m_startDateTime->deleteLater();
	if (m_countEdit != nullptr)
		 m_countEdit->deleteLater();
	if (m_timerTypeComBox != nullptr)
		m_timerTypeComBox->deleteLater();
	if (m_timerStackLayout != nullptr)
		m_timerStackLayout->deleteLater();
	if (m_timerEdit_time != nullptr)
		m_timerEdit_time->deleteLater();
	if (m_timeTypeComBox_date != nullptr)
		 m_timeTypeComBox_date->deleteLater();
	if (m_daySelectComBox_date != nullptr)
		m_daySelectComBox_date->deleteLater();
	if (m_timerEdit_date != nullptr)
		m_timerEdit_date->deleteLater();
	if (m_taskTypeComBox != nullptr)
		m_taskTypeComBox->deleteLater();
	if (m_taskStackLayout != nullptr)
		m_taskStackLayout->deleteLater();
	if (m_taskRootComBox != nullptr)
		m_taskRootComBox->deleteLater();
	if (m_taskUserEdit != nullptr)
		m_taskUserEdit->deleteLater();
	if (m_robot != nullptr)
		m_robot->deleteLater();
}
XQTimerGroup* XQTimerTaskWidget::timerGroup() const
{
	return XQAppInfo::timerGroup();
}
int XQTimerTaskWidget::timerId() const
{
	return m_timerId;
}
bool XQTimerTaskWidget::isTaskEnable() const
{
	return m_enableCheck->isChecked();
}
QString XQTimerTaskWidget::taskName() const
{
	return m_taskName->text();
}
QDateTime XQTimerTaskWidget::startDateTime() const
{
	return m_startDateTime->dateTime();
}
int XQTimerTaskWidget::runCount() const
{
	if (m_countEdit->text().isEmpty())
		return -1;
	return m_countEdit->text().toInt();
}
TimerType XQTimerTaskWidget::timerType() const
{
	return TimerType(m_timerTypeComBox->currentData().toInt());
}
QTime XQTimerTaskWidget::timerTime_time() const
{
	return m_timerEdit_time->time();
}
TimeType XQTimerTaskWidget::timeType() const
{
	return TimeType(m_timeTypeComBox_date->currentData().toInt());
}
QList<int> XQTimerTaskWidget::daySelect() const
{
	return std::move(m_daySelectComBox_date->selectIndexs());
}
QTime XQTimerTaskWidget::timerTime_date() const
{
	return m_timerEdit_date->time();
}
TaskType XQTimerTaskWidget::taskType() const
{
	return TaskType(m_taskTypeComBox->currentData().toInt());
}
int XQTimerTaskWidget::taskRoot() const
{
	return m_taskRootComBox->currentIndex();
}
QString XQTimerTaskWidget::taskUser() const
{
	return m_taskUserEdit->text();
}
QList<int> XQTimerTaskWidget::robotSelect() const
{
	return std::move(m_robot->selectIndexs());
}
void XQTimerTaskWidget::setTimerId(int id)
{
	m_timerId = id;
}
void XQTimerTaskWidget::addTaskRoot(const QString& name, QMap<QString, std::function<void()>>* taskGroup)
{
	m_taskRootComBox->addItem(name, QVariant::fromValue< QMap<QString, std::function<void()>>*>(taskGroup));
}

void XQTimerTaskWidget::setBackgroundBorderColour(QRgb rgb)
{
	QString style = QString(".QWidget#background {"
		"border: 2px solid rgb(%1,%2,%3);"
		"}").arg(qRed(rgb)).arg(qGreen(rgb)).arg(qBlue(rgb)) ;
	m_background->setStyleSheet(style);
}

void XQTimerTaskWidget::setBackgroundBorderColour()
{
	m_background->setStyleSheet("");
}
void XQTimerTaskWidget::setTaskEnable(bool enable)
{
	auto timer=XQAppInfo::timerGroup();
	
	if (enable)
	{//启用
		if (m_timerId != -1)
		{
			if (!timer->task(m_timerId) )
			{//不存在
				if(!joinTimer(m_timerId))
				{
					m_enableCheck->setChecked(false);
					return;
				}
			}
		}
		else if (!joinTimer(m_timerId))
		{
			m_enableCheck->setChecked(false);
			return;
		}	
	}
	else if(!enable&&m_timerId!=-1)
	{//禁用的时候从定时器中删除任务
		timer->removeTask(m_timerId);
		m_timerId = -1;
		disconnect(timerGroup(), &XQTimerGroup::runTimeUpData,this,&XQTimerTaskWidget::nextRunTime);
		m_timerInfo->clear();
	}
	m_enableCheck->setChecked(enable);
	//更新下显示的运行时间
	//qInfo() << timerGroup()->task(timerId()) << timerId();
	if(enable)
	{
		nextRunTime(timerId(), timerGroup()->task(timerId())->runTime());
		connect(timerGroup(), &XQTimerGroup::runTimeUpData, this, &XQTimerTaskWidget::nextRunTime, Qt::QueuedConnection);
	}
}

void XQTimerTaskWidget::setTaskName(const QString& name)
{
	m_taskName->setText(name);
}

void XQTimerTaskWidget::setStartDateTime(const QDateTime& dateTime)
{
	m_startDateTime->setDateTime(dateTime);
}

void XQTimerTaskWidget::setRunCount(int count)
{
	if(count==-1)
		m_countEdit->setText("");
	else
		m_countEdit->setText(QString::number(count));
}

void XQTimerTaskWidget::setTimerType(TimerType type)
{
	m_timerTypeComBox->setCurrentIndex(int(type));
}

void XQTimerTaskWidget::setTimerTime_time(const QTime& time)
{
	m_timerEdit_time->setTime(time);
}

void XQTimerTaskWidget::setTimeType(TimeType type)
{
	m_timeTypeComBox_date->setCurrentIndex(int(type));
}

void XQTimerTaskWidget::setDaySelect(const QList<int>& select)
{
	/*qInfo() << m_daySelectComBox_date->textAll();
	qInfo() << select;*/
	m_daySelectComBox_date->setSelectIndexs(select);
}

void XQTimerTaskWidget::setTimerTime_date(const QTime& time)
{
	m_timerEdit_date->setTime(time);
}

void XQTimerTaskWidget::setTaskType(TaskType type)
{
	m_taskTypeComBox->setCurrentIndex(int(type));
}

void XQTimerTaskWidget::setTaskRoot(int index)
{
	m_taskRootComBox->setCurrentIndex(index);
}

void XQTimerTaskWidget::setTaskUser(const QString& user)
{
	m_taskUserEdit->setText(user);
}

void XQTimerTaskWidget::setRobotSelect(const QList<int>& select)
{
	m_robot->setSelectIndexs(select);
}
