﻿#include"XQTimerTaskSetupWidget.h"
#include"XQTimerTaskWidget.h"
#include"XQAlgorithm.h"
#include"XQSettingsInfo.h"
#include"XQAppInfo.h"
#include"XQCircleLabel.h"
#include<QBoxLayout>
#include<QGridLayout>
#include<QMenu>
#include<QPushButton>
#include<QLabel>
#include<QSettings>
#include<QMessageBox>
#include<QProgressBar>
#include<QTimer>
void XQTimerTaskSetupWidget::init()
{
	init_ui();
	bindFunc();
	if (m_taskList.isEmpty())
		m_clearBtn->setEnabled(false);
	/*init_UiData();*/

}

void XQTimerTaskSetupWidget::bindFunc()
{
	setCssFile(this, ":/XQTimerTaskSetupWidget/style.css");
	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QWidget::customContextMenuRequested, this, &XQTimerTaskSetupWidget::showMainContextMenu);
	connect(timerGroup(), &XQTimerGroup::pollState, this, &XQTimerTaskSetupWidget::timerState, Qt::QueuedConnection);

	//绑定函数 XQTimerTaskSetupWidget::init_OneRow()
	connect(m_addBtn, &QPushButton::pressed, this, &XQTimerTaskSetupWidget::addTask);
	connect(m_removeBtn, &QPushButton::pressed, this, &XQTimerTaskSetupWidget::removeTask);
	connect(m_clearBtn, &QPushButton::pressed, this, &XQTimerTaskSetupWidget::clearTask);
	auto timer = new QTimer(this);
	timer->callOnTimeout([this] {m_currentTime->setText(QDateTime::currentDateTime().toString("yyyy年MM月dd日 hh时mm分ss秒")); });
	timer->start(1000);
}

void XQTimerTaskSetupWidget::init_UiData()
{
	auto settings = settingsInfo()->Settings();
	int count = settings->beginReadArray(TimerTask_KEY);
	for (size_t i = 0; i < count; i++)
	{
		settings->setArrayIndex(i);
		auto widget =new XQTimerTaskWidget(this);
		addTaskWidget(widget);
		widget->setTimerId(settings->value("timerId").toInt());
		widget->setTaskName(settings->value("taskName").toString());
		widget->setStartDateTime(settings->value("startDateTime").toDateTime());
		widget->setRunCount(settings->value("runCount").toInt());
		int type= settings->value("timerType").toInt();
		widget->setTimerType(TimerType(settings->value("timerType").toInt()));
		//XQAppInfo::Global()->processEvents();
		widget->setTimerTime_time(settings->value("timerTime_time").toTime());
		widget->setTimeType(TimeType(settings->value("timeType").toInt()));
		//XQAppInfo::Global()->processEvents();
		{
			QString daySelect = settings->value("daySelect").toString();
			if(!daySelect.isEmpty())
			{
				QList<int> list;
				for (auto& nSel : daySelect.split(" "))
				{
					list << nSel.toInt();
				}
				widget->setDaySelect(list);
			}
		}
		widget->setTimerTime_date(settings->value("timerTime_date").toTime());
		widget->setTaskType(TaskType(settings->value("taskType").toInt()));
		//XQAppInfo::Global()->processEvents();
		widget->setTaskRoot(settings->value("taskRoot").toInt());
		widget->setTaskUser(settings->value("taskUser").toString());
		//XQAppInfo::Global()->processEvents();
		{
			QString robotSelect = settings->value("robotSelect").toString();
			if(!robotSelect.isEmpty())
			{
				QList<int> list;
				for (auto& nSel : robotSelect.split(" "))
				{
					list << nSel.toInt();
				}
				widget->setRobotSelect(list);
			}
		}
		widget->setTaskEnable(settings->value("enable").toBool());
	}
	settings->endArray();
}

void XQTimerTaskSetupWidget::showMainContextMenu()
{
	auto menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	menu->addAction("添加任务",this, &XQTimerTaskSetupWidget::addTask);
	menu->addAction("清空任务",this,&XQTimerTaskSetupWidget::clearTaskWidget);
	menu->popup(QCursor::pos());
}

void XQTimerTaskSetupWidget::showTaskContextMenu()
{
	auto menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	menu->addAction("删除", this, &XQTimerTaskSetupWidget::removeTask);
	menu->popup(QCursor::pos());
}

void XQTimerTaskSetupWidget::init_ui()
{

	auto widget = new QWidget(this);
	m_layout = new QBoxLayout(QBoxLayout::TopToBottom, widget);//
	m_layout->setContentsMargins(0, 0, 0, 0);
	m_layout->addLayout(init_OneRow());
	/*layout->addWidget(new  XQTimerTaskWidget(widget));*/
	m_layout->addStretch(1);
	widget->setLayout(m_layout);
	//new  XQTimerTaskWidget(widget);
	//设置滚动条
	setWidget(widget);
	setWidgetResizable(true);
}

QLayout* XQTimerTaskSetupWidget::init_OneRow()
{
	m_addBtn = new QPushButton("添加任务");
	m_removeBtn= new QPushButton("删除任务");
	m_clearBtn= new QPushButton("清空任务");
	m_taskCount = new XQCircleLabel("0");
	m_info=new QLabel();
	m_currentTime = new QLabel();
	auto Grid=new QGridLayout;
	{
		Grid->addWidget(m_addBtn, 0,0);
		Grid->addWidget(m_removeBtn, 0,1);
		Grid->addWidget(m_clearBtn, 0,2);
		Grid->addWidget(m_info, 1, 0,1,2);
		Grid->addWidget(m_currentTime, 1, 2);
	}

	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//
	layout->setContentsMargins(1, 1, 1, 1);
	layout->addLayout(Grid,1);
	layout->addWidget(m_taskCount);
	//参数设置
	m_addBtn->setObjectName("add");
	m_removeBtn->setObjectName("remove");
	m_clearBtn->setObjectName("clear");
	m_taskCount->setMinimumWidth(40);
	m_taskCount->setLineWidth(3);
	m_taskCount->setCircleHz(0);
	m_taskCount->setAlignment(Qt::AlignCenter);
	m_currentTime->setAlignment(Qt::AlignCenter);
	
	return layout;
}

void XQTimerTaskSetupWidget::closeEvent(QCloseEvent* event)
{
	//保存数据到配置文件
	auto settings = settingsInfo()->Settings();
	settings->remove(TimerTask_KEY);
	settings->beginWriteArray(TimerTask_KEY);
	int count = m_layout->count();
	for (size_t i = 1; i < count -1; i++)
	{
		auto widget=(XQTimerTaskWidget*)m_layout->itemAt(i)->widget();
		settings->setArrayIndex(i - 1);
		settings->setValue("timerId", widget->timerId());
		settings->setValue("enable", widget->isTaskEnable());
		settings->setValue("taskName", widget->taskName());
		settings->setValue("startDateTime", widget->startDateTime());
		settings->setValue("runCount", widget->runCount());
		settings->setValue("timerType", int(widget->timerType()));
		settings->setValue("timerTime_time", widget->timerTime_time());
		settings->setValue("timeType", int(widget->timeType()));
		QString daySelect;
		for (auto nSel: widget->daySelect())
		{
			daySelect += QString("%1 ").arg(nSel);
		}
		settings->setValue("daySelect", daySelect.trimmed());
		settings->setValue("timerTime_date", widget->timerTime_date());
		settings->setValue("taskType", int(widget->taskType()));
		settings->setValue("taskRoot", widget->taskRoot());
		settings->setValue("taskUser", widget->taskUser());
		QString robotSelect;
		for (auto nSel : widget->robotSelect())
		{
			robotSelect += QString("%1 ").arg(nSel);
		}
		settings->setValue("robotSelect", robotSelect.trimmed());
	}
	settings->endArray();
	settings->sync();
	/*qInfo() << "关闭";*/
}
