﻿#include"XQSystemSetupWidget.h"
#include"XQSettingsInfo.h"
#include"XQAppInfo.h"
#include"XQArguments.h"
#include<QSettings>
#include<QApplication>
#include<QCheckBox>
#include<QLineEdit>
#include<QFileDialog>
#include<QLabel>
XQSystemSetupWidget::XQSystemSetupWidget(QWidget* parent, bool AutoInit)
	:QScrollArea(parent)
{
	if (AutoInit)
	{
		init();
	}
}

XQSystemSetupWidget::~XQSystemSetupWidget()
{
}

int XQSystemSetupWidget::leftLabelWidth() const
{
	return m_leftLabelWidth;
}

XQSettingsInfo* XQSystemSetupWidget::settingsInfo() const
{
	return XQAppInfo::settingsInfo();
}

bool XQSystemSetupWidget::isAutoStartEnabled() const
{
#ifdef WIN32
	// 读取注册表中的开机自启设置
	QSettings settings("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", QSettings::NativeFormat);
	return settings.contains(QApplication::applicationName());
#endif // WIN32
}

void XQSystemSetupWidget::setLeftLabelWidth(int width)
{
	m_leftLabelWidth = width;
}
void XQSystemSetupWidget::setBufferPath()
{
	auto dir = QFileDialog::getExistingDirectory(this, "选择缓存目录");
	if (dir.isEmpty())
		return;
	m_bufferPath->setText(dir);
	if (settingsInfo() != nullptr)settingsInfo()->setBufferPath(dir);
	emit bufferPathChange(dir);
}

void XQSystemSetupWidget::setAutorun(bool Auto)
{
	XQArguments::setAutoStartType(AutoStartType(settingsInfo()->autoStartType()));
#ifdef WIN32
	// 如果用户勾选了“开机自启”
	if (Auto) {
		QSettings reg("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
			QSettings::NativeFormat);
		QString appName = QApplication::applicationName();
		QString appPath = QApplication::applicationFilePath() + XQArguments::toArgs();
		reg.setValue(appName, appPath.replace('/', '\\'));
	}
	// 如果用户取消勾选“开机自启”
	else {
		QSettings reg("HKEY_CURRENT_USER\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
			QSettings::NativeFormat);
		QString appName = QApplication::applicationName();
		reg.remove(appName);
	}
#endif // WIN32
}