﻿#include"XQSystemSetupWidget.h"
#include"XQMouseEventFilter_Press.hpp"
#include"XQSettingsInfo.h"
#include"XQStackedWidget.h"
#include"XQAppInfo.h"
#include"XQSystemTray.h"
#include<QButtonGroup>
#include<QLineEdit>
#include<QCheckBox>
#include<QFileDialog>
#include<QSettings>
#include<QApplication>
#include<QDebug>
void XQSystemSetupWidget::init()
{
	init_ui();
	bindFunc();
	init_UiData();
}

void XQSystemSetupWidget::bindFunc()
{
	//选中开机自启
	if(m_autoStart!=nullptr)
		connect(m_autoStart, &QCheckBox::stateChanged, this, &XQSystemSetupWidget::autorunChange);
	connect(this, &XQSystemSetupWidget::autorunChange, [this](bool Auto) {if (settingsInfo() != nullptr)settingsInfo()->setAutorun(Auto); if (isVisible())setAutorun(Auto); });
	//自启启动类型
	if (m_autorunGroup != nullptr)
		connect(m_autorunGroup, &QButtonGroup::idClicked, [=](int id) {autorunStartTypeChange(AutoStartType(id)); if(m_autoStart)setAutorun(true); });
	connect(this, &XQSystemSetupWidget::autorunStartTypeChange, [this](AutoStartType type) {if (settingsInfo() != nullptr)settingsInfo()->setAutoStartType(type); });
	//托盘菜单
	if(m_systemTray !=nullptr)
		connect(m_systemTray, &QCheckBox::stateChanged, this, &XQSystemSetupWidget::systemTrayChange);
	connect(this, &XQSystemSetupWidget::systemTrayChange, [this](bool show) {
		if (settingsInfo() != nullptr)
			settingsInfo()->setShowSystemTray(show); 
		if(XQAppInfo::Global())
			XQAppInfo::Global()->systemTray()->setVisible(show); });
	//选中显示悬浮窗
	if (m_suspension != nullptr)
		connect(m_suspension, &QCheckBox::stateChanged, this, &XQSystemSetupWidget::suspensionChange);
	connect(this, &XQSystemSetupWidget::suspensionChange, [this](bool yes) {if (settingsInfo() != nullptr)settingsInfo()->setOpenSuspension(yes); });
	//记住？主窗口位置
	if(m_saveMainWindowPosition!=nullptr)
		connect(m_saveMainWindowPosition, &QCheckBox::stateChanged, this, &XQSystemSetupWidget::saveMainWindowPositionChange);
	connect(this, &XQSystemSetupWidget::saveMainWindowPositionChange, [this](bool yes) {if (settingsInfo() != nullptr)settingsInfo()->setSaveMainWindowPosition(yes); });
	//鼠标单机缓存编辑框
	if(m_bufferPath!=nullptr)
		m_bufferPath->installEventFilter(new XQMouseEventFilter_Press([this] {this->setBufferPath(); }, m_bufferPath));
}

void XQSystemSetupWidget::init_UiData()
{
	//从配置文件读取信息初始化界面
	if(m_autoStart)
		m_autoStart->setChecked(isAutoStartEnabled());
	if (m_autorunGroup)
		m_autorunGroup->button(settingsInfo()->autoStartType())->setChecked(true);
	if (m_systemTray)
		m_systemTray->setChecked(settingsInfo()->isShowSystemTray());
	if (m_suspension)
		m_suspension->setChecked(settingsInfo()->openSuspension());
	if (m_saveMainWindowPosition)
		m_saveMainWindowPosition->setChecked(settingsInfo()->saveMainWindowPosition());
	if (m_bufferPath)
		m_bufferPath->setText(settingsInfo()->bufferPath());
}


void XQSystemSetupWidget::closeEvent(QCloseEvent* event)
{
	//if(m_StackedWidget!=nullptr)
	//{
	//	m_StackedWidget->clearWidget();//清理下窗口
	//	if (m_StackedWidget->count() != 0)
	//		event->ignore();
	//}
}