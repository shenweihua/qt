﻿#include"XQRobotSetupWidget.h"
#include"XQSettingsInfo.h"
#include"XQReturnEventFilter.hpp"
#include<QBoxLayout>
#include<QGridLayout>
#include<QStackedLayout>
#include<QComboBox>
#include<QCheckBox>
#include<QLineEdit>
#include<QLabel>
#include<QPushButton>
#include<QDebug>
void XQRobotSetupWidget::init()
{
	init_ui();
	bindFunc();
	init_UiData();
}
void XQRobotSetupWidget::bindFunc()
{
	if (m_Enabled)
		connect(m_Enabled, &QCheckBox::stateChanged, [=](int state) {emit robotEnabledChange(RobotType(m_RobotBox->currentData().toInt()), state); });
	connect(this,&XQRobotSetupWidget::robotEnabledChange, this, &XQRobotSetupWidget::writeRobotEnabled);
	if (m_tokenDingDing)
		connect(m_tokenDingDing, &QLineEdit::textChanged, this, &XQRobotSetupWidget::tokenDingDingChange);
	connect( this, &XQRobotSetupWidget::tokenDingDingChange,this,&XQRobotSetupWidget::writeTokenDingDing);
	if (m_secretDingDing)
		connect(m_secretDingDing, &QLineEdit::textChanged, this, &XQRobotSetupWidget::secretDingDingChange);
	connect(this, &XQRobotSetupWidget::secretDingDingChange, this, &XQRobotSetupWidget::writeSecretDingDing);
}

void XQRobotSetupWidget::init_UiData()
{
	//从配置文件读取信息初始化界面
	if (m_Enabled)
		readRobotEnabled();
	if (m_tokenDingDing)
		readTokenDingDing();
	if (m_secretDingDing)
		readSecretDingDing();
}

void XQRobotSetupWidget::init_ui()
{
	init_StackLayout();
	
	auto widget = new QWidget(this);
	auto layout = new QBoxLayout(QBoxLayout::TopToBottom, widget);//从上到下
	layout->setContentsMargins(10, 10, 10, 10);
	layout->addLayout(init_OneRow());
	layout->addLayout(m_StackLayout);
	layout->addWidget(init_test());
	layout->addStretch(1);
	widget->setLayout(layout);

	//设置滚动条
	setWidget(widget);
	setWidgetResizable(true);
}

void XQRobotSetupWidget::init_StackLayout()
{
	m_StackLayout = new QStackedLayout();
	m_StackLayout->addWidget(init_DingDingWidget());

}

QBoxLayout* XQRobotSetupWidget::init_OneRow()
{
	m_RobotBox = new QComboBox();
	m_RobotBox->setFont(QFont("黑体", 15));
	m_Enabled = new QCheckBox("启用");
	m_Enabled->setFont(QFont("黑体", 15));
	auto layout = new QBoxLayout(QBoxLayout::LeftToRight);//从左到右
	layout->addWidget(m_Enabled);
	layout->addWidget(m_RobotBox);
	layout->addStretch(1);
	//添加数据
	m_RobotBox->addItem("钉钉机器人",DingDing);
	//绑定下槽函数
	connect(m_RobotBox,&QComboBox::currentIndexChanged,m_StackLayout, &QStackedLayout::setCurrentIndex);
	connect(m_RobotBox, &QComboBox::currentIndexChanged, this, &XQRobotSetupWidget::readRobotEnabled);
	return layout;
}

QWidget* XQRobotSetupWidget::init_DingDingWidget()
{
	m_tokenDingDing = new QLineEdit();
	m_secretDingDing= new QLineEdit();
	auto label = new QLabel("参数设置:");
	label->setFont(QFont("黑体", 15));
	QWidget* widget = new QWidget;
	auto layout = new QGridLayout(widget);
	layout->setContentsMargins(10, 10, 10, 10);
	layout->addWidget(label,0, 0);
	layout->addWidget(new QLabel("token(令牌):"), 1, 0);
	layout->addWidget(m_tokenDingDing, 1, 1);
	layout->addWidget(new QLabel("secret(密钥):"), 2, 0);
	layout->addWidget(m_secretDingDing, 2, 1);
	widget->setLayout(layout);
	return widget;
}

QWidget* XQRobotSetupWidget::init_test()
{
	m_testText = new QLineEdit();
	m_testSend = new QPushButton("发送",this);
	auto label = new QLabel("测试:");
	label->setFont(QFont("黑体", 15));
	QWidget* widget = new QWidget;
	auto layout = new QGridLayout(widget);
	layout->setContentsMargins(10, 10, 10, 10);
	layout->addWidget(label, 0, 0);
	layout->addWidget(new QLabel("发送文本:"), 1, 0);
	layout->addWidget(m_testText, 1, 1);
	layout->addWidget(m_testSend, 1, 2);
	widget->setLayout(layout);

	//绑定槽函数 
	connect(m_testSend, &QPushButton::pressed, this, &XQRobotSetupWidget::testSend);
	//绑定回车确认发送
	m_testText->installEventFilter(new XQReturnEventFilter([=] {emit m_testSend->pressed(); }, m_testText));
	return widget;
}
