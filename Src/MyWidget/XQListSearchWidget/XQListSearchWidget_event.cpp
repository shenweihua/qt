﻿#include "XQListSearchWidget.h"
#include<QCloseEvent>
#include<QLineEdit>
#include<QListWidget>
void XQListSearchWidget::resizeEvent(QResizeEvent* event)
{
}

void XQListSearchWidget::changeEvent(QEvent* event)
{
	//窗口不是激活状态将关闭
	if (event->type() == QEvent::ActivationChange)
	{
		if (!isActiveWindow())
		{
			if (m_AutoClose)
				close();
			else
				hide();
		}
	}
	QWidget::changeEvent(event);
}

void XQListSearchWidget::closeEvent(QCloseEvent* event)
{
	m_listWidget->clearSelection();
	m_search->clear();
	deleteLater();
}
