﻿#ifndef XQListSearchWidget_H
#define XQListSearchWidget_h
#include<QWidget>
#include"XQHead.h"
//列表搜索小部件

class XQListSearchWidget:public QWidget
{
	Q_OBJECT
public:
	XQListSearchWidget(QWidget* parent = nullptr);
	~XQListSearchWidget();
	//返回当窗口不是活动窗口时自动关闭窗口
	bool activityAutoClose()const;
	//获取数据库的数据
	QStringList& database();
	//获取我已经选择的数据
	QStringList& selectedData();
	//获取检查时间
	size_t testingTime()const;
public slots://槽函数
	//设置当窗口不是活动窗口时自动关闭窗口
	void setActivityAutoClose(bool flag);
	//设置数据库，用来选择的数据
	void setDatabase(const QStringList& data);
	//设置我已经选择的数据
	void setSelectedData(const QStringList& data);
	//设置检查时间
	void setTestingTime(size_t time);
	//改变列表,根据输入框值
	void setChangeList();
signals://信号

	//选择
	void selectText(const QString& text);
protected://

protected://初始化
	void init();
	void init_UI();
	QBoxLayout* init_SearchBox();
	QListWidget* init_ListWidget();
protected://事件
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* event)override;
	//窗口状态改变了
	void changeEvent(QEvent* event)override;
	void closeEvent(QCloseEvent* event)override;
protected://变量
	bool m_AutoClose = false;//是否自动关闭窗口
	QStringList m_selectedData;//已经选择的数据，用来防止重复选择
	QStringList m_database;//数据库
	QLineEdit* m_search = nullptr;//搜索框
	XQAnimationButton* m_okBtn = nullptr;//确认按钮
	QListWidget* m_listWidget = nullptr;//列表小部件
	QDateTime* m_startTime = nullptr;//开始的时间记录
	size_t m_testingTime = 1000;//检查搜索款变化时间ms
};
#endif