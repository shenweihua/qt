﻿#include"XQListWidget.h"
#include"XQAlgorithm.h"
#include"XQComboCheckButton.h"
#include"XQReturnEventFilter.hpp"
#include<QListWidget>
#include<QBoxLayout>
#include<QSpinBox>
#include<QLabel>
#include<QComboBox>
#include<QPushButton>
void XQListWidget::init()
{
	init_ui();
	//数据量改变时显示到界面上
	connect(this, &XQListWidget::dataCountChange, [=](size_t count) { m_count->setText(QString("数量:%1").arg(count)); /*m_sumCount = count;*/
	if(m_onePagesCount!=0&& m_showData!=nullptr)
	{
		if (count == 0)
		{
			setNumberPages(0, 0, -1, QString("页数:%1").arg(0));
			return;
		}
		int PagesCount = curNumberPages();
		int num = count / PagesCount + ((count % PagesCount) == 0 ? 0 : 1);
		setNumberPages(1, num, -1, QString("页数:%1").arg(num));
	}
		});
	setCssFile(this, ":/XQListWidget/style.css");
	//链接查询实现函数和
	connect(m_findBtn,&QPushButton::pressed,this,&XQListWidget::searchData);
	//切换页码
	connect(m_paging,&QSpinBox::valueChanged,this, QOverload<size_t>::of(&XQListWidget::showListExhibitWidget));
	/*m_listWidget->resize(1000,600);*/
	//查询回车按下
	m_findEdit->installEventFilter(new XQReturnEventFilter([=] {emit m_findBtn->pressed(); }, m_findEdit));
	
}
