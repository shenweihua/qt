﻿#include "XQListExhibitWidget.h"
#include"XQListData.h"
XQListExhibitWidget::XQListExhibitWidget(QWidget* parent)
	:QWidget(parent)
{
}

XQListExhibitWidget::~XQListExhibitWidget()
{
}

XQListData* XQListExhibitWidget::data() const
{
	return m_data;
}

void XQListExhibitWidget::setData(XQListData* data)
{
	if(data!=nullptr)
	{
		m_data = data;
		connect(this, &XQListExhibitWidget::free, data, &XQListData::freeRequest);
		updata();
	}
}

void XQListExhibitWidget::updata()
{
}
