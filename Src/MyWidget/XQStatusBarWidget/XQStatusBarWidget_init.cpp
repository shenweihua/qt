﻿#include"XQStatusBarWidget.h"
#include"XQFuncEventFilter.hpp"
#include"XQLogWidget.h"
#include"XQAlgorithm.h"
#include"XQAppInfo.h"
#include"XQMouseEventFilter_Press.hpp"
#include<QProgressBar>
#include<QPushButton>
#include<QMenu>
void XQStatusBarWidget::init()
{
	init_ProgressBar();
	init_ui();
	installEventFilter(new XQFuncEventFilter(this));
	installEventFilter(new XQMouseEventFilter_Press([=] {
			contextMenuRequested();
		}, Qt::MouseButton::RightButton,this));
	//设置默认右键菜单
	setMenuFunc([this] (QMenu* menu){
			addMenu_SystemInfoWidge(menu);
			addMenu_LogWidge(menu);
			addMenu_LogButton(menu);
		});
}

void XQStatusBarWidget::init_ui()
{
	setFont(QFont("黑体", 15));
	addPermanentWidget(m_progressBar);
	setSizeGripEnabled(false); // 隐藏指示器
}

void XQStatusBarWidget::init_ProgressBar()
{
	m_progressBar = new QProgressBar(this);
	m_progressBar->hide();
	m_progressBar->setFont(QFont("黑体", 15));
	m_progressBar->setFixedWidth(400);
	m_progressBar->setAlignment(Qt::AlignCenter);
	m_progressBar->setRange(0, 100);
	m_progressBar->setValue(100);
	connect(this,&XQStatusBarWidget::setProgressRange, m_progressBar,&QProgressBar::setRange,Qt::QueuedConnection);
	connect(this,QOverload<int>::of(&XQStatusBarWidget::setProgressValue), m_progressBar, &QProgressBar::setValue, Qt::QueuedConnection);
}

void XQStatusBarWidget::contextMenuRequested()
{
	if (m_menuFunc == nullptr)
		return;
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	m_menuFunc(menu);
	menu->popup(QCursor::pos());
}


