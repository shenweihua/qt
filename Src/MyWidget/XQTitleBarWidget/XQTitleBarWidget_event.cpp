﻿#include"XQTitleBarWidget.h"
#include"XQEventFilterObject.h"
#include"XQAlgorithm.h"
#include"XQStackedWidget.h"
#include"XQStackedTitleWidget.h"
#include<QResizeEvent>
#include<QLabel>
#include<QPushButton>
#include<QMenu>
#include<QDebug>
void XQTitleBarWidget::resizeEvent(QResizeEvent* ev)
{
	m_icon->setFixedSize(height(), height());
	m_title->setFixedHeight(height());
	for (auto& button:m_buttons)
	{
		if(button!=nullptr)
			button->setFixedSize(height(), height());
	}
}

void XQTitleBarWidget::mousePressEvent(QMouseEvent* ev)
{
	
	if (m_mouseMoveWidge)
		pressPos = ev->pos();//记录位置
	else
		ev->ignore();
}

void XQTitleBarWidget::mouseMoveEvent(QMouseEvent* ev)
{
	if (m_mouseMoveWidge&&ev->buttons() & Qt::MouseButton::LeftButton)//左键按下的时候
	{
		controlWidget()->move(ev->globalPosition().toPoint() - pressPos);//移动窗口
	}
	else if (!m_mouseMoveWidge)
		ev->ignore();
	//找最顶层窗口QWidget
	QWidget* parent= stackedWidget();
	while (parent)
	{
		auto widget = parent->parentWidget();
		if (widget == nullptr)
			break;
		parent = widget;
	}
	if (stackedWidget()&& m_autoMouseHeap && parent&&parent->isVisible()&& !parent->isMinimized())
	{//拖动入堆栈
		auto title = stackedWidget()->titleWidget();
		QRect globalRect = title->geometry();
		globalRect.moveTopLeft(title->mapToGlobal(globalRect.topLeft()));
		if (globalRect.contains(QCursor::pos()))
			stackedWidget()->addWidget(controlWidget());
	}
}

void XQTitleBarWidget::mouseDoubleClickEvent(QMouseEvent* ev)
{
	if (m_mouseDoubleClickmaxWidget && (m_types & XQTBWType::maximized))//最大化按钮存在时，双击标题栏最大化窗口
	{
		if (ev->button() == Qt::LeftButton) {
			// 处理左键双击事件
			maximizedWidget();
		}
		else if (ev->button() == Qt::RightButton) {
			// 处理右键双击事件
			// ...
		}
	}
	else if(!m_mouseDoubleClickmaxWidget)
		ev->ignore();
}

void XQTitleBarWidget::leaveEvent(QEvent* event)
{
	//QPoint globalPos = QCursor::pos();
	//QPoint pos = mapToGlobal(QPoint(0, 0)); // 将当前 QWidget 窗口左上角坐标转换为全局坐标
	//qInfo() << "鼠标离开"<< globalPos<< pos;
}

void XQTitleBarWidget::WindowStaysOnTopHintEventFilter()
{
	/*qInfo() << "状态改变";*/
	if (m_types & XQTBWType::top)//存在置顶按钮
	{
		QWidget* widget = this->controlWidget();
		// 判断窗口是否处于置顶状态
		if (widget->windowFlags() & Qt::WindowStaysOnTopHint)
		{
			/*qInfo() << "置顶状态";*/
			auto btn = QObject::findChild<QPushButton*>("topOff");
			if (btn != nullptr)
			{
				btn->setObjectName("topOn");
				//重新加载以其更新
				setCssFile(this, ":/XQTitleBarWidgetx/style.css");
			}
		}
		else
		{
			auto btn = QObject::findChild<QPushButton*>("topOn");
			if (btn != nullptr)
			{
				btn->setObjectName("topOff");
				//重新加载以其更新
				setCssFile(this, ":/XQTitleBarWidgetx/style.css");
			}
		}
	}
}

void XQTitleBarWidget::contextMenuRequested()
{
	QMenu* menu = new QMenu(this);
	menu->setAttribute(Qt::WA_DeleteOnClose);
	menuStackedWidget(menu);
	
	menu->popup(QCursor::pos());
}

void XQTitleBarWidget::menuStackedWidget(QMenu* mainMenu)
{
	if (!stackedWidget())
		return;
	QMenu* menu = new QMenu("堆栈", mainMenu);
	mainMenu->addMenu(menu);
	menu->addAction("加入堆栈", [=]{
		m_StackedWidget->addWidget(controlWidget());
		m_StackedWidget->setCurrentWidget(controlWidget());
		});
	if(m_autoMouseHeap)
		menu->addAction("关闭鼠标拖动入堆栈", [=] {m_autoMouseHeap=false; });
	else
		menu->addAction("开启鼠标拖动入堆栈", [=] {m_autoMouseHeap = true; });
}
