﻿#ifndef XQTITLEBARWIDGET_H
#define XQTITLEBARWIDGET_H
#include<QWidget>
#include<QMap>
#include"XQTBWType.h"
#include"XQHead.h"
//#define MAXBTNCOUNT 5//按钮最大数量

//标题栏小部件
class XQTitleBarWidget:public QWidget
{
	Q_OBJECT
public:
	XQTitleBarWidget(QWidget* parent=nullptr);
	~XQTitleBarWidget();
	//返回需要控制的小部件
	QWidget* controlWidget();
	//返回标题
	QString windowTitle()const;
	//返回窗口图标
	QPixmap windowIcon()const;
	//返回按钮间距
	int btnSpacing()const;
	//获取按钮种类
	int buttons()const;
	//获取皮肤窗口
    QWidget* skinWidget()const;
	//获取是否鼠标移动窗口
	bool mouseMoveWidget()const;
	//获取双击标题栏是否最大化窗口
	bool mouseDoubleClickmaxWidget()const;
	//获取鼠标是否可以拉伸窗口
	bool mouseStretchWidget()const;
	//堆栈小部件
	XQStackedWidget* stackedWidget();
public :
	//堆栈小部件
	void setStackedWidget(XQStackedWidget* StackedWidget);
	//设置按钮
	void setButtons(int types);
	void setButtons(const QList<XQTBWType::Type>& types);
	//设置需要控制的小部件
	void setControlWidget(QWidget* widget);
	//设置窗口标题
	void setWindowTitle(const QString& title);
	//设置窗口图标
	void setWindowIcon(const QPixmap& icon,bool circle =true/*圆形*/);
	void setWindowIcon(const QIcon& icon, bool circle = true/*圆形*/);
	//设置按钮间距
	void setBtnSpacing(int Spacing);
	//设置弹出的皮肤窗口指针
	void setSkinWidget(QWidget*widget);
	//设置鼠标移动窗口
	void setMouseMoveWidget(bool flag);
	//设置双击标题栏是否最大化窗口
	void setMouseDoubleClickmaxWidget(bool flag);
	//设置鼠标是否可以拉伸窗口
	void setMouseStretchWidget(bool flag);
	//设置自动隐藏显示标题栏，作为子窗口的时候
	void setAutoHide(bool hide);
	//内部已经绑定的槽函数
	//显示皮肤小部件
	void showSkinWidget();
	//置顶窗口
	void topWidget();
	//最小化窗口
	void minimizedWidget();
	//最大化窗口
	void maximizedWidget();
	//窗口关闭
	void closeWidget();
signals://信号
	void skinBtn();//单机了皮肤
	void topBtn();//单机了置顶
	void minimizedBtn();//单机了最小化
	void maximizedBtn();//单机了最大化
	void closeBtn();//单机关闭
protected:
	void init();
	void init_ui();
protected:
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* ev)override;
	//鼠标按下
	void mousePressEvent(QMouseEvent* ev)override;
	//鼠标移动
	void mouseMoveEvent(QMouseEvent* ev)override;
	//鼠标双击事件
	void mouseDoubleClickEvent(QMouseEvent* ev)override;
	//鼠标离开
	void leaveEvent(QEvent* event)override;
	//窗口状态改变置顶改变按钮图标事件过滤器的函数
	void WindowStaysOnTopHintEventFilter();
	void contextMenuRequested();
	void menuStackedWidget(QMenu*menu);
protected:
	bool m_mouseMoveWidge = true;//鼠标移动窗口
	bool m_mouseDoubleClickmaxWidget = true;//双击最大化窗口
	bool m_autoHide = true;
	bool m_autoMouseHeap = false;//鼠标自动入堆栈
	XQStackedWidget* m_StackedWidget = nullptr;//绑定堆栈窗口小部件
	XQStretchEventFilter* m_StretchEventFilter = nullptr;//鼠标拉伸窗口事件过滤器
	XQEventFilterObject* m_TopHintEventFilter = nullptr;//窗口状态改变置顶改变按钮图标
	QWidget* m_skinWidget = nullptr;//设置皮肤的窗口
	QLabel* m_icon = nullptr;//图标
	QLabel* m_title = nullptr;//标题
	QBoxLayout* m_layoutBtn= nullptr;//按钮布局
	
	int m_types = 0; /*XQTBWType::top | XQTBWType::minimized | XQTBWType::maximized | XQTBWType::close*/;//默认启用的按钮
	QMap<XQTBWType*,QPushButton*>m_buttons;//已经有的按钮
	static QList<XQTBWType> m_typeAll;//全部的控件数据
	QWidget* m_parentWidget = nullptr;//父窗口指针
	QPoint pressPos;//窗口拖动时记录的坐标
};
#endif