﻿#ifndef  XQTBWType_H
#define XQTBWType_H
#include<QObject>
#include"XQHead.h"
//using XQTBWFunc = std::function<void(XQTitleBarWidget*)>;//XQTitleBarWidget的函数指针
using XQTBWFunc = void(XQTitleBarWidget::*)();//XQTitleBarWidget的函数指针
//标题栏小部件内的控件类型
class XQTBWType
{
public:
	enum  Type
	{
		top = 1,//置顶
		minimized = 2,//最小化
		maximized = 4,//最大化
		close = 8,//关闭
		skin = 16//皮肤
	};
	XQTBWType();
	XQTBWType(Type type, const QString& name, XQTBWFunc func);
	~XQTBWType();
	XQTBWType::Type type()const;
	QString objectName()const;
	XQTBWFunc func()const;

	void setType(Type type);
	void setObjectName(const QString& name);
	void setFunc(XQTBWFunc func);
protected:
	Type m_type;//类型
	QString m_objectName;//名字
	XQTBWFunc m_func;//成员函数指针
	//void(XQTitleBarWidget::*slotsFunc)();//槽函数指针
};

#endif // ! XQTBWType_H
