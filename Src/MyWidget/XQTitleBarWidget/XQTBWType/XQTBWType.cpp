﻿#include "XQTBWType.h"
#include"XQTitleBarWidget.h"
XQTBWType::XQTBWType()
{
}

XQTBWType::XQTBWType(Type type, const QString& name, XQTBWFunc func)
	:m_type(type),
	m_objectName(name),
	m_func(func)
{
}


XQTBWType::~XQTBWType()
{
}

XQTBWType::Type XQTBWType::type() const
{
	return m_type;
}

QString XQTBWType::objectName() const
{
	return m_objectName;
}

XQTBWFunc XQTBWType::func() const
{
	return m_func;
}

void XQTBWType::setType(Type type)
{
	m_type = type;
}

void XQTBWType::setObjectName(const QString& name)
{
	m_objectName = name;
}

void XQTBWType::setFunc(XQTBWFunc func)
{
	m_func = func;
}
