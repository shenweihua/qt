﻿#include"XQPython.hpp"
#undef slots
#include"Python.h"

void XQPython::init()
{
	wchar_t buff[512]{0};
	m_PythonHome.toWCharArray(buff);
	Py_SetPythonHome(buff);
	if (!Py_IsInitialized()) {
		//python环境的初始化
		Py_Initialize();
		//初始化python系统文件路径
		PyRun_SimpleString("import sys");
		PyRun_SimpleString("sys.path.append(r'')");
	}
	
	// 创建第一个Python解释器实例
	/*m_threadState = Py_NewInterpreter();*/
	m_PyObject_GetAttrString = PyObject_GetAttrString;
	m_PyObject_CallFunction = PyObject_CallFunction;
	m_PyArg_Parse = PyArg_Parse;
	m_PyArg_ParseTuple = PyArg_ParseTuple;


}

void XQPython::PyDECREF(void* object)
{
#ifndef _DEBUG
	if(object!=nullptr)
		Py_DECREF(object);
#endif // _DEBUG
}

int XQPython::PyDictCheck(PyObject* object)
{
	return PyDict_Check(object);
}

size_t XQPython::PyDictSize(PyObject* object)
{
	return PyDict_Size(object);
}

PyObject* XQPython::PyDictKeys(PyObject* object)
{
	return PyDict_Keys(object);
}

PyObject* XQPython::PyListGetItem(PyObject* object, size_t nSel)
{
	return PyList_GetItem(object, nSel);
}

PyObject* XQPython::PyDictGetItem(PyObject* object, PyObject* key)
{
	return PyDict_GetItem(object,key);
}

QVariant XQPython::getReturnValue(PyObject* pValue)
{
	QVariant data;
	if (PyBool_Check(pValue))
	{//BOOL布尔类型
		data = bool(PyObject_IsTrue(pValue));
	}
	else if (PyFloat_Check(pValue))
	{//float 浮点数处理
		data = PyFloat_AsDouble(pValue);
	}
	else if (PyLong_Check(pValue))
	{//long 整形处理
		data = PyLong_AsLongLong(pValue);
	}
	else if (PyList_Check(pValue))
	{//list处理
		QVariantList list;
		size_t count = PyList_Size(pValue);
		for (size_t i = 0; i < count; i++)
		{
			// 获取列表中的值
			PyObject* pListValue = PyList_GetItem(pValue, i);  // 获取元素
			// 对返回的值进行处理...
			list<<getReturnValue(pListValue);
			//PyDECREF(pListValue);  // 释放引用
		}
		data = list;
	}
	else if (PyTuple_Check(pValue))
	{//tuple 元组处理
		QVariantList list;
		size_t count = PyTuple_Size(pValue);
		for (size_t i = 0; i < count; i++)
		{
			// 获取元组中的值
			PyObject* pTupleValue = PyTuple_GetItem(pValue, i);  // 获取元素
			// 对返回的值进行处理...
			list << getReturnValue(pTupleValue);
			//PyDECREF(pTupleValue);  // 释放引用
		}
		data = list;
	}
	else if (PyBytes_Check(pValue))
	{//字节类型
		Py_ssize_t size = PyBytes_Size(pValue);
		const char* bytes = PyBytes_AsString(pValue);
		data = QByteArray(bytes, size);
	}
	else if (PyUnicode_Check(pValue))
	{//字符串处理
		data=PyUnicode_AsUTF8(pValue);
		/*char* str = nullptr;
		PyArg_Parse(pValue, "s", &str);
		data = QString(str);*/
	}
	else if (PyDict_Check(pValue))
	{//字典处理
		/*QMap<QVariant, QVariant> dict;
		Py_ssize_t dictSize = PyDict_Size(pValue);
		PyObject* keys = PyDict_Keys(pValue);
		for (Py_ssize_t i = 0; i < dictSize; ++i) {
			PyObject* key = PyList_GetItem(keys, i);
			PyObject* value = PyDict_GetItem(pValue, key);
			dict.insert("10", getReturnValue(value));
			PyDECREF(key);
			PyDECREF(value);
		}*/
		//data = dict;
	}
	else 
	{//其他
		
	}
	
	if (pValue == m_pValue)
	{
		PyDECREF(pValue);//清理上次的
		m_pValue = nullptr;
	}
	return std::move(data);
}
