﻿#include"XQLoginWidget.h"
#include"XQUserInfo.h"
#include"XQAppInfo.h"
#include<QComboBox>
#include<QLineEdit>
#include<QAction>
#include<QCheckBox>
#include<QLabel>
XQLoginWidget::XQLoginWidget(QWidget* parent, bool AutoUi)
{
	if(AutoUi)
		init();
}

XQLoginWidget::~XQLoginWidget()
{
	if (m_userNameAction != nullptr)
		m_userNameAction->deleteLater();
	if (m_passwordAction != nullptr)
		m_passwordAction->deleteLater();
}

QPixmap XQLoginWidget::headPortrait() const
{
	return m_headPixmap;
}

QString XQLoginWidget::userName()
{
	return m_userNameIn->lineEdit()->text();
}

QStringList XQLoginWidget::userNames()
{
	QStringList list;
	for (int i = 0; i < m_userNameIn->count(); ++i) {
		list<< m_userNameIn->itemText(i);
	}
	return std::move(list);
}

QString XQLoginWidget::password()
{
	return std::move(m_passwordIn->text());
}

bool XQLoginWidget::autoLoginState() const
{
	return m_autoLoginBtn->checkState();
}

XQAppInfo* XQLoginWidget::appInfo() const
{
	return m_appInfo;
}

void XQLoginWidget::setAppInfo(XQAppInfo* appInfo)
{
	m_appInfo = appInfo;
}

void XQLoginWidget::setUserName(const QString& name)
{
	m_userNameIn->setEditText(name);
}

void XQLoginWidget::setPassword(const QString& password)
{
	//qInfo() << password;
	m_passwordIn->setText(password);
}

void XQLoginWidget::setHeadPortrait(const QPixmap& image)
{
	m_headPixmap = image;
	auto pixmap = QPixmap(image);
	pixmap = pixmap.scaled(m_headPortrait->size());
	pixmap.setMask(QBitmap::fromImage(QPixmap(":/XQLoginWidget/images/portrait/mask.png").scaled(m_headPortrait->size()).toImage()));
	m_headPortrait->setPixmap(pixmap);
	m_headPortrait->update();
}
void XQLoginWidget::loginPressed()
{
	auto userInfo = m_appInfo->userInfo();
	userInfo->setAccount(userName());
	userInfo->setPassword(password());
	qInfo() << "登录按钮按下";
}
