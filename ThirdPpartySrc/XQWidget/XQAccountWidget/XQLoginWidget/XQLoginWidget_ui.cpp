﻿#include"XQLoginWidget.h"
#include"XQFocusGotEventFilter.hpp"
#include"XQFocusLoseEventFilter.hpp"
#include"XQTitleBarWidget.h"
#include<QBoxLayout>
#include<QLabel>
#include<QComboBox>
#include<QLineEdit>
#include<QCheckBox>
#include<QPushButton>
#include<QAction>
#include<QMovie>
#include<QGraphicsDropShadowEffect>
void XQLoginWidget::init_UI()
{
    setFixedSize(604, 404);
    setWindowTitle("登录");
    setWindowIcon(QIcon(":/XQLoginWidget/images/username-focus.png"));
    auto widget = new QWidget(this);
    widget->setFixedSize(600, 400);
    widget->move(2, 2);
    ////设置焦点顺序
    //setTabOrder(m_userNameIn,m_passwordIn);
    //setTabOrder(m_passwordIn, m_loginBtn);
    //setTabOrder(m_loginBtn, m_userNameIn);
    //设置背景动图
    auto movie = new QMovie(":/XQLoginWidget/images/gifLabel.gif","",widget);
    auto background = new QLabel(widget);
    background->setMovie(movie);//绑定
    background->move(0, 0);//移动位置
    background->resize(width(), height() * 0.4);//设置大小
    movie->setScaledSize(background->size()); // 设置缩放大小
    movie->start();//开始播放
   //设置标题栏
    auto title = new XQTitleBarWidget(this);
    title->setBtnSpacing(20);
    title->setButtons(XQTBWType::top| XQTBWType::minimized| XQTBWType::close);
    title->setMouseStretchWidget(false);
    //控件布局
    auto Layout = new QBoxLayout(QBoxLayout::Direction::TopToBottom, this);//从上到下
    Layout->setContentsMargins(0, 0, 0, 0);
    Layout->addWidget(title);
    Layout->addStretch(1);
    Layout->addLayout(init_oneRow());
    Layout->addLayout(init_twoRow());
    Layout->addLayout(init_threeRow());
    Layout->addLayout(init_fourRow());
    Layout->addLayout(init_fiveRow());
    Layout->addLayout(init_sixRow());
    widget->setLayout(Layout);
    //窗口阴影
    auto effect = new QGraphicsDropShadowEffect(this);
    effect->setColor(qRgb(200,200,200));
    effect->setBlurRadius(2);
    effect->setOffset(0,0);
    setGraphicsEffect(effect);
}

QBoxLayout* XQLoginWidget::init_oneRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    m_headPortrait = new QLabel(this);
    Layout->addWidget(m_headPortrait,0,Qt::AlignHCenter);

    m_headPortrait->setFixedSize(100, 100);
    setHeadPortrait(QPixmap(":/XQLoginWidget/images/portrait/default.jpg"));
    return Layout;
}

QBoxLayout* XQLoginWidget::init_twoRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    m_userNameIn = new QComboBox(this);
    Layout->addWidget(m_userNameIn, 0, Qt::AlignHCenter);
    m_userNameIn->setEditable(true);
    m_userNameIn->setFixedSize(m_userpassSize);
    m_userNameIn->lineEdit()->setPlaceholderText("用户名/邮箱");
    //用户名框密码设置
    m_userNameAction = m_userNameIn->lineEdit()->addAction(QIcon(":/XQLoginWidget/images/username.png"), QLineEdit::LeadingPosition);
   //获得焦点时
    m_userNameIn->installEventFilter(new XQFocusGotEventFilter([=] {
   m_userNameIn->removeAction(m_userNameAction);//清除上一次的图标
   m_userNameAction->deleteLater();//图标动作释放
   m_userNameAction= m_userNameIn->lineEdit()->addAction(QIcon(":/XQLoginWidget/images/username-focus.png"), QLineEdit::LeadingPosition);}, m_userNameIn));
   //失去焦点时
   m_userNameIn->installEventFilter(new XQFocusLoseEventFilter([=] {
   m_userNameIn->removeAction(m_userNameAction);//清除上一次的图标
   delete m_userNameAction;//图标动作释放
   m_userNameAction = m_userNameIn->lineEdit()->addAction(QIcon(":/XQLoginWidget/images/username.png"), QLineEdit::LeadingPosition);
   m_userNameIn->lineEdit()->update(); }, m_userNameIn));
    return Layout;
}

QBoxLayout* XQLoginWidget::init_threeRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    m_passwordIn = new QLineEdit(this);
    Layout->addWidget(m_passwordIn, 0, Qt::AlignHCenter);
    m_passwordIn->setFixedSize(m_userpassSize);
    m_passwordIn->setPlaceholderText("密码");
    m_passwordIn->setEchoMode(QLineEdit::Password);
    //密码框图标设置
    m_passwordAction = m_passwordIn->addAction(QIcon(":/XQLoginWidget/images/passwd.png"), QLineEdit::LeadingPosition);
    //获得焦点时
    m_passwordIn->installEventFilter(new XQFocusGotEventFilter([=] {
    m_passwordIn->removeAction(m_passwordAction);//清除上一次的图标
    m_passwordAction->deleteLater();//图标动作释放
    m_passwordAction = m_passwordIn->addAction(QIcon(":/XQLoginWidget/images/passwd-focus.png"), QLineEdit::LeadingPosition); }, m_passwordIn));
    //失去焦点时
    m_passwordIn->installEventFilter(new XQFocusLoseEventFilter([=] {
    m_passwordIn->removeAction(m_passwordAction);//清除上一次的图标
    m_passwordAction->deleteLater();//图标动作释放
    m_passwordAction = m_passwordIn->addAction(QIcon(":/XQLoginWidget/images/passwd.png"), QLineEdit::LeadingPosition); }, m_passwordIn));
    return Layout;
}

QBoxLayout* XQLoginWidget::init_fourRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    Layout->addStretch(1);
    m_autoLoginBtn = new QCheckBox(this);
    Layout->addWidget(m_autoLoginBtn, 0);
    m_savePasswordBtn = new QCheckBox(this);
    Layout->addWidget(m_savePasswordBtn, 0);
    m_retrievePasswordBtn = new QPushButton(this);
    Layout->addWidget(m_retrievePasswordBtn, 0);
    Layout->addStretch(1);
    Layout->setSpacing(40);//设置间距
    m_autoLoginBtn->setText("自动登录");
    m_autoLoginBtn->setObjectName("autoLogin");
    m_autoLoginBtn->setFixedSize(m_btnSize);
    m_savePasswordBtn->setText("记住密码");
    m_savePasswordBtn->setObjectName("savePass");
    m_savePasswordBtn->setFixedSize(m_btnSize);
    //当自动登录为选中是将保存密码也选中
    connect(m_autoLoginBtn, &QCheckBox::stateChanged, [this](int state) {
        if (Qt::Checked == state)
            m_savePasswordBtn->setCheckState(Qt::Checked);
        });
    m_retrievePasswordBtn->setText("找回密码");
    m_retrievePasswordBtn->setObjectName("retrPassBtn");
   /* m_retrievePasswordBtn->setFixedSize(m_btnSize);*/
    return Layout;
}

QBoxLayout* XQLoginWidget::init_fiveRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    m_loginBtn = new QPushButton(this);
    Layout->addWidget(m_loginBtn, 0, Qt::AlignHCenter);
    m_loginBtn->setText("登录");
    m_loginBtn->setFixedSize(m_loginbtnSize);
    m_loginBtn->setObjectName("loginBtn");
    return Layout;
}

QBoxLayout* XQLoginWidget::init_sixRow()
{
    auto Layout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);//从左到右
    m_registerBtn = new QPushButton(this);
    Layout->addWidget(m_registerBtn, 0);
    Layout->addStretch(1);
    m_registerBtn->setText("注册账号");
    m_registerBtn->setObjectName("registerBtn");
    return Layout;
}
