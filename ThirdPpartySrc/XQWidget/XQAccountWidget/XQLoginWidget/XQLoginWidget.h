﻿#ifndef XQLOGINWIDGET_H
#define XQLOGINWIDGET_H
#include<QWidget>
#include"XQHead.h"
//登录小部件
class XQLoginWidget :public QWidget
{
	Q_OBJECT
public:
	XQLoginWidget(QWidget* parent = nullptr,bool AutoUi=true);
	virtual~XQLoginWidget();
	//获取头像
	QPixmap headPortrait()const;
	//获得用户名
	QString userName();
	//获取用户列表
	QStringList userNames();
	//获得密码
	QString password();
	//自动登录状态
	bool autoLoginState()const;
	//获取app基本共享数据
	XQAppInfo* appInfo()const;
public slots:
	//设置app基本信息
	void setAppInfo(XQAppInfo* appInfo);//不提供内存释放，因为共享使用
	//设置用户名
	void setUserName(const QString& name);
	//设置密码
	void setPassword(const QString& password);
	//设置头像
	void setHeadPortrait(const QPixmap& image);
	//登录按钮按下
	virtual void loginPressed();
	//打开注册窗口
	virtual void openCreateAccountWidget();
	//打开找回密码窗口
	virtual void openRetrievePassword();
signals://信号 完成的信号都没有发出的
	//登录完成
	void loginFinish(XQUserInfo* userInfo);
protected://隐藏的函数
	//居中打开一个小部件
	void centerOpenWidget(QWidget* widget);
	//配置文件初始化
	void init_configurationFile();//
protected://初始化和ui界面
	//初始化
	virtual void init();
	//初始化UI
	virtual void init_UI();
	//第一行主要是头像
	virtual QBoxLayout* init_oneRow();
	//第二行主要是账户输入框
	virtual QBoxLayout* init_twoRow();
	//第三行主要是密码框
	virtual QBoxLayout* init_threeRow();
	//第四行主要是多选框和找回密码
	virtual QBoxLayout* init_fourRow();
	//第五行主要是登录按钮
	virtual QBoxLayout* init_fiveRow();
	//第六行主要是注册按钮
	virtual QBoxLayout* init_sixRow();
protected://事件
	//窗口大小改变事件
	virtual void resizeEvent(QResizeEvent* event)override;
	//窗口关闭事件
	virtual void closeEvent(QCloseEvent* event)override;
protected:
	XQAppInfo* m_appInfo;//app的基本共享信息
	QSize m_userpassSize = QSize(350, 30);//用户和密码大小
	QSize m_btnSize= QSize(90,45);//用户和密码大小
	QSize m_loginbtnSize = QSize(350, 50);//登录大小
	QPixmap m_headPixmap;//头像原图
	QLabel* m_headPortrait = nullptr;//头像
	QComboBox* m_userNameIn = nullptr;//用户名输入框
	QAction* m_userNameAction = nullptr;//用户名输入框的图标动作
	QLineEdit* m_passwordIn = nullptr;//密码输入框
	QAction* m_passwordAction = nullptr;//密码输入框的图标动作
	QCheckBox* m_autoLoginBtn = nullptr;//自动登录按钮
	QCheckBox* m_savePasswordBtn = nullptr;//保存密码按钮
	QPushButton* m_retrievePasswordBtn = nullptr;//找回密码按钮
	QPushButton* m_loginBtn = nullptr;//登录按钮
	QPushButton* m_registerBtn = nullptr;//注册按钮
};
#endif


