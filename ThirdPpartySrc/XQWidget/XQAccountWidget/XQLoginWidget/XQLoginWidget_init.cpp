﻿#include"XQLoginWidget.h"
#include"XQAlgorithm.h"
#include"XQReturnEventFilter.hpp"
#include"XQCreateAccountWidget.h"
#include"XQRetrievePasswordWidget.h"
#include"XQUserInfo.h"
#include"XQAppInfo.h"
#include<QFile>
#include<QPushButton>
#include<QComboBox>
#include<QKeyEvent>

void XQLoginWidget::init()
{
	init_UI();
    setCssFile(this, ":/XQLoginWidget/style.css");
	//过滤掉回车事件
	m_userNameIn->installEventFilter(new XQReturnEventFilter([](QKeyEvent* ev) {ev->accept(); }, m_userNameIn));
	//登录按钮，将数据写进userinfo待后面验证
	connect(m_loginBtn, &QPushButton::pressed,this,&XQLoginWidget::loginPressed);
	//打开注册窗口
	connect(m_registerBtn, &QPushButton::pressed, this,&XQLoginWidget::openCreateAccountWidget);
	//打开找回密码窗口
	connect(m_retrievePasswordBtn, &QPushButton::pressed,this, &XQLoginWidget::openRetrievePassword);
	/*m_userInfo = new XQUserInfo(this);*/
}
void XQLoginWidget::centerOpenWidget(QWidget* widget)
{
	//widget->setWindowModality(Qt::ApplicationModal); //设置模态
	//widget->setAttribute(Qt::WA_DeleteOnClose); //关闭时自动删除实例，防止内存泄漏
	//窗口关闭时
	connect(widget, &QWidget::destroyed, [this] {this->setEnabled(true); });
	this->setEnabled(false);
	//如果当前窗口时置顶的
	if (windowFlags() & Qt::WindowStaysOnTopHint)
		widget->setWindowFlag(Qt::WindowStaysOnTopHint);
	widget->show(); //显示窗口
	size_t w = widget->width(), h = widget->height();
	widget->move(pos() - (QPoint(w - width(), h - height()) / 2));//移动新窗口居中显示
}
void XQLoginWidget::init_configurationFile()
{
	auto settingsInfo = m_appInfo->settingsInfo();
}
void XQLoginWidget::openCreateAccountWidget()
{
	auto widget = new XQCreateAccountWidget; //创建自定义 QWidget 实例

	centerOpenWidget(widget);
}

void XQLoginWidget::openRetrievePassword()
{
	auto widget = new XQRetrievePasswordWidget; //创建自定义 QWidget 实例
	centerOpenWidget(widget);
}