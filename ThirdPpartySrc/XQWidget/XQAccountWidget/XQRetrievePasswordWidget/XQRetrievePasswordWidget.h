﻿#ifndef XQRETRIEVEPASSWORDWIDGET_H
#define XQRETRIEVEPASSWORDWIDGET_H
#include<QWidget>
#include"XQHead.h"
//找回密码小部件窗口
class XQRetrievePasswordWidget: public QWidget
{
	Q_OBJECT
public:
	XQRetrievePasswordWidget(QWidget* parent = nullptr);
	~XQRetrievePasswordWidget();
public slots:
	//发送验证码
	void sendVerify();
	//修改账号密码
	void modifyAccount();
signals://信号

protected://初始化和ui界面
	//初始化
	void init();
	//初始化UI
	void init_UI();
	//邮箱输入框初始化
	QBoxLayout* init_emailEdit();
	//验证码
	QBoxLayout* init_verify();
	//密码输入框初始化
	QBoxLayout* init_passwordEdit();
	//密码输入框初始化
	QBoxLayout* init_RedoPasswordEdit();
	//修改和关闭
	QBoxLayout* init_ModifyAndClose();
protected://事件
	//窗口显示事件
	void showEvent(QShowEvent* event)override;
	//窗口大小改变事件
	void resizeEvent(QResizeEvent* event)override;
	//窗口关闭事件
	void closeEvent(QCloseEvent* event)override;
protected:
	QLineEdit* m_emailEdit = nullptr;//邮箱输入框
	QLineEdit* m_verifyEdit = nullptr;//验证码的输入框
	QPushButton* m_sendVerifyBtn = nullptr;//发送验证码的按钮
	QLineEdit* m_passwordEdit = nullptr;//密码输入框
	QLineEdit* m_RedoPasswordEdit = nullptr;//密码输入框
};
#endif // !XQCreateAccountWidget_h
