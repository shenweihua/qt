﻿#ifndef XQCREATEACCOUNTWIDGET_H
#define XQCREATEACCOUNTWIDGET_H
#include<QWidget>
#include"XSymbolOverload.h"
#include<QDateTime>
#include"XQHead.h"
class XQAppInfo;
//创建注册账号小部件窗口
class XQCreateAccountWidget:public QWidget
{
	Q_OBJECT
public:
	XQCreateAccountWidget(QWidget* parent = nullptr, bool AutoUi = true);
	XQCreateAccountWidget(XQAppInfo* info,QWidget* parent = nullptr, bool AutoUi = true);
	virtual~XQCreateAccountWidget();
	//获取app基本共享数据
	XQAppInfo* appInfo();
	//获取过期时间
	size_t overtime()const;
	//获取重新发送时间
	size_t resendTime()const;
public slots:
	//设置app基本信息
	void setAppInfo(XQAppInfo* appInfo);//不提供内存释放，因为共享使用
	//设置过期时间
	void setOvertime(size_t time);
	//设置重新发送时间
	void setResendTime(size_t time);
	//注册账号按下
	virtual void registerAccountPressed();
	//发送验证码按下
	virtual void sendPressed();
	//发送验证码邮箱发送
	virtual void SendVerificationCode();
signals://信号
	//注册成功
	void createAccountFinish(const XQUserInfo* userInfo);
	//发送验证码成功
	void sendVerifyFinish();
protected://初始化和ui界面
	//初始化
	virtual void init();
	//初始化UI
	virtual void init_UI();
	//头像初始化()
	virtual QBoxLayout* init_headPortrait();
	//账号输入初始化
	virtual QBoxLayout* init_accountEdit();
	//密码输入框初始化
	virtual QBoxLayout* init_passwordEdit();
	//邮箱输入框初始化
	virtual QBoxLayout* init_emailEdit();
	//手机号输入框初始化
	virtual QBoxLayout* init_phoneEdit();
	//名字输入框初始化
	virtual QBoxLayout* init_nameEdit();
	//性别单选框初始化
	virtual QBoxLayout* init_genderButtons();
	//注册验证码
	virtual QBoxLayout* init_verify();
	//注册和关闭
	virtual QBoxLayout* init_registerAndClose();
protected://事件
	//窗口大小改变事件
	virtual void resizeEvent(QResizeEvent* event)override;
	//窗口关闭事件
	virtual void closeEvent(QCloseEvent* event)override;
protected:
	XQAppInfo* m_appInfo=nullptr;//app的基本共享信息
	XQSurfacePlotWidget* m_headPortrait =nullptr;//头像
	QLineEdit* m_accountEdit=nullptr;//账号输入框
	QLineEdit* m_passwordEdit = nullptr;//密码输入框
	QLineEdit* m_emailEdit = nullptr;//邮箱输入框
	QLineEdit* m_phoneEdit = nullptr;//手机号输入框
	QLineEdit* m_nameEdit = nullptr;//名字输入框
	QButtonGroup* m_gender = nullptr;//单选框按钮组
	QLineEdit* m_verifyEdit = nullptr;//验证码的输入框
	QPushButton* m_sendVerifyBtn = nullptr;//发送验证码的按钮
	QPushButton*  m_registerBtn = nullptr;//注册按钮
	QDateTime m_startTime = QDateTime::currentDateTime();//验证码开始时间
	size_t m_overtime = 5_mins;//过期时间
	size_t m_resendTime = 1_mins;//验证码重新发送时间
};
#endif // !XQCreateAccountWidget_h
