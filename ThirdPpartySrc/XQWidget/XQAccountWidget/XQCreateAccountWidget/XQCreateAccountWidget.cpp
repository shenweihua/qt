﻿#include"XQCreateAccountWidget.h"
#include"XQAlgorithm.h"
#include"XQSurfacePlotWidget.h"
#include"XQUserInfo.h"
#include"XQAppInfo.h"
#include<QLineEdit>
#include<QButtonGroup>
#include<QPushButton>
#include<QTimer>
#include<QDebug>
XQCreateAccountWidget::XQCreateAccountWidget(QWidget* parent, bool AutoUi)
	:QWidget(parent)
{
	if(AutoUi)
		init();
}

XQCreateAccountWidget::XQCreateAccountWidget(XQAppInfo* info, QWidget* parent, bool AutoUi)
	:QWidget(parent),
	m_appInfo(info)
{
	if (AutoUi)
		init();
}


XQCreateAccountWidget::~XQCreateAccountWidget()
{
	/*qInfo() << "释放注册窗口";*/
}
XQAppInfo* XQCreateAccountWidget::appInfo()
{
	return m_appInfo;
}
size_t XQCreateAccountWidget::overtime() const
{
	return m_overtime;
}
size_t XQCreateAccountWidget::resendTime() const
{
	return m_resendTime;
}
void XQCreateAccountWidget::registerAccountPressed()
{
	/*qInfo() << "注册账号";*/
	if (m_appInfo == nullptr)
	{
		qWarning() << "XQUserInfo指针缺少,无法返回数据";
		return;
	}
	auto userInfo = appInfo()->userInfo();
	//注册流程
	
	//复制数据
	userInfo->setheadPortrait(m_headPortrait->imagePath());
	userInfo->setAccount(m_accountEdit->text());
	userInfo->setPassword(m_passwordEdit->text());
	userInfo->setEmail(m_emailEdit->text());
	userInfo->setPhone(m_phoneEdit->text());
	userInfo->setName(m_nameEdit->text());
	userInfo->setGender(genderType(m_gender->checkedId()));
	userInfo->setPermission(Permission::user);//设置普通账号
}
void XQCreateAccountWidget::sendPressed()
{
	qInfo() << "发送验证码按下了";
	SendVerificationCode();
}
void XQCreateAccountWidget::SendVerificationCode()
{
	emit sendVerifyFinish();
}
void XQCreateAccountWidget::setAppInfo(XQAppInfo* appInfo)
{
	m_appInfo;
}
void XQCreateAccountWidget::setOvertime(size_t time)
{
	m_overtime = time;
}
void XQCreateAccountWidget::setResendTime(size_t time)
{
	m_resendTime = time;
}
void XQCreateAccountWidget::init()
{
	setCssFile(this,":/XQCreateAccountWidget/style.css");
	init_UI();

	//链接发送验证码信号
	connect(m_sendVerifyBtn, &QPushButton::pressed, this, &XQCreateAccountWidget::sendPressed);

	//当验证码发送成功链接禁用一段时间发送验证按钮
	connect(this, &XQCreateAccountWidget::sendVerifyFinish, [this] {
		m_startTime = QDateTime::currentDateTime();//更新时间
		m_sendVerifyBtn->setEnabled(false);//禁用发送按钮
		QString Text = m_sendVerifyBtn->text();//保存现在的按钮文本
		auto timer = new QTimer(this);
	m_sendVerifyBtn->setText(QString::number(m_resendTime / 1000));//设置倒计时开始时间
	timer->callOnTimeout([=] {
		QString currentText = m_sendVerifyBtn->text();
	if (currentText.toInt() == 0)//结束了
	{
		m_sendVerifyBtn->setText(Text);//恢复原来的文本
		m_sendVerifyBtn->setEnabled(true);
		timer->stop();
		timer->deleteLater();
		return;
	}
		m_sendVerifyBtn->setText(QString::number(currentText.toInt()-1));
		});
	timer->start(1_s);
		});
}
