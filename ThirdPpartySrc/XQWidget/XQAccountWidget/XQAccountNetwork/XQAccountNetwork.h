﻿#ifndef XQACCOUNTNETWORK_H
#define XQACCOUNTNETWORK_H
#include<QObject>
#include<QPixmap>
#include"XQAccountResult.h"
#include"XQHead.h"
//账号登录，注册，找回密码的网络操作
class XQAccountNetwork:public QObject
{
	Q_OBJECT
public:
	XQAccountNetwork(QObject* parent = nullptr);
	~XQAccountNetwork();
	//获取app基本共享数据
	XQAppInfo* appInfo()const;
public slots:
	//联网登录
	void loginNetwork();
	//联网注册账号
	void registerAccountNetwork();
	//获取头像信息
	QStringList getheadPortraitInfo(const QString& account);
	//重载获取远程服务器地址和对应的md5值
	bool getheadPortraitInfo(const QString& account,QString& remoteHeadPortraitPath, QString& headPortraitPathMd5);
	//联网根据账号查询头像
	QPixmap findHeadPortrait(const QString& account);
	//上传用户头像到ftp服务器(服务器地址，md5值)
	QPair<QString, QString> uploadUserheadPortrait(const QPixmap& pixmap);
	//保存用户头像到本地缓存
	void saveUserheadPortrait(const QString& account,  QString remoteHeadPortraitPath,  QString headPortraitPathMd5 ,const QPixmap& pixmap= QPixmap());
signals://信号
	//登录验证结果，成功会把数据写入XQUserInfo变量中
	void loginVerifyResult(login result, const QString& String = "");
	//注册验证结果
	void createVerifyResult(::create result,const QString& String="");
protected://隐藏的函数
	//判断数据库是否有用户表
	void userTableExists(const QString& tableName="user");
	
protected://初始化
	void init();
protected://事件
protected://变量
};
#endif // !XQAccountMySql_H
