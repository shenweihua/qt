﻿#ifndef XQACCOUNTRESULT_H
#define XQACCOUNTRESULT_H
//登录的验证返回结果
enum class login
{
	yes,//成功
	userNameNotExist,//用户名不存在
	passwordError,//密码错误
	error//发生错误
};
//注册账号的返回结果
enum class create
{
	yes,//成功
	userNameRepeat,//用户名重复
	error,//发生错误
	UploadheadPortrait//上传头像中
};
#endif