﻿#ifndef XQZIPPACK_ZIPINFO_H
#define XQZIPPACK_ZIPINFO_H
#include<QFileInfo>
//打包类型
enum class ZipPackType
{
	gzip,//压缩
	unzip//解压
};
struct  XQZipPack_zipInfo//文件压缩或解压时的信息
{
public:
	// 以字符串形式输出当前文件大小
	QString curfileSizeToString()const;
	//以字符串形式输出全部文件大小
	QString fileSizeToString()const;
	//设置文件
	void setFile(const QFileInfo& file);
	//增加的大小
	void addSize(const size_t size);
	//设置文件索引
	void setIndexes(size_t nSel);
	//索引+1
	void addIndexes();
	void  clear();

	QFileInfo File;//当前文件
	size_t nSel=0;//当前文件索引,已经完成数量
	size_t fileCount=0;//文件总数
	size_t fileSize = 0;//当前完成的大小
	size_t fileSumSize=0;//全部文件大小
	qreal  fileSizePercent=0;//文件大小百分比
	qreal  fileCountPercent=0;//文件数量百分比
	ZipPackType type;//方式
};
#endif