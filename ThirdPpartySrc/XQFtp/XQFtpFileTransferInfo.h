﻿#ifndef XQFTPFILETRANSFERINFO_H
#define XQFTPFILETRANSFERINFO_H
#include<QString>
class XQFtp;
//传输方式
enum class tranType
{
	up,//上传
	down//下载
};
struct XQFtpFileTransferInfo//ftp文件传输信息
{
	XQFtp* ftp;
	QString local_filePath;//本地文件路径
	QString remoteFilePath;//远程文件路径
	size_t curSize; //当前传输大小
	size_t fileSize; //文件大小
	float percent;//进度百分比
	QString fileName()const;//文件名字
	tranType type;//传输方式上传/下载
};

#endif