﻿#ifndef XQFTPINFO_H
#define XQFTPINFO_H
#include<QObject>
#include<QHostInfo>

class XQFtpInfo
{
public:
	XQFtpInfo(const QString& host="127.0.0.1", const unsigned short int port = 21, const QString& username = "", const QString& password = "");
	QString host();
	QStringList ips();
	QString ip();
	QString url();
	unsigned short int port();
	QString userName();
	QString password();
	void setHost(const QString& host);
	void setPort(const unsigned short int port);
	void setUserName(const QString& username);
	void setPassword(const QString& password);
protected:
	QHostInfo m_host;//主机地址
	unsigned short int m_port;//端口
	QString m_username;//用户名
	QString m_password;//密码
};
#endif // !XFTPINFO
