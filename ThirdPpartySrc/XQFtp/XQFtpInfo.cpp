﻿#include"XQFtpInfo.h"

XQFtpInfo::XQFtpInfo(const QString& host, const unsigned short int port, const QString& username, const QString& password)
	:
	m_port(port),
	m_username(username),
	m_password(password)
{
	setHost(host);
}

QString XQFtpInfo::host()
{
	return m_host.hostName();
}

QStringList XQFtpInfo::ips()
{
	QStringList ips;
	auto address = m_host.addresses();
	for (auto&addr:address)
	{
		ips << addr.toString();
	}
	return ips;
}

QString XQFtpInfo::ip()
{
	auto ip = ips();
	if(ip.isEmpty())
		return QString();
	return ip[0];
}

QString XQFtpInfo::url()
{
	return QString("ftp://%1:%2").arg(ip()).arg(port());
}

unsigned short int XQFtpInfo::port()
{
	return m_port;
}

QString XQFtpInfo::userName()
{
	return m_username;
}

QString XQFtpInfo::password()
{
	return m_password;
}

void XQFtpInfo::setHost(const QString& host)
{
	m_host = QHostInfo::fromName(host);
}

void XQFtpInfo::setPort(const unsigned short int port)
{
	m_port = port;
}

void XQFtpInfo::setUserName(const QString& username)
{
	m_username = username;
}

void XQFtpInfo::setPassword(const QString& password)
{
	m_password = password;
}
