﻿#ifndef XQHTTPHEADRECV_H
#define XQHTTPHEADRECV_H
#include<QString>
#include<vector>
#include"XStringMap.hpp"
enum class XQHttpRequestType
{
	GET,
	POST
};
//http的头部数据报文是发送的还是接收的
enum class XQHttpHeadType
{
	send,//发送
	recv//接收
};
//http的头部数据报文
class XQHttpHeadRecv
{
public:
	XQHttpHeadRecv();
	XQHttpHeadRecv(const QString& header);
	void setHeadType(XQHttpHeadType headType);
	//设置http的头部数据报文
	void setHeader(const QString& header);
	//设置资源路径
	void setResPath(const QString& path);
	//设置方法GET-POST
	void setMethod(XQHttpRequestType q= XQHttpRequestType::GET);
	void setHost(const QString& host);
	void setHost(const QString& host,const int port);
	void setPort(const int port);
	void setReferer(const QString& ref);
	void setCookie(const QString& cookie);
	virtual void clear();
	virtual bool isEmpty()const;
	//初始化设置默认的header
	void init_header();
	//获取报文头的类型
	XQHttpHeadType headType()const;
	//获取状态码
	int state()const;
	//获取host
	QString host();
	//获取端口号
	const int port();
	//获取资源路径
	QString getResPath();
	//获取内容长度
	qint64 ContentLength();
	XStringMap<QString> header;//请求头
protected:
	QString m_method;//方法
	QString m_resPath;//资源
	int m_port=-1;//端口
	XQHttpHeadType m_headType= XQHttpHeadType::recv;//报文类型
	int m_state = 0;
	//string Accept;//接收
	//string AcceptEncoding;//编码
	//string AcceptLanguage;//语言
	//string Connection;//连接
	//string UserAgent;
	//string Referer;
	//string Cookie;
};
#endif // !__REQUEST_H__
