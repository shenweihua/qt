﻿#ifndef XQHTTP
#define XQHTTP
#include<QSslSocket>
#include"XQHttpHeadSend.h"
//http请求类
class XQHttp:public QSslSocket
{
	Q_OBJECT
public:
	XQHttp();
	~XQHttp();
	//返回转文本
	QString dataToString()const;
	//返回主机名字
	QString hostName();
	XQHttpHeadSend& sendHead();
	//获取其文本
	QString text();
	//获取其内容
	QByteArray content()const;
	//获取数据头
	QString header_recv()const;
	//获取当前内容长度
	size_t currentContentLength();
	//获取接收的数据
	const QByteArray& data()const;
public:
	void get(const QString& url);
	void Post(const QString& url);
	void wait();
	//显示错误信息到控制台
	void showErrorToCmd(bool on);
	//错误处理
	void error(QAbstractSocket::SocketError socketError);
	//转码
	void GbkToUtf8();
signals://信号
	//请求结束
	void finish();
protected:
	void send();
	//解析url
	void getHost(const QString& url, XQHttpHeadRecv& SendRequest);
	//发送请求头
	long int sendHead(XQHttpHeadSend& SendRequest);
	//接收到服务器发来的信息
	void recvData();
	//当前块大小
	qint64 currentChunkedLength();
	//当前块的内容
	QByteArray currentContent(qint64 size);
	//结束了一些初始化准备
	void finish_init();
	//判断是否需要解压
	void deflate();
protected:
	void init();
protected:
	QString m_lastHost;//上次连接的主机名
	int m_lastPort = -1;//上次连接的端口号
	XQHttpHeadSend m_SendHead;//发送的请求头
	XQHttpHeadRecv m_RecvHead;//接收的响应头
	QByteArray m_buff;//接收缓冲
	QByteArray m_content;//内容
	bool m_showError = false;
	bool m_chunked = false;//chunked编码
	qint64 m_nSel = -1;//用在chunked编码的时候
};
#endif // !XQHttp
