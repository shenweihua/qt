﻿#include "XQHttpHeadSend.h"

XQHttpHeadSend::XQHttpHeadSend()
	:XQHttpHeadRecv()
{
	m_headType = XQHttpHeadType::send;
}

XQHttpHeadSend::XQHttpHeadSend(const QString& header)
	:XQHttpHeadRecv(header)
{
	m_headType = XQHttpHeadType::send;
}

void XQHttpHeadSend::clear()
{
	args.clear();
	header.clear();
}

bool XQHttpHeadSend::isEmpty() const
{
	return header.isEmpty()&&args.isEmpty();
}

QString XQHttpHeadSend::toString()
{
	QString quest;
	quest = m_method + " " + m_resPath + argsToString() + "  HTTP/1.1\r\n" +
		header.join("", ": ", "\r\n") + "\r\n";
	return quest;
}

QString XQHttpHeadSend::argsToString()
{
	QString str = "?";
	int n = 0;
	for (auto& data : args.data())
	{
		if (n != 0)
			str += "&";
		str += data.first;
		str += "=";
		str += data.second;
		n++;
	}
	return str;
}
